-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 10-Jul-2016 às 03:46
-- Versão do servidor: 10.0.17-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carga_pesada`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `alterarVeiculoPDisponivel` (IN `placaVeiculo` VARCHAR(45))  NO SQL
UPDATE veiculo SET status='Disponível' WHERE placa=placaVeiculo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `alteraVeiucloLocado` (IN `placaVeiculo` VARCHAR(45))  NO SQL
UPDATE veiculo SET status='Locado' WHERE placa=placaVeiculo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deletarRerva` (IN `idReserva` INT)  NO SQL
BEGIN
DELETE FROM reserva WHERE id_reserva = idReserva;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `automovel_pequeno`
--

CREATE TABLE `automovel_pequeno` (
  `id_veiculo` int(11) NOT NULL,
  `radio` tinyint(1) NOT NULL,
  `mp3` tinyint(1) NOT NULL,
  `dvd` tinyint(1) NOT NULL,
  `tamanho` float NOT NULL,
  `camera_re` tinyint(1) NOT NULL,
  `direcao_hidraulica` tinyint(1) NOT NULL,
  `tipo_cambio` varchar(45) NOT NULL,
  `ar_condicionado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `automovel_pequeno`
--

INSERT INTO `automovel_pequeno` (`id_veiculo`, `radio`, `mp3`, `dvd`, `tamanho`, `camera_re`, `direcao_hidraulica`, `tipo_cambio`, `ar_condicionado`) VALUES
(4, 1, 1, 1, 34, 1, 1, 'Manual', 1),
(5, 1, 1, 1, 12, 1, 1, 'Manual', 1),
(8, 0, 0, 0, 50, 0, 1, 'Manual', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `caminhonete_carga`
--

CREATE TABLE `caminhonete_carga` (
  `id_veiculo` int(10) NOT NULL,
  `capacidade_carga` float NOT NULL,
  `vol_vombustivel` float NOT NULL,
  `desempenho` float NOT NULL,
  `distancia_eixo` float NOT NULL,
  `potencia` float NOT NULL,
  `embreagem` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `caminhonete_carga`
--

INSERT INTO `caminhonete_carga` (`id_veiculo`, `capacidade_carga`, `vol_vombustivel`, `desempenho`, `distancia_eixo`, `potencia`, `embreagem`) VALUES
(2, 400, 12, 30, 12, 40, 'Manual'),
(6, 300, 12, 30, 12, 40, 'Manual');

-- --------------------------------------------------------

--
-- Estrutura da tabela `caminhonete_passageiros`
--

CREATE TABLE `caminhonete_passageiros` (
  `id_veiculo` int(11) NOT NULL,
  `air_bag` tinyint(1) NOT NULL,
  `contr_poluicao_ar` tinyint(1) NOT NULL,
  `cinto_seg_traseiro_retrateis` tinyint(1) NOT NULL,
  `direcao_assistida` tinyint(1) NOT NULL,
  `rodas_liga_leve` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `caminhonete_passageiros`
--

INSERT INTO `caminhonete_passageiros` (`id_veiculo`, `air_bag`, `contr_poluicao_ar`, `cinto_seg_traseiro_retrateis`, `direcao_assistida`, `rodas_liga_leve`) VALUES
(3, 0, 1, 1, 0, 1),
(7, 1, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_veiculo`
--

CREATE TABLE `categoria_veiculo` (
  `id_categoria_veiculo` int(10) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `valor` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria_veiculo`
--

INSERT INTO `categoria_veiculo` (`id_categoria_veiculo`, `nome`, `valor`) VALUES
(1, 'P3', 25),
(2, 'M2', 50),
(3, 'G4', 100);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nome`) VALUES
(1, 'Elane'),
(2, 'Robson'),
(3, 'Yago'),
(4, 'Referencial'),
(5, 'Jodibe'),
(6, 'lindão'),
(7, 'Marcos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente_fisico`
--

CREATE TABLE `cliente_fisico` (
  `cpf` varchar(15) NOT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  `num_habilitacao` varchar(45) NOT NULL,
  `vencimento_habilitacao` varchar(15) NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente_fisico`
--

INSERT INTO `cliente_fisico` (`cpf`, `sexo`, `data_nascimento`, `id_cliente`, `num_habilitacao`, `vencimento_habilitacao`) VALUES
('111.111.111-11', 'F', '1994-01-02', 1, 'Não tem', '0000-00-00'),
('222.222.222-22', 'M', '1994-08-30', 2, '456789', '2019-07-30'),
('333.333.333-33', 'F', '1994-02-11', 3, '', '0000-00-00'),
('666.666.666-66', 'M', '2016-07-01', 6, 'Não tem', '0000-00-00'),
('555.555.555-55', 'M', '1993-07-03', 7, '', '0000-00-00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente_juridico`
--

CREATE TABLE `cliente_juridico` (
  `id_cliente` int(11) NOT NULL,
  `cnpj` varchar(40) NOT NULL,
  `inscr_estadual` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente_juridico`
--

INSERT INTO `cliente_juridico` (`id_cliente`, `cnpj`, `inscr_estadual`) VALUES
(4, '11.111.111/1111-11', NULL),
(5, '22.222.222/2222-22', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco_cliente`
--

CREATE TABLE `endereco_cliente` (
  `id_cliente` int(11) NOT NULL,
  `rua` varchar(20) DEFAULT NULL,
  `bairro` varchar(20) DEFAULT NULL,
  `cidade` varchar(20) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `estado` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `endereco_cliente`
--

INSERT INTO `endereco_cliente` (`id_cliente`, `rua`, `bairro`, `cidade`, `cep`, `estado`) VALUES
(1, 'Bela', 'IPSEP', 'IPSEP', '56908-131', 'AL'),
(2, 'Bela Vista', 'IPSEP', 'IPSEP', '56908-134', 'AL'),
(3, 'Sei não', 'Seila', 'Triunfo', '56908-143', 'AL'),
(4, 'Da fis', 'Seila', 'Seila', '56909-512', 'PE'),
(5, 'Sem nome', 'IPSEP', 'IPSEP', '56908-321', 'AL'),
(6, 'semrua', 'sem', 'sem', '1233421-123', 'AL'),
(7, '12312', 'IPSEP', 'Serra', '56908-131', 'AL');

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco_locadora`
--

CREATE TABLE `endereco_locadora` (
  `id_locadora` int(10) NOT NULL,
  `rua` varchar(20) DEFAULT NULL,
  `bairro` varchar(20) DEFAULT NULL,
  `cidade` varchar(20) DEFAULT NULL,
  `cep` varchar(20) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `endereco_locadora`
--

INSERT INTO `endereco_locadora` (`id_locadora`, `rua`, `bairro`, `cidade`, `cep`, `estado`) VALUES
(1, 'Sem Nome', 'IPSEP', 'Serra', '56907-132', 'AL'),
(2, 'Sem Saida', 'COAB', 'Serra', '56903-414', 'PE'),
(3, 'Boa Vista', 'AABB', 'Calumbi', '56908-231', 'PE'),
(4, 'Rua Bela', 'BOA', 'Sertania', '56092-123', 'CE'),
(5, 'Kirino', 'Cordeiro', 'Mata Seca', '56908-166', 'PA'),
(6, 'Facada', 'Alto', 'Sertta', '56982-234', 'ES'),
(7, 'Pintonbeiras', 'Pitomba', 'Sergipe', '56982-213', 'PB'),
(8, 'Doida', 'Sem Nome', 'De boa', '12987-123', 'BA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `locacao`
--

CREATE TABLE `locacao` (
  `id_locacao` int(11) NOT NULL,
  `id_reserva` int(11) NOT NULL,
  `motorista` varchar(45) NOT NULL,
  `placa_veiculo` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `locacao`
--

INSERT INTO `locacao` (`id_locacao`, `id_reserva`, `motorista`, `placa_veiculo`) VALUES
(1, 2, '222.222.222-22', 'ccc-2222'),
(3, 3, '222.222.222-22', 'bbb-2222'),
(4, 4, '222.222.222-22', 'ccc-2222'),
(5, 5, '222.222.222-22', 'ccc-2222'),
(6, 6, '222.222.222-22', 'ccc-2222'),
(8, 8, '222.222.222-22', 'ddd-1111');

-- --------------------------------------------------------

--
-- Estrutura da tabela `locadora`
--

CREATE TABLE `locadora` (
  `id_locadora` int(10) NOT NULL,
  `cnpj` varchar(25) NOT NULL,
  `nome` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `locadora`
--

INSERT INTO `locadora` (`id_locadora`, `cnpj`, `nome`) VALUES
(1, '11-111-111/1111-11', 'Sertalmol'),
(2, '22-222-222/2222-22', 'Yamaha'),
(3, '33-333-333/3333-33', 'Honda'),
(4, '44-444-444/4444-44', 'Locadora do zé'),
(5, '55-555-555/5555-55', 'Sem Nome'),
(6, '66-666-666/6666-66', 'Laninha Loca'),
(7, '23-425-234/2345-23', 'LocaFácil'),
(8, '77-565-456/4564-56', 'LocaMaloca');

-- --------------------------------------------------------

--
-- Estrutura da tabela `reserva`
--

CREATE TABLE `reserva` (
  `id_reserva` int(11) NOT NULL,
  `data_retirada` date NOT NULL,
  `status` varchar(35) NOT NULL,
  `data_entrega` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `nome_categoria_veiculo` varchar(35) NOT NULL,
  `nome_locadora` varchar(45) NOT NULL,
  `tipo_cliente` varchar(35) NOT NULL,
  `valor_reserva` float NOT NULL,
  `tipo_locacao` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `reserva`
--

INSERT INTO `reserva` (`id_reserva`, `data_retirada`, `status`, `data_entrega`, `id_cliente`, `nome_categoria_veiculo`, `nome_locadora`, `tipo_cliente`, `valor_reserva`, `tipo_locacao`) VALUES
(2, '2016-07-02', 'Finalizada', '2016-07-09', 4, 'M2', 'Honda', 'Cliente Jurídico', 105, 'KM Livre'),
(3, '2016-07-01', 'Finalizada', '2016-07-02', 7, 'G4', 'Honda', 'Cliente Físico', 210, 'KM Livre'),
(4, '2016-07-01', 'Finalizada', '2016-07-03', 2, 'M2', 'Honda', 'Cliente Físico', 165, 'KM Controle'),
(6, '2016-07-14', 'Finalizada', '2016-07-15', 2, 'M2', 'Honda', 'Cliente Físico', 102, 'KM Livre'),
(8, '2016-07-08', 'Efetivada', '2016-07-09', 2, 'P3', 'Honda', 'Cliente Físico', 25, 'KM Controle');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nome` varchar(65) NOT NULL,
  `email` varchar(15) DEFAULT NULL,
  `cpf` varchar(20) NOT NULL,
  `senha` varchar(15) DEFAULT NULL,
  `login` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome`, `email`, `cpf`, `senha`, `login`) VALUES
(2, 'Murilo', 'murilo@murilo', '222.222.222-22', 'semsenha', 'murilox');

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculo`
--

CREATE TABLE `veiculo` (
  `id_veiculo` int(11) NOT NULL,
  `nome_locadora` varchar(45) NOT NULL,
  `ano_modelo` int(10) UNSIGNED DEFAULT NULL,
  `num_portas` int(10) UNSIGNED DEFAULT NULL,
  `num_chassi` int(10) UNSIGNED DEFAULT NULL,
  `torque` int(10) UNSIGNED DEFAULT NULL,
  `num_motor` int(10) UNSIGNED DEFAULT NULL,
  `placa` varchar(10) NOT NULL,
  `nome_categoria_veiculo` varchar(30) NOT NULL,
  `ano_fabricacao` int(10) UNSIGNED DEFAULT NULL,
  `quilometragem_atual` int(11) DEFAULT NULL,
  `cor` varchar(10) DEFAULT NULL,
  `tipo_combustivel` varchar(15) DEFAULT NULL,
  `num_passageiros` int(10) UNSIGNED DEFAULT NULL,
  `revisoes` varchar(30) NOT NULL,
  `status` varchar(35) NOT NULL,
  `limpeza` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `veiculo`
--

INSERT INTO `veiculo` (`id_veiculo`, `nome_locadora`, `ano_modelo`, `num_portas`, `num_chassi`, `torque`, `num_motor`, `placa`, `nome_categoria_veiculo`, `ano_fabricacao`, `quilometragem_atual`, `cor`, `tipo_combustivel`, `num_passageiros`, `revisoes`, `status`, `limpeza`) VALUES
(2, 'Honda', 2000, 4, 4, 23, 5345, 'bbb-2222', 'G4', 1992, 150, 'Preto', 'Álcool', 4, 'Ok', 'Disponível', 'Ok'),
(3, 'Honda', 2000, 4, 4, 23, 5345, 'ccc-2222', 'G4', 1992, 190, 'Vermelho', 'Álcool', 4, 'Ok', 'Disponível', 'Ok'),
(4, 'Honda', 2000, 4, 4, 23, 5345, 'ddd-1111', 'G4', 1992, 70, 'Vermelho', 'Álcool', 4, 'Ok', 'Locado', 'Ok'),
(5, 'Honda', 2000, 4, 4, 23, 5345, 'hhh-8888', 'G4', 1992, 80, 'Azul', 'Álcool', 4, 'Ok', 'Disponível', 'Ok'),
(6, 'Honda', 2000, 4, 4, 23, 5345, 'ttt-8888', 'G4', 1992, 90, 'Azul', 'Álcool', 4, 'Ok', 'Disponível', 'Ok'),
(7, 'Yamaha', 2000, 4, 4, 23, 5345, 'sss-8888', 'G4', 1992, 100, 'Azul', 'Álcool', 4, 'Ok', 'Disponível', 'Ok'),
(8, 'Yamaha', 1993, 4, 4, 12, 123123, 'jjj-1234', 'G4', 1992, 100, 'Vermelho', 'Gasonila', 4, 'Ok', 'Disponível', 'Ok');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_caminhoneteCarga`
--
CREATE TABLE `v_caminhoneteCarga` (
`id_veiculo` int(11)
,`nome_locadora` varchar(45)
,`ano_modelo` int(10) unsigned
,`num_portas` int(10) unsigned
,`num_chassi` int(10) unsigned
,`torque` int(10) unsigned
,`num_motor` int(10) unsigned
,`placa` varchar(10)
,`nome_categoria_veiculo` varchar(30)
,`ano_fabricacao` int(10) unsigned
,`quilometragem_atual` int(11)
,`cor` varchar(10)
,`tipo_combustivel` varchar(15)
,`num_passageiros` int(10) unsigned
,`revisoes` varchar(30)
,`status` varchar(35)
,`limpeza` varchar(35)
,`capacidade_carga` float
,`vol_vombustivel` float
,`desempenho` float
,`distancia_eixo` float
,`potencia` float
,`embreagem` varchar(35)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_caminhonetePassageiro`
--
CREATE TABLE `v_caminhonetePassageiro` (
`id_veiculo` int(11)
,`nome_locadora` varchar(45)
,`ano_modelo` int(10) unsigned
,`num_portas` int(10) unsigned
,`num_chassi` int(10) unsigned
,`torque` int(10) unsigned
,`num_motor` int(10) unsigned
,`placa` varchar(10)
,`nome_categoria_veiculo` varchar(30)
,`ano_fabricacao` int(10) unsigned
,`quilometragem_atual` int(11)
,`cor` varchar(10)
,`tipo_combustivel` varchar(15)
,`num_passageiros` int(10) unsigned
,`revisoes` varchar(30)
,`status` varchar(35)
,`limpeza` varchar(35)
,`air_bag` tinyint(1)
,`contr_poluicao_ar` tinyint(1)
,`cinto_seg_traseiro_retrateis` tinyint(1)
,`direcao_assistida` tinyint(1)
,`rodas_liga_leve` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_clienteFisico`
--
CREATE TABLE `v_clienteFisico` (
`id_cliente` int(11)
,`nome` varchar(40)
,`cpf` varchar(15)
,`sexo` varchar(15)
,`data_nascimento` date
,`num_habilitacao` varchar(45)
,`vencimento_habilitacao` varchar(15)
,`rua` varchar(20)
,`bairro` varchar(20)
,`cidade` varchar(20)
,`cep` varchar(11)
,`estado` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_clienteJuridico`
--
CREATE TABLE `v_clienteJuridico` (
`id_cliente` int(11)
,`nome` varchar(40)
,`cnpj` varchar(40)
,`inscr_estadual` varchar(20)
,`rua` varchar(20)
,`bairro` varchar(20)
,`cidade` varchar(20)
,`cep` varchar(11)
,`estado` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_LocacaoReserva`
--
CREATE TABLE `v_LocacaoReserva` (
`id_locacao` int(11)
,`placa_veiculo` varchar(45)
,`nome` varchar(40)
,`data_retirada` date
,`data_entrega` date
,`nome_categoria_veiculo` varchar(35)
,`nome_locadora` varchar(45)
,`valor_reserva` float
,`tipo_locacao` varchar(45)
,`status` varchar(35)
,`id_reserva` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_locadora`
--
CREATE TABLE `v_locadora` (
`id_locadora` int(10)
,`Nome` varchar(45)
,`cnpj` varchar(25)
,`rua` varchar(20)
,`bairro` varchar(20)
,`cidade` varchar(20)
,`cep` varchar(20)
,`estado` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_reserva`
--
CREATE TABLE `v_reserva` (
`nome` varchar(40)
,`id_reserva` int(11)
,`data_retirada` date
,`status` varchar(35)
,`data_entrega` date
,`nome_categoria_veiculo` varchar(35)
,`nome_locadora` varchar(45)
,`tipo_cliente` varchar(35)
,`valor_reserva` float
,`tipo_locacao` varchar(45)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_veiculoPequeno`
--
CREATE TABLE `v_veiculoPequeno` (
`id_veiculo` int(11)
,`nome_locadora` varchar(45)
,`ano_modelo` int(10) unsigned
,`num_portas` int(10) unsigned
,`num_chassi` int(10) unsigned
,`torque` int(10) unsigned
,`num_motor` int(10) unsigned
,`placa` varchar(10)
,`nome_categoria_veiculo` varchar(30)
,`ano_fabricacao` int(10) unsigned
,`quilometragem_atual` int(11)
,`cor` varchar(10)
,`tipo_combustivel` varchar(15)
,`num_passageiros` int(10) unsigned
,`revisoes` varchar(30)
,`status` varchar(35)
,`limpeza` varchar(35)
,`radio` tinyint(1)
,`mp3` tinyint(1)
,`dvd` tinyint(1)
,`tamanho` float
,`camera_re` tinyint(1)
,`direcao_hidraulica` tinyint(1)
,`tipo_cambio` varchar(45)
,`ar_condicionado` tinyint(1)
);

-- --------------------------------------------------------

--
-- Structure for view `v_caminhoneteCarga`
--
DROP TABLE IF EXISTS `v_caminhoneteCarga`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_caminhoneteCarga`  AS  select `vei`.`id_veiculo` AS `id_veiculo`,`vei`.`nome_locadora` AS `nome_locadora`,`vei`.`ano_modelo` AS `ano_modelo`,`vei`.`num_portas` AS `num_portas`,`vei`.`num_chassi` AS `num_chassi`,`vei`.`torque` AS `torque`,`vei`.`num_motor` AS `num_motor`,`vei`.`placa` AS `placa`,`vei`.`nome_categoria_veiculo` AS `nome_categoria_veiculo`,`vei`.`ano_fabricacao` AS `ano_fabricacao`,`vei`.`quilometragem_atual` AS `quilometragem_atual`,`vei`.`cor` AS `cor`,`vei`.`tipo_combustivel` AS `tipo_combustivel`,`vei`.`num_passageiros` AS `num_passageiros`,`vei`.`revisoes` AS `revisoes`,`vei`.`status` AS `status`,`vei`.`limpeza` AS `limpeza`,`camCarga`.`capacidade_carga` AS `capacidade_carga`,`camCarga`.`vol_vombustivel` AS `vol_vombustivel`,`camCarga`.`desempenho` AS `desempenho`,`camCarga`.`distancia_eixo` AS `distancia_eixo`,`camCarga`.`potencia` AS `potencia`,`camCarga`.`embreagem` AS `embreagem` from (`veiculo` `vei` join `caminhonete_carga` `camCarga` on((`vei`.`id_veiculo` = `camCarga`.`id_veiculo`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_caminhonetePassageiro`
--
DROP TABLE IF EXISTS `v_caminhonetePassageiro`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_caminhonetePassageiro`  AS  select `vei`.`id_veiculo` AS `id_veiculo`,`vei`.`nome_locadora` AS `nome_locadora`,`vei`.`ano_modelo` AS `ano_modelo`,`vei`.`num_portas` AS `num_portas`,`vei`.`num_chassi` AS `num_chassi`,`vei`.`torque` AS `torque`,`vei`.`num_motor` AS `num_motor`,`vei`.`placa` AS `placa`,`vei`.`nome_categoria_veiculo` AS `nome_categoria_veiculo`,`vei`.`ano_fabricacao` AS `ano_fabricacao`,`vei`.`quilometragem_atual` AS `quilometragem_atual`,`vei`.`cor` AS `cor`,`vei`.`tipo_combustivel` AS `tipo_combustivel`,`vei`.`num_passageiros` AS `num_passageiros`,`vei`.`revisoes` AS `revisoes`,`vei`.`status` AS `status`,`vei`.`limpeza` AS `limpeza`,`camPassageiro`.`air_bag` AS `air_bag`,`camPassageiro`.`contr_poluicao_ar` AS `contr_poluicao_ar`,`camPassageiro`.`cinto_seg_traseiro_retrateis` AS `cinto_seg_traseiro_retrateis`,`camPassageiro`.`direcao_assistida` AS `direcao_assistida`,`camPassageiro`.`rodas_liga_leve` AS `rodas_liga_leve` from (`veiculo` `vei` join `caminhonete_passageiros` `camPassageiro` on((`vei`.`id_veiculo` = `camPassageiro`.`id_veiculo`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_clienteFisico`
--
DROP TABLE IF EXISTS `v_clienteFisico`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clienteFisico`  AS  select `cli`.`id_cliente` AS `id_cliente`,`cli`.`nome` AS `nome`,`fisico`.`cpf` AS `cpf`,`fisico`.`sexo` AS `sexo`,`fisico`.`data_nascimento` AS `data_nascimento`,`fisico`.`num_habilitacao` AS `num_habilitacao`,`fisico`.`vencimento_habilitacao` AS `vencimento_habilitacao`,`endCliente`.`rua` AS `rua`,`endCliente`.`bairro` AS `bairro`,`endCliente`.`cidade` AS `cidade`,`endCliente`.`cep` AS `cep`,`endCliente`.`estado` AS `estado` from ((`cliente` `cli` join `cliente_fisico` `fisico` on((`cli`.`id_cliente` = `fisico`.`id_cliente`))) join `endereco_cliente` `endCliente` on((`cli`.`id_cliente` = `endCliente`.`id_cliente`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_clienteJuridico`
--
DROP TABLE IF EXISTS `v_clienteJuridico`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clienteJuridico`  AS  select `cli`.`id_cliente` AS `id_cliente`,`cli`.`nome` AS `nome`,`juridico`.`cnpj` AS `cnpj`,`juridico`.`inscr_estadual` AS `inscr_estadual`,`endCliente`.`rua` AS `rua`,`endCliente`.`bairro` AS `bairro`,`endCliente`.`cidade` AS `cidade`,`endCliente`.`cep` AS `cep`,`endCliente`.`estado` AS `estado` from ((`cliente` `cli` join `cliente_juridico` `juridico` on((`cli`.`id_cliente` = `juridico`.`id_cliente`))) join `endereco_cliente` `endCliente` on((`cli`.`id_cliente` = `endCliente`.`id_cliente`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_LocacaoReserva`
--
DROP TABLE IF EXISTS `v_LocacaoReserva`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_LocacaoReserva`  AS  select `loc`.`id_locacao` AS `id_locacao`,`loc`.`placa_veiculo` AS `placa_veiculo`,`vReserva`.`nome` AS `nome`,`vReserva`.`data_retirada` AS `data_retirada`,`vReserva`.`data_entrega` AS `data_entrega`,`vReserva`.`nome_categoria_veiculo` AS `nome_categoria_veiculo`,`vReserva`.`nome_locadora` AS `nome_locadora`,`vReserva`.`valor_reserva` AS `valor_reserva`,`vReserva`.`tipo_locacao` AS `tipo_locacao`,`vReserva`.`status` AS `status`,`vReserva`.`id_reserva` AS `id_reserva` from (`v_reserva` `vReserva` join `locacao` `loc` on((`vReserva`.`id_reserva` = `loc`.`id_reserva`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_locadora`
--
DROP TABLE IF EXISTS `v_locadora`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_locadora`  AS  select `loc`.`id_locadora` AS `id_locadora`,`loc`.`nome` AS `Nome`,`loc`.`cnpj` AS `cnpj`,`endLocadora`.`rua` AS `rua`,`endLocadora`.`bairro` AS `bairro`,`endLocadora`.`cidade` AS `cidade`,`endLocadora`.`cep` AS `cep`,`endLocadora`.`estado` AS `estado` from (`locadora` `loc` join `endereco_locadora` `endLocadora` on((`loc`.`id_locadora` = `endLocadora`.`id_locadora`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_reserva`
--
DROP TABLE IF EXISTS `v_reserva`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_reserva`  AS  select `cli`.`nome` AS `nome`,`res`.`id_reserva` AS `id_reserva`,`res`.`data_retirada` AS `data_retirada`,`res`.`status` AS `status`,`res`.`data_entrega` AS `data_entrega`,`res`.`nome_categoria_veiculo` AS `nome_categoria_veiculo`,`res`.`nome_locadora` AS `nome_locadora`,`res`.`tipo_cliente` AS `tipo_cliente`,`res`.`valor_reserva` AS `valor_reserva`,`res`.`tipo_locacao` AS `tipo_locacao` from (`reserva` `res` join `cliente` `cli` on((`res`.`id_cliente` = `cli`.`id_cliente`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_veiculoPequeno`
--
DROP TABLE IF EXISTS `v_veiculoPequeno`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_veiculoPequeno`  AS  select `vei`.`id_veiculo` AS `id_veiculo`,`vei`.`nome_locadora` AS `nome_locadora`,`vei`.`ano_modelo` AS `ano_modelo`,`vei`.`num_portas` AS `num_portas`,`vei`.`num_chassi` AS `num_chassi`,`vei`.`torque` AS `torque`,`vei`.`num_motor` AS `num_motor`,`vei`.`placa` AS `placa`,`vei`.`nome_categoria_veiculo` AS `nome_categoria_veiculo`,`vei`.`ano_fabricacao` AS `ano_fabricacao`,`vei`.`quilometragem_atual` AS `quilometragem_atual`,`vei`.`cor` AS `cor`,`vei`.`tipo_combustivel` AS `tipo_combustivel`,`vei`.`num_passageiros` AS `num_passageiros`,`vei`.`revisoes` AS `revisoes`,`vei`.`status` AS `status`,`vei`.`limpeza` AS `limpeza`,`autoPequeno`.`radio` AS `radio`,`autoPequeno`.`mp3` AS `mp3`,`autoPequeno`.`dvd` AS `dvd`,`autoPequeno`.`tamanho` AS `tamanho`,`autoPequeno`.`camera_re` AS `camera_re`,`autoPequeno`.`direcao_hidraulica` AS `direcao_hidraulica`,`autoPequeno`.`tipo_cambio` AS `tipo_cambio`,`autoPequeno`.`ar_condicionado` AS `ar_condicionado` from (`veiculo` `vei` join `automovel_pequeno` `autoPequeno` on((`vei`.`id_veiculo` = `autoPequeno`.`id_veiculo`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `automovel_pequeno`
--
ALTER TABLE `automovel_pequeno`
  ADD PRIMARY KEY (`id_veiculo`),
  ADD UNIQUE KEY `id_Veiculo` (`id_veiculo`);

--
-- Indexes for table `caminhonete_carga`
--
ALTER TABLE `caminhonete_carga`
  ADD PRIMARY KEY (`id_veiculo`);

--
-- Indexes for table `caminhonete_passageiros`
--
ALTER TABLE `caminhonete_passageiros`
  ADD PRIMARY KEY (`id_veiculo`);

--
-- Indexes for table `categoria_veiculo`
--
ALTER TABLE `categoria_veiculo`
  ADD PRIMARY KEY (`id_categoria_veiculo`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `id_cliente` (`id_cliente`);

--
-- Indexes for table `cliente_fisico`
--
ALTER TABLE `cliente_fisico`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `id_cliente` (`id_cliente`),
  ADD UNIQUE KEY `cpf` (`cpf`),
  ADD KEY `id_cliente_2` (`id_cliente`);

--
-- Indexes for table `cliente_juridico`
--
ALTER TABLE `cliente_juridico`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `id_cliente` (`id_cliente`),
  ADD UNIQUE KEY `cnpj` (`cnpj`);

--
-- Indexes for table `endereco_cliente`
--
ALTER TABLE `endereco_cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD UNIQUE KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_cliente_2` (`id_cliente`);

--
-- Indexes for table `endereco_locadora`
--
ALTER TABLE `endereco_locadora`
  ADD PRIMARY KEY (`id_locadora`),
  ADD KEY `id_locadora` (`id_locadora`);

--
-- Indexes for table `locacao`
--
ALTER TABLE `locacao`
  ADD PRIMARY KEY (`id_locacao`),
  ADD UNIQUE KEY `id_reserva` (`id_reserva`);

--
-- Indexes for table `locadora`
--
ALTER TABLE `locadora`
  ADD PRIMARY KEY (`id_locadora`),
  ADD UNIQUE KEY `cnpj` (`cnpj`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- Indexes for table `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id_reserva`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`cpf`),
  ADD UNIQUE KEY `id_usuario` (`id_usuario`);

--
-- Indexes for table `veiculo`
--
ALTER TABLE `veiculo`
  ADD PRIMARY KEY (`id_veiculo`),
  ADD UNIQUE KEY `placa` (`placa`),
  ADD KEY `ID_locadora` (`nome_locadora`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria_veiculo`
--
ALTER TABLE `categoria_veiculo`
  MODIFY `id_categoria_veiculo` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `cliente_fisico`
--
ALTER TABLE `cliente_fisico`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `endereco_locadora`
--
ALTER TABLE `endereco_locadora`
  MODIFY `id_locadora` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `locacao`
--
ALTER TABLE `locacao`
  MODIFY `id_locacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `locadora`
--
ALTER TABLE `locadora`
  MODIFY `id_locadora` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `veiculo`
--
ALTER TABLE `veiculo`
  MODIFY `id_veiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `automovel_pequeno`
--
ALTER TABLE `automovel_pequeno`
  ADD CONSTRAINT `id_veiculo_categoria` FOREIGN KEY (`id_Veiculo`) REFERENCES `veiculo` (`id_veiculo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
