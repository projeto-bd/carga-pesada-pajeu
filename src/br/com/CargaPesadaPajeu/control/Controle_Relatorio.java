/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import br.com.CargaPesadaPajeu.dao.Dao_Relatorio;
import java.sql.ResultSet;

/**
 *
 * @author root
 */
public class Controle_Relatorio {

    Dao_Relatorio gerenciarRelatorio = new Dao_Relatorio();

    public ResultSet listarReservasPorPeriodo(String dataInicial, String dataFinal) {
        ResultSet rs = gerenciarRelatorio.listarReservas(dataInicial, dataFinal);
        return rs;
    }

    public ResultSet listarLocacaoPorPeriodo(String dataInicial, String dataFinal) {
        ResultSet rs = gerenciarRelatorio.listarLocacoes(dataInicial, dataFinal);
        return rs;
    }

    public ResultSet listarClienteFisico(String cpf) {
        ResultSet rs = gerenciarRelatorio.listarClienteFisico(cpf);
        return rs;
    }

    public ResultSet listarClienteJuridico(String cnpj) {
        ResultSet rs = gerenciarRelatorio.listarClienteJuridico(cnpj);
        return rs;
    }

}
