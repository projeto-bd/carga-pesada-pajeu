/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import br.com.CargaPesadaPajeu.dao.Dao_Reserva;
import br.com.CargaPesadaPajeu.modeller.Reserva;
import java.sql.ResultSet;

/**
 *
 * @author root
 */
public class Controle_Reserva {

    Dao_Reserva gerenciarReserva = new Dao_Reserva();

    public ResultSet verificarClienteFisico(String cpf) {
        ResultSet rs = gerenciarReserva.verificaClienteFisico(cpf);
        return rs;
    }
    public int retornarId(){
    int idReserva = gerenciarReserva.retormarMaxReserva();
    return idReserva;
    }

    public ResultSet verificarClienteJuridico(String cnpj) {
        ResultSet rs = gerenciarReserva.verificaClienteJuridico(cnpj);
        return rs;

    }

    public ResultSet carregarCategoriaVeiculo(String nome) {
        ResultSet rs = gerenciarReserva.carregarValorCategoria(nome);
        return rs;

    }

    public boolean efeutarReserva(String dataRetirada, String status, String dataEntrega,
            int idCliente, String nome_categoria_veiculo, String nomeLocadora, String tipo_cliente, float valor,String tipo_locacao) {

        Reserva reserva = new Reserva(dataRetirada, status, dataEntrega, idCliente, nome_categoria_veiculo,
                nomeLocadora, tipo_cliente, valor,tipo_locacao);
        boolean resposta = gerenciarReserva.efetuarReserva(reserva);

        return resposta;
    }

    public ResultSet listarReservas(String consulta) {
        ResultSet rs = gerenciarReserva.listarReservas(consulta);

        return rs;
    }

    public boolean deletarReserva(String deletar, int resposta) {

        boolean confirmacao = gerenciarReserva.deletarReserva(deletar, resposta);
        return confirmacao;
    }

    public boolean alterarReserva(String dataRetirada, String status, String dataEntrega,
            String nome_categoria_veiculo, String nomeLocadora, float valor, String idReserva,String tipo_locacao) {

        boolean confirmacao = gerenciarReserva.alterarReserva(dataRetirada, status, dataEntrega,
                nome_categoria_veiculo, nomeLocadora, valor, idReserva,tipo_locacao);
        return confirmacao;
    }

}
