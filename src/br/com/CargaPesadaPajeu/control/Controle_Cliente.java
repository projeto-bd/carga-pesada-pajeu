/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import br.com.CargaPesadaPajeu.dao.ConexaoBD;
import br.com.CargaPesadaPajeu.modeller.Cliente;
import br.com.CargaPesadaPajeu.modeller.ClientePessoaFisica;
import br.com.CargaPesadaPajeu.modeller.ClientePessoaJuridica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Controle_Cliente {

    Connection conn = null;
    PreparedStatement pstm = null;
    public ResultSet rs;
    public Statement st;

    public boolean verificaCpf(String cpf) {
        boolean resultado = false;

        String sql = "SELECT cpf FROM cliente_fisico";
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {

                if (cpf.equals(rs.getString("cpf"))) {

                    resultado = true;

                    break;
                } else {

                    resultado = false;
                }

            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Socorro! algo deu errado :(");
        }

        return resultado;

    }

    public void adicionarClienteFisico(ClientePessoaFisica cliente) {
      
        conn = new ConexaoBD().getConexao();
        if (verificaCpf(cliente.getCpf()) == false) {
            try {
                String sqlCliente = "INSERT INTO cliente (nome) "
                        + "VALUES (?)";

                pstm = conn.prepareStatement(sqlCliente);

                pstm.setString(1, cliente.getNome());

                pstm.execute();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro:" + e.getMessage());
            }

            try {

                int resultado = retornaProximoId();
                String sqlEndereco = "INSERT INTO endereco_cliente (id_cliente,rua,bairro,cidade,cep,estado) "
                        + "VALUES (?,?,?,?,?,?)";

                pstm = conn.prepareStatement(sqlEndereco);
                pstm.setInt(1, resultado);
                pstm.setString(2, cliente.getNomeRua());
                pstm.setString(3, cliente.getBairro());
                pstm.setString(4, cliente.getCidade());
                pstm.setString(5, cliente.getCep());
                pstm.setString(6, cliente.getEstado());
                pstm.execute();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro:" + e.getMessage());
            }
            try {
                String sqlClienteFisico = "INSERT INTO cliente_fisico (cpf,sexo,data_nascimento,id_cliente,num_habilitacao,vencimento_habilitacao) "
                        + "VALUES (?,?,?,?,?,?)";

                int resultado = retornaProximoId();
                pstm = conn.prepareStatement(sqlClienteFisico);
                pstm.setString(1, cliente.getCpf());
                pstm.setString(2, cliente.getSexo());
                pstm.setString(3, cliente.getDataNascimento());
                pstm.setInt(4, resultado);
                pstm.setString(5, cliente.getNumeroHabilitacao());
                pstm.setString(6, cliente.getVencimentoHabilitacao());

                pstm.execute();
                JOptionPane.showMessageDialog(null, "Adiconado com sucesso!");

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro:" + e.getMessage());
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    JOptionPane.showMessageDialog(null, "Ops! algo deu errado:"+e.getMessage());
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ops! CPF já cadastrado!");

        }
    }

    public void listarClienteFisico(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("")) {
            sql = "SELECT * FROM v_clienteFisico";

        } else {
            sql = "SELECT * FROM v_clienteFisico WHERE cpf='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
        }

    }

    public void deletarClienteFisico(String deletar, int resposta, int id) {
        String sqlClienteFisico = "DELETE FROM cliente_fisico WHERE cpf='" + deletar + "'";
        String sqlCliente = "DELETE FROM cliente WHERE id_cliente='" + id + "'";
        String sqlEndereco = "DELETE FROM endereco_cliente WHERE id_cliente='" + id + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();
                pstm = conn.prepareStatement(sqlClienteFisico);
                pstm.executeUpdate();
                pstm = conn.prepareStatement(sqlCliente);
                pstm.executeUpdate();
                pstm = conn.prepareStatement(sqlEndereco);
                pstm.executeUpdate();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Oh! algo terrível aconteceu:" + e.getMessage());
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    JOptionPane.showMessageDialog(null, "Ops! algo deu errado!");
                }
            }

        }

    }

    public void alterarClienteFisico(String nome, String cpf, String sexo,
            String dataNascimento, String numHabilitacao,
            String vencimentoHabilitacao, String rua, String bairro, String cidade, 
            String cep, String estado,String idCliente) {

        conn = new ConexaoBD().getConexao();
        String sqlCliente = null;
        String sqlClienteFisico = null;
        String sqlEndereco = null;
        
        try {
            sqlCliente = "UPDATE v_clienteFisico SET nome='" + nome
                    + "'WHERE id_cliente='" + idCliente + "'";
            
            sqlClienteFisico = "UPDATE v_clienteFisico SET cpf='" + cpf
                    + "',sexo='" + sexo
                    + "',data_nascimento='" + dataNascimento
                    + "',num_habilitacao='" + numHabilitacao
                    + "',vencimento_habilitacao='" + vencimentoHabilitacao
                    + "'WHERE id_cliente='" + idCliente + "'";
            
             sqlEndereco = "UPDATE v_clienteFisico SET rua='" + rua
                    + "',bairro='" + bairro
                    + "',cidade='" + cidade
                    + "',cep='" + cep
                    + "',estado='" + estado 
                    +"'WHERE id_cliente='" + idCliente + "'";
            
                
            
            
            pstm = conn.prepareStatement(sqlCliente);
            pstm.executeUpdate();
            pstm = conn.prepareStatement(sqlClienteFisico);
            pstm.executeUpdate();
            pstm = conn.prepareStatement(sqlEndereco);
            pstm.executeUpdate();

            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar dados:"+e.getMessage());
        }finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    JOptionPane.showMessageDialog(null, "Ops! algo deu errado!");
                }
            }
    }


    //cliente juridico
    public int retornaProximoId() {
        int resultado = 0;
        try {
            Statement st2 = conn.createStatement();;
            ResultSet rs2;

            String sqlMaxId = "SELECT MAX(id_cliente) FROM cliente";
            rs2 = st2.executeQuery(sqlMaxId);
            while (rs2.next()) {
                resultado = rs2.getInt(1);
            }
        } catch (Exception e) {
        }

        return resultado;
    }

    public boolean verificaCnpj(String cnpj) {
        conn = new ConexaoBD().getConexao();
        boolean resultado = false;

        String sql = "SELECT cnpj FROM cliente_juridico";
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {

                if (cnpj.equals(rs.getString("cnpj"))) {

                    resultado = true;

                    break;
                } else {

                    resultado = false;
                }

            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Socorro! algo deu errado :(");
        }

        return resultado;

    }

    public void adicionarClienteJuridico(ClientePessoaJuridica cliente) {
        
        if (verificaCnpj(cliente.getCnpj()) == false) {
            try {
                
                String sqlCliente = "INSERT INTO cliente (nome) "
                        + "VALUES (?)";

                pstm = conn.prepareStatement(sqlCliente);

                pstm.setString(1, cliente.getNome());
                
                pstm.execute();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro:" + e.getMessage());
            }

            try {

                int resultado = retornaProximoId();
                String sqlEndereco = "INSERT INTO endereco_cliente (id_cliente,rua,bairro,cidade,cep,estado) "
                        + "VALUES (?,?,?,?,?,?)";

                pstm = conn.prepareStatement(sqlEndereco);
                pstm.setInt(1, resultado);
                pstm.setString(2, cliente.getNomeRua());
                pstm.setString(3, cliente.getBairro());
                pstm.setString(4, cliente.getCidade());
                pstm.setString(5, cliente.getCep());
                pstm.setString(6, cliente.getEstado());
                
                pstm.execute();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro:" + e.getMessage());
            }
            try {
                String sqlClienteJuridico = "INSERT INTO cliente_juridico (id_cliente,cnpj,inscr_estadual) "
                        + "VALUES (?,?,?)";

                int resultado = retornaProximoId();
                pstm = conn.prepareStatement(sqlClienteJuridico);
                pstm.setInt(1, resultado);
                pstm.setString(2, cliente.getCnpj());
                pstm.setString(3, cliente.getInscricaoEstadual());
               
                pstm.execute();
                JOptionPane.showMessageDialog(null, "Adiconado com sucesso!");

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro:" + e.getMessage());
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    JOptionPane.showMessageDialog(null, "Ops! algo deu errado!");
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ops! CNPJ já cadastrado");

        }

    }

    public void listarClienteJuridico(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("")) {
            sql = "SELECT * FROM v_clienteJuridico";

        } else {
            sql = "SELECT * FROM v_clienteJuridico WHERE cnpj='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
        }

    }

    public void deletarClienteJuridico(String deletar, int resposta, int id) {
        String sqlClienteFisico = "DELETE FROM cliente_juridico WHERE cnpj='" + deletar + "'";
        String sqlCliente = "DELETE FROM cliente WHERE id_cliente='" + id + "'";
        String sqlEndereco = "DELETE FROM endereco_cliente WHERE id_cliente='" + id + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();
                pstm = conn.prepareStatement(sqlClienteFisico);
                pstm.executeUpdate();
                pstm = conn.prepareStatement(sqlCliente);
                pstm.executeUpdate();
                pstm = conn.prepareStatement(sqlEndereco);
                pstm.executeUpdate();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Oh! algo terrível aconteceu:" + e.getMessage());
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    JOptionPane.showMessageDialog(null, "Ops! algo deu errado!");
                }
            }

        }

    }
    public void alterarClienteJuridico(String nome,String cnpj,String inscricaoEstudal,String rua, String bairro, String cidade, 
            String cep, String estado,String idCliente) {

        conn = new ConexaoBD().getConexao();
        String sqlCliente = null;
        String sqlClienteJuridico = null;
        String sqlEndereco = null;
        
        try {
            sqlCliente = "UPDATE v_clienteJuridico SET nome='" + nome
                    + "'WHERE id_cliente='" + idCliente + "'";
            
            sqlClienteJuridico = "UPDATE v_clienteJuridico SET cnpj='" + cnpj
                    + "',inscr_estadual='" + inscricaoEstudal
                    + "'WHERE id_cliente='" + idCliente + "'";
            
             sqlEndereco = "UPDATE v_clienteJuridico SET rua='" + rua
                    + "',bairro='" + bairro
                    + "',cidade='" + cidade
                    + "',cep='" + cep
                    + "',estado='" + estado 
                    +"'WHERE id_cliente='" + idCliente + "'";
        
            pstm = conn.prepareStatement(sqlCliente);
            pstm.executeUpdate();
            pstm = conn.prepareStatement(sqlClienteJuridico);
            pstm.executeUpdate();
            pstm = conn.prepareStatement(sqlEndereco);
            pstm.executeUpdate();

            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao atualizar dados:"+e.getMessage());
        }finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    JOptionPane.showMessageDialog(null, "Ops! algo deu errado!");
                }
            }
    }

}
