/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 *
 * @author root
 */
public class Utilitarios {

    public String converteCpf(String cpf) {
        //102.648.834-12
        String parte1 = cpf.substring(0, 3);
        String parte2 = cpf.substring(4, 7);
        String parte3 = cpf.substring(8, 11);
        String parte4 = cpf.substring(12, 14);
        String cpfConvertido = parte1 + parte2 + parte3 + parte4;
        return cpfConvertido;
    }

    public String converteCnpj(String cnpj) {
        //11-111-111/1111-11
        String parte1 = cnpj.substring(0, 2);
        String parte2 = cnpj.substring(3, 6);
        String parte3 = cnpj.substring(7, 10);
        String parte4 = cnpj.substring(11, 15);
        String parte5 = cnpj.substring(15);
        String cpfConvertido = parte1 + parte2 + parte3 + parte4 + parte5;
        return cpfConvertido;
    }

    public String converteData(String dataNascimento) {

        String dia = dataNascimento.substring(0, 2);
        String mes = dataNascimento.substring(3, 5);
        String ano = dataNascimento.substring(6);
        String dataParaSql = ano + "-" + mes + "-" + dia;

        return dataParaSql;

    }

    public String desconverteData(String dataNascimento) {
        String resultado;
        if (dataNascimento.equals("0000-00-00")) {
            resultado = "Não tem";
            return resultado;

        } else {

            String ano = dataNascimento.substring(0, 4);
            String mes = dataNascimento.substring(5, 7);
            String dia = dataNascimento.substring(8);
            String dataParaSql = dia + "/" + mes + "/" + ano;

            return dataParaSql;
        }

    }

    public String convertePlaca(String placa) {

        String parte1 = placa.substring(0, 3);
        String parte2 = placa.substring(4, 8);
        String placaConvertida = parte1 + parte2;

        return placaConvertida;

    }

    public void exportarExcel(JTable table, File file) {
        try {

            WritableWorkbook workbook1 = Workbook.createWorkbook(file);
            WritableSheet sheet1 = workbook1.createSheet("First Sheet", 0);
            TableModel model = table.getModel();

            for (int i = 0; i < model.getColumnCount(); i++) {
                Label column = new Label(i, 0, model.getColumnName(i));
                sheet1.addCell(column);
            }
            int j = 0;
            for (int i = 0; i < model.getRowCount(); i++) {
                for (j = 0; j < model.getColumnCount(); j++) {
                    Label row = new Label(j, i + 1,
                            model.getValueAt(i, j).toString());
                    sheet1.addCell(row);
                }
            }
            workbook1.write();
            workbook1.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //Teste para impressão
    public void printComponenet(final JPanel pane) {
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setJobName(" Print Component ");
        pj.setPrintable(new Printable() {
            public int print(Graphics pg, PageFormat pdf, int pageNum) {
                if (pageNum > 0) {
                    return Printable.NO_SUCH_PAGE;
                }
                Graphics2D g2 = (Graphics2D) pg;
                g2.translate(pdf.getImageableX(), pdf.getImageableY());
                g2.scale(0.7,0.7);
                pane.paint(g2);        // o JPanel aqui
                return Printable.PAGE_EXISTS;
            }
        });
        if (pj.printDialog() == false) {
            return;
        }
        try {
            pj.print();
            JOptionPane.showMessageDialog(null, "Pronto!");
           
        } catch (PrinterException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar imprimir");
           
        }
    }
    //fim impressão
}
