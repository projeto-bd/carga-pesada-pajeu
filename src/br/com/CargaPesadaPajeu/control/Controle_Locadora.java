/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import br.com.CargaPesadaPajeu.dao.Dao_Devolucao;
import br.com.CargaPesadaPajeu.dao.Dao_Locadora;
import br.com.CargaPesadaPajeu.modeller.Modelo_Locadora;
import java.io.File;
import java.sql.ResultSet;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 *
 * @author root
 */
public class Controle_Locadora {

    Dao_Locadora gerenciarLocadora = new Dao_Locadora();
    Dao_Devolucao gerenciarDevolucao = new Dao_Devolucao();

    public boolean cadastrarLocadora(String nome, String cnpj, String rua, String bairro,
            String cidade, String cep, String estado) {

        Modelo_Locadora novaLocadora = new Modelo_Locadora(nome, cnpj, rua, bairro, cidade, cep, estado);

       boolean confirmacao =  gerenciarLocadora.adicionarLocadora(novaLocadora);
       return confirmacao;
    }

    public ResultSet listarLocadora(String consulta) {

        ResultSet rs = gerenciarLocadora.listarLocadora(consulta);

        return rs;

    }

    public int deletarLocadora(String cnpj,int confirmacao) {
        int resposta = gerenciarLocadora.deletarLocadora(cnpj,confirmacao);
        if (resposta == 0) {
            return 0;

        }else{
            return 1;
        }
    }
    public int alterarLocadora(String idLocadora,String nome,String cnpj,String rua,String bairro,String cidade,String cep,String estado){
        int retorno = gerenciarLocadora.alterarLocadora(idLocadora, nome, cnpj, rua, bairro, cidade, cep, estado);
        if(retorno==0){
            return 0;
        }else{
        return 1;
        
        }
   
    }
    
    public void exportarExcel(JTable table, File file) {
        try {

            WritableWorkbook workbook1 = Workbook.createWorkbook(file);
            WritableSheet sheet1 = workbook1.createSheet("First Sheet", 0);
            TableModel model = table.getModel();

            for (int i = 0; i < model.getColumnCount(); i++) {
                Label column = new Label(i, 0, model.getColumnName(i));
                sheet1.addCell(column);
            }
            int j = 0;
            for (int i = 0; i < model.getRowCount(); i++) {
                for (j = 0; j < model.getColumnCount(); j++) {
                    Label row = new Label(j, i + 1,
                            model.getValueAt(i, j).toString());
                    sheet1.addCell(row);
                }
            }
            workbook1.write();
            workbook1.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    boolean alterarStatusVeiculoPdispinvel(String placa){
        boolean confirmacao = gerenciarDevolucao.alteraStatusVeiculoPDispinvel(placa);
        return confirmacao;
    }

}
