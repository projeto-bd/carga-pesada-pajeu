/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

;

import br.com.CargaPesadaPajeu.dao.Dao_Devolucao;
import java.sql.ResultSet;

/**
 *
 * @author root
 */
public class Controle_Devolucao {
    Dao_Devolucao gerenciarDevolucao = new Dao_Devolucao();
    
     public ResultSet preencherCamposReserva(String consulta) {
        ResultSet rs = gerenciarDevolucao.preencherCamposReserva(consulta);
        return rs;
    }
     public boolean finalizarLocacao(String status, int idLocacao, String placa,int KmAtual,float valorTotal){
         boolean confirmacao = gerenciarDevolucao.finalizarLocacao(status, idLocacao, placa, KmAtual, valorTotal);
         return confirmacao;
     
     }
    
}
