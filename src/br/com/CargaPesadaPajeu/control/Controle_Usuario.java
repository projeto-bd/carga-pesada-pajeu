/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import br.com.CargaPesadaPajeu.dao.ConexaoBD;
import br.com.CargaPesadaPajeu.dao.Dao_Usuario;
import br.com.CargaPesadaPajeu.modeller.Usuarios;
import br.com.CargaPesadaPajeu.view.GerencialUsuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Controle_Usuario {

    Connection conn = null;
    PreparedStatement pstm = null;
    public ResultSet rs;
    public Statement st;
    Dao_Usuario gerenciarUsuario = new Dao_Usuario();

    public boolean adicionarUsuario(String nome, String email, String cpf, String senha, String longin) {

        Usuarios adicionarUsuario = new Usuarios(nome, email, cpf, senha, longin);
        boolean confirmacao = gerenciarUsuario.adicionarUsuario(adicionarUsuario);

        return confirmacao;
    }

    public ResultSet listarUsuario(String consulta) {
        ResultSet rs = gerenciarUsuario.listarUsuarios(consulta);
        return rs;
    }

    public boolean deletarUsuario(String deletar, int resposta) {
        boolean confirmacao = gerenciarUsuario.deletarUsuario(deletar, resposta);
        return confirmacao;
    }

    public boolean alterarUsuario(String nome, String email, String cpf,
            String login, String senha, String cpfNovo) {
        boolean confirmacao = gerenciarUsuario.alterarUsuario(nome, email, cpf, login, senha, cpfNovo);

        return confirmacao;
    }

    public boolean veirificaUsuari(String login, String senha) {
        boolean confirmacao = gerenciarUsuario.verificaUsuario(login, senha);
        return confirmacao;
    }

}
