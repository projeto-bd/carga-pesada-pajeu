/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import br.com.CargaPesadaPajeu.dao.Dao_Veiculo;
import br.com.CargaPesadaPajeu.modeller.VeiculoAutomovelPequeno;
import br.com.CargaPesadaPajeu.modeller.VeiculoCaminhoneteCarga;
import br.com.CargaPesadaPajeu.modeller.VeiculoCaminhonetePassageiro;
import java.sql.ResultSet;

/**
 *
 * @author root
 */
public class Controle_Veiculo {

    Dao_Veiculo gerenciarVeiculo = new Dao_Veiculo();

    public ResultSet carregarCategoria() {
        ResultSet rs = gerenciarVeiculo.carregarCategoria();
        if (rs != null) {
            return rs;
        } else {
            return null;
        }
    }

    public ResultSet carregarLocadora() {
        ResultSet rs = gerenciarVeiculo.CarregarLocadora();
        if (rs != null) {
            return rs;
        } else {
            return null;
        }
    }

    public boolean adicionarVeiculoPequeno(boolean radio, boolean mp3, float tamanho, boolean cameraRe,
            boolean direcaoHidraulica, String tipoCambio, boolean arCondicionado, boolean dvd,
            String placa, int numeroChassi, String numeroMotor, String tipoCombustivel, int tork,
            String cor, int kmAtual, int anoModelo, int anoFabricacao, int numeroPortas, String categoria,
            String responsavel, int numeroOcupantes, String revisoes, String limpeza, String status) {

        boolean confirmacao;

        VeiculoAutomovelPequeno veiculoPequeno = new VeiculoAutomovelPequeno(radio, mp3, tamanho, cameraRe,
                direcaoHidraulica, tipoCambio, arCondicionado, dvd, placa, numeroChassi, numeroMotor,
                tipoCombustivel, tork, cor, kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria,
                responsavel, numeroOcupantes, revisoes, limpeza, status);

        if (gerenciarVeiculo.adicionarVeiculoPequeno(veiculoPequeno) == true) {
            return true;

        } else {
            return false;

        }

    }

    public boolean adicionarCaminhoneteCarga(float capacidadeCarga, float volumeCombustivel, float desempenho,
            float distanciaEixos, float potencia, String embreagem, String placa, int numeroChassi,
            String numeroMotor, String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo,
            int anoFabricacao, int numeroPortas, String categoria, String responsavel, int numeroOcupantes,
            String revisoes, String limpeza, String status) {

        boolean confirmacao;

        VeiculoCaminhoneteCarga caminhoneteCarga = new VeiculoCaminhoneteCarga(capacidadeCarga, volumeCombustivel, desempenho,
                distanciaEixos, potencia, embreagem, placa, numeroChassi, numeroMotor, tipoCombustivel,
                tork, cor, kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria, responsavel,
                numeroOcupantes, revisoes, limpeza, status);

        if (gerenciarVeiculo.adicionarCaminhonheteCarga(caminhoneteCarga) == true) {
            return true;

        } else {
            return false;

        }
    }

    public boolean adicionarCaminhonetePassageiro(boolean cintoSeguranca, boolean airbag, boolean rodasLigaLeve,
            boolean direcaoAssistida, boolean controlePoulicao, String placa, int numeroChassi, String numeroMotor,
            String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo, int anoFabricacao,
            int numeroPortas, String categoria, String responsavel, int numeroOcupantes, String revisoes,
            String limpeza, String status) {

        boolean confirmacao;

        VeiculoCaminhonetePassageiro caminhonetePassageiro = new VeiculoCaminhonetePassageiro(cintoSeguranca, airbag,
                rodasLigaLeve, direcaoAssistida, controlePoulicao, placa, numeroChassi, numeroMotor, tipoCombustivel,
                tork, cor, kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria, responsavel, numeroOcupantes,
                revisoes, limpeza, status);

        if (gerenciarVeiculo.adicionarCaminhonetePassageiro(caminhonetePassageiro) == true) {
            return true;

        } else {
            return false;

        }

    }

    public ResultSet listarAutomovelPequeno(String consulta) {
        ResultSet rs = gerenciarVeiculo.listarAutomovelPequeno(consulta);
        return rs;

    }

    public ResultSet listarCaminhoneteCarga(String consulta) {
        ResultSet rs = gerenciarVeiculo.listarCaminhoneteCarga(consulta);
        return rs;
    }

    public ResultSet listarCaminhonetePassageiro(String consulta) {
        ResultSet rs = gerenciarVeiculo.listarCaminhonetePassageiro(consulta);
        return rs;
    }

    public boolean deletarAutoPequeno(String placa, int confirmacao) {
        boolean resposta = gerenciarVeiculo.deletarAutomovelPequeno(placa, confirmacao);
        if (resposta == false) {
            return false;

        } else {
            return true;
        }
    }

    public boolean deletarCamCarga(String placa, int confirmacao) {
        boolean resposta = gerenciarVeiculo.deletarCamCarga(placa, confirmacao);
        if (resposta == false) {
            return false;

        } else {
            return true;
        }
    }

    public boolean deletarCamPassageiro(String placa, int confirmacao) {
        boolean resposta = gerenciarVeiculo.deletarCamPassageiro(placa, confirmacao);
        if (resposta == false) {
            return false;

        } else {
            return true;
        }
    }

    public boolean alterarAutoPequeno(boolean radio, boolean mp3, float tamanho, boolean cameraRe,
            boolean direcaoHidraulica, String tipoCambio, boolean arCondicionado, boolean dvd,
            String placa, int numeroChassi, String numeroMotor, String tipoCombustivel, int tork,
            String cor, int kmAtual, int anoModelo, int anoFabricacao, int numeroPortas, String categoria,
            String responsavel, int numeroOcupantes, String revisoes, String limpeza, String status, String idVeiculo) {

        boolean confirmacao = gerenciarVeiculo.alterarAutoPequeno(radio, mp3, tamanho, cameraRe, direcaoHidraulica,
                tipoCambio, arCondicionado, dvd, placa, numeroChassi, numeroMotor, tipoCombustivel,
                tork, cor, kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria, responsavel,
                numeroOcupantes, revisoes, limpeza, status, idVeiculo);

        return confirmacao;

    }

    public boolean alterarCamCarga(float capacidadeCarga, float volumeCombustivel, float desempenho,
            float distanciaEixos, float potencia, String embreagem, String placa, int numeroChassi,
            String numeroMotor, String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo,
            int anoFabricacao, int numeroPortas, String categoria, String responsavel, int numeroOcupantes,
            String revisoes, String limpeza, String status, String idVeiculo) {

        boolean confirmacao = gerenciarVeiculo.alterarCamCarga(capacidadeCarga, volumeCombustivel, desempenho,
                distanciaEixos, potencia, embreagem, placa, numeroChassi, numeroMotor, tipoCombustivel, tork, cor,
                kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria, responsavel, numeroOcupantes,
                revisoes, limpeza, status, idVeiculo);

        return confirmacao;
    }

    public boolean alterarCamPassageiro(boolean cintoSeguranca, boolean airbag, boolean rodasLigaLeve,
            boolean direcaoAssistida, boolean controlePoulicao, String placa, int numeroChassi, String numeroMotor,
            String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo, int anoFabricacao,
            int numeroPortas, String categoria, String responsavel, int numeroOcupantes, String revisoes,
            String limpeza, String status, String idVeiculo) {

        boolean confirmacao = gerenciarVeiculo.alterarCamPassageiro(cintoSeguranca, airbag, rodasLigaLeve,
                direcaoAssistida, controlePoulicao, placa, numeroChassi, numeroMotor, tipoCombustivel, tork,
                cor, kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria, responsavel, numeroOcupantes,
                revisoes, limpeza, status, idVeiculo);

        return confirmacao;

    }
}
