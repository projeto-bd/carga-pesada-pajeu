/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import br.com.CargaPesadaPajeu.dao.Dao_Locacao;
import br.com.CargaPesadaPajeu.modeller.Locacao;
import java.sql.ResultSet;

/**
 *
 * @author root
 */
public class Controle_Locacao {

    Dao_Locacao gerenciarLocacao = new Dao_Locacao();

    //preenche os campos de reserva
    public ResultSet listarLocacao(String consulta) {
        ResultSet rs = gerenciarLocacao.listarReservas(consulta);
        return rs;
    }

    public ResultSet veiculosDisponiveis(String consulta) {

        ResultSet rs = gerenciarLocacao.veiculosDisponiveis(consulta);
        return rs;
    }
    public ResultSet kmAtual(String placa){
        ResultSet rs = gerenciarLocacao.listaKmAtual(placa);
        return rs;
    }

    public boolean efetuarLocacao(String motorista, String placa_veiculo, int idReserva) {

        Locacao locacao = new Locacao(motorista, placa_veiculo);
        boolean resposta = gerenciarLocacao.efetuarLocacao(locacao, idReserva,placa_veiculo);
        return resposta;
    }

    public ResultSet listarLocacoes(String consulta) {
        ResultSet rs = gerenciarLocacao.listarLocacoes(consulta);
        return rs;
    }

    public boolean deletarReservaLocacao(String deletarLocacao, String IdDeletarReserva,String placa, int resposta) {
        boolean confirmacao = gerenciarLocacao.deletarLocacaoReserva(deletarLocacao, IdDeletarReserva,placa, resposta);
        return confirmacao;
    }

    public int retornaNumeroLocacao() {

        int numeroLocacao = gerenciarLocacao.retornaNumeroLocacao();
        return numeroLocacao;
    }
    public boolean verificaMotorista(String cpf){
        boolean confirmacao = gerenciarLocacao.verificaMotoristas(cpf);
        return confirmacao;
    
    }

}
