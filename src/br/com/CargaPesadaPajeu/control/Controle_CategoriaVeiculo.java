/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.control;

import br.com.CargaPesadaPajeu.dao.Dao_CategoriaVeiculo;
import br.com.CargaPesadaPajeu.modeller.CategoriaVeiculo;
import java.sql.ResultSet;

/**
 *
 * @author root
 */
public class Controle_CategoriaVeiculo {
    int retorno;
    Dao_CategoriaVeiculo gerenciarCategoriaVeiculo = new Dao_CategoriaVeiculo();
    
    
    
    public int adicionarCategoria(String nome,float valor){
        CategoriaVeiculo adicionaCatergoria = new CategoriaVeiculo(nome,valor);
        retorno=gerenciarCategoriaVeiculo.adicionarCategoria(adicionaCatergoria);
        return retorno;
    }
    public ResultSet listarCategoria(String consulta){
        ResultSet rs = gerenciarCategoriaVeiculo.listarCategorias(consulta);
        return rs;
    }
    public int deletarCategoria(String deletar,int resposta){
        int retorno;
        retorno=gerenciarCategoriaVeiculo.exlcuirCategoria(deletar, resposta);
        return retorno;
    
    }
}
