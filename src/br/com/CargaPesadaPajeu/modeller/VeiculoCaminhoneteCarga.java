/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.modeller;

/**
 *
 * @author root
 */
public class VeiculoCaminhoneteCarga extends Veiculo{
    
    private float capacidadeCarga;
    private float volumeCombustivel;
    private float desempenho;
    private float distanciaEixos;
    private float potencia;
    private String embreagem;

    public VeiculoCaminhoneteCarga(float capacidadeCarga, float volumeCombustivel, float desempenho, float distanciaEixos, float potencia, String embreagem, String placa, int numeroChassi, String numeroMotor, String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo, int anoFabricacao, int numeroPortas, String categoria, String responsavel, int numeroOcupantes, String revisoes, String limpeza, String status) {
        super(placa, numeroChassi, numeroMotor, tipoCombustivel, tork, cor, kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria, responsavel, numeroOcupantes, revisoes, limpeza, status);
        this.capacidadeCarga = capacidadeCarga;
        this.volumeCombustivel = volumeCombustivel;
        this.desempenho = desempenho;
        this.distanciaEixos = distanciaEixos;
        this.potencia = potencia;
        this.embreagem = embreagem;
    }

    

   
    public float getCapacidadeCarga() {
        return capacidadeCarga;
    }

    public void setCapacidadeCarga(float capacidadeCarga) {
        this.capacidadeCarga = capacidadeCarga;
    }

    public float getVolumeCombustivel() {
        return volumeCombustivel;
    }

    public void setVolumeCombustivel(float volumeCombustivel) {
        this.volumeCombustivel = volumeCombustivel;
    }

    public float getDesempenho() {
        return desempenho;
    }

    public void setDesempenho(float desempenho) {
        this.desempenho = desempenho;
    }

    public float getDistanciaEixos() {
        return distanciaEixos;
    }

    public void setDistanciaEixos(float distanciaEixos) {
        this.distanciaEixos = distanciaEixos;
    }

    public float getPotencia() {
        return potencia;
    }

    public void setPotencia(float potencia) {
        this.potencia = potencia;
    }

    public String getEmbreagem() {
        return embreagem;
    }

    public void setEmbreagem(String embreagem) {
        this.embreagem = embreagem;
    }
    
    
    
    
    
    
}
