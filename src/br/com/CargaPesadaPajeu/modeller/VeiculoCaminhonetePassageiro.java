/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.modeller;

/**
 *
 * @author root
 */
public class VeiculoCaminhonetePassageiro extends Veiculo {
   
    private boolean cintoSeguranca;
    private boolean airbag;
    private boolean rodasLigaLeve;
    private boolean direcaoAssistida;
    private boolean controlePoulicao; 

    public VeiculoCaminhonetePassageiro(boolean cintoSeguranca, boolean airbag, boolean rodasLigaLeve, boolean direcaoAssistida, boolean controlePoulicao, String placa, int numeroChassi, String numeroMotor, String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo, int anoFabricacao, int numeroPortas, String categoria, String responsavel, int numeroOcupantes, String revisoes, String limpeza, String status) {
        super(placa, numeroChassi, numeroMotor, tipoCombustivel, tork, cor, kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria, responsavel, numeroOcupantes, revisoes, limpeza, status);
        this.cintoSeguranca = cintoSeguranca;
        this.airbag = airbag;
        this.rodasLigaLeve = rodasLigaLeve;
        this.direcaoAssistida = direcaoAssistida;
        this.controlePoulicao = controlePoulicao;
    }

   

  

    public boolean isCintoSeguranca() {
        return cintoSeguranca;
    }

    public void setCintoSeguranca(boolean cintoSeguranca) {
        this.cintoSeguranca = cintoSeguranca;
    }

    public boolean isAirbag() {
        return airbag;
    }

    public void setAirbag(boolean airbag) {
        this.airbag = airbag;
    }

    public boolean isRodasLigaLeve() {
        return rodasLigaLeve;
    }

    public void setRodasLigaLeve(boolean rodasLigaLeve) {
        this.rodasLigaLeve = rodasLigaLeve;
    }

    public boolean isDirecaoAssistida() {
        return direcaoAssistida;
    }

    public void setDirecaoAssistida(boolean direcaoAssistida) {
        this.direcaoAssistida = direcaoAssistida;
    }

    public boolean isControlePoulicao() {
        return controlePoulicao;
    }

    public void setControlePoulicao(boolean controlePoulicao) {
        this.controlePoulicao = controlePoulicao;
    }
    
    
    
    
    
}
