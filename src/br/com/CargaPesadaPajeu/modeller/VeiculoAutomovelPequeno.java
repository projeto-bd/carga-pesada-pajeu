/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.modeller;

/**
 *
 * @author root
 */
public class VeiculoAutomovelPequeno extends Veiculo {

    private boolean radio;
    private boolean mp3;
    private float tamanho;
    private boolean cameraRe;
    private boolean direcaoHidraulica;
    private String tipoCambio;
    private boolean arCondicionado;
    private boolean dvd;

    public VeiculoAutomovelPequeno(boolean radio, boolean mp3, float tamanho, boolean cameraRe, boolean direcaoHidraulica, String tipoCambio, boolean arCondicionado, boolean dvd, String placa, int numeroChassi, String numeroMotor, String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo, int anoFabricacao, int numeroPortas, String categoria, String responsavel, int numeroOcupantes, String revisoes, String limpeza, String status) {
        super(placa, numeroChassi, numeroMotor, tipoCombustivel, tork, cor, kmAtual, anoModelo, anoFabricacao, numeroPortas, categoria, responsavel, numeroOcupantes, revisoes, limpeza, status);
        this.radio = radio;
        this.mp3 = mp3;
        this.tamanho = tamanho;
        this.cameraRe = cameraRe;
        this.direcaoHidraulica = direcaoHidraulica;
        this.tipoCambio = tipoCambio;
        this.arCondicionado = arCondicionado;
        this.dvd = dvd;
    }

    

    

    public boolean isRadio() {
        return radio;
    }

    public void setRadio(boolean radio) {
        this.radio = radio;
    }

    public boolean isMp3() {
        return mp3;
    }

    public void setMp3(boolean mp3) {
        this.mp3 = mp3;
    }

    public float getTamanho() {
        return tamanho;
    }

    public void setTamanho(float tamanho) {
        this.tamanho = tamanho;
    }

    public boolean isCameraRe() {
        return cameraRe;
    }

    public void setCameraRe(boolean cameraRe) {
        this.cameraRe = cameraRe;
    }

    public boolean isDirecaoHidraulica() {
        return direcaoHidraulica;
    }

    public void setDirecaoHidraulica(boolean direcaoHidraulica) {
        this.direcaoHidraulica = direcaoHidraulica;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public boolean isArCondicionado() {
        return arCondicionado;
    }

    public void setArCondicionado(boolean arCondicionado) {
        this.arCondicionado = arCondicionado;
    }

    public boolean isDvd() {
        return dvd;
    }

    public void setDvd(boolean dvd) {
        this.dvd = dvd;
    }
    
    
    
    

}
