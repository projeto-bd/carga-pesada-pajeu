/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.modeller;

/**
 *
 * @author root
 */
public class CategoriaVeiculo {
    String nome;
    float valor;

    public CategoriaVeiculo(String nome,float valor) {
        this.nome = nome;
        this.valor = valor;
    }
    public CategoriaVeiculo(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
    
    
    
}
