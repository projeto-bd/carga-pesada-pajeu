/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.modeller;

import java.util.Date;

/**
 *
 * @author root
 */
public class ClientePessoaFisica extends Cliente {
    
    private String sexo;
    private String dataNascimento;
    private String cpf;
    private String vencimentoHabilitacao;
    private String numeroHabilitacao;

    public ClientePessoaFisica(){
    
    }
    public ClientePessoaFisica(String sexo, String dataNascimento, String cpf, 
            String nome, String nomeRua, String bairro, String cidade, String estado,String cep) {
        super(nome, nomeRua, bairro, cidade, estado,cep);
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
        this.cpf = cpf;
    }
     public ClientePessoaFisica(String sexo, String dataNascimento, String cpf,
             String vencimentoHabilitacao,String numeroHabilitacao, 
             String nome, String nomeRua, String bairro, String cidade, String estado,String cep) {
       
        super(nome, nomeRua, bairro, cidade, estado,cep);
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
        this.cpf = cpf;
        this.vencimentoHabilitacao=vencimentoHabilitacao;
        this.numeroHabilitacao = numeroHabilitacao;
    
     }
    
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getVencimentoHabilitacao() {
        return vencimentoHabilitacao;
    }

    public void setVencimentoHabilitacao(String vencimentoHabilitacao) {
        this.vencimentoHabilitacao = vencimentoHabilitacao;
    }

    public String getNumeroHabilitacao() {
        return numeroHabilitacao;
    }

    public void setNumeroHabilitacao(String numeroHabilitacao) {
        this.numeroHabilitacao = numeroHabilitacao;
    }
    
  
    
}
