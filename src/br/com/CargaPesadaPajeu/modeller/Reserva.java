/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.modeller;

/**
 *
 * @author root
 */
public class Reserva {
    private String dataRetirada;
    private String status;
    private String dataEntrega;
    private int idCliente;
    private String nome_categoria_veiculo;
    private String nomeLocadora;
    private String tipo_cliente;
    private float valor_reserva;
    private String tipo_locacao;

    public Reserva(String dataRetirada, String status, String dataEntrega, 
            int idCliente, String nome_categoria_veiculo, String nomeLocadora, String tipo_cliente,float valorReserva,String tipo_locacao) {
        this.dataRetirada = dataRetirada;
        this.status = status;
        this.dataEntrega = dataEntrega;
        this.idCliente = idCliente;
        this.nome_categoria_veiculo = nome_categoria_veiculo;
        this.nomeLocadora = nomeLocadora;
        this.tipo_cliente = tipo_cliente;
        this.valor_reserva = valorReserva;
        this.tipo_locacao = tipo_locacao;
    }
    public Reserva(){
    
    }

    public String getDataRetirada() {
        return dataRetirada;
    }

    public void setDataRetirada(String dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(String dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNome_categoria_veiculo() {
        return nome_categoria_veiculo;
    }

    public void setNome_categoria_veiculo(String nome_categoria_veiculo) {
        this.nome_categoria_veiculo = nome_categoria_veiculo;
    }

    public String getNomeLocadora() {
        return nomeLocadora;
    }

    public void setNomeLocadora(String nomeLocadora) {
        this.nomeLocadora = nomeLocadora;
    }

    public String getTipo_cliente() {
        return tipo_cliente;
    }

    public void setTipo_cliente(String tipo_cliente) {
        this.tipo_cliente = tipo_cliente;
    }

    public float getValor_reserva() {
        return valor_reserva;
    }

    public void setValor_reserva(float valor_reserva) {
        this.valor_reserva = valor_reserva;
    }

    public String getTipo_locacao() {
        return tipo_locacao;
    }

    public void setTipo_locacao(String tipo_locacao) {
        this.tipo_locacao = tipo_locacao;
    }
    
    
    
}
