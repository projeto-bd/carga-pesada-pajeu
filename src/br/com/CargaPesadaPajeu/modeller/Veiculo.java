/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.modeller;

/**
 *
 * @author root
 */
public class Veiculo {
    
    private String placa;
    private int numeroChassi;
    private String numeroMotor;
    private String tipoCombustivel;
    private int tork;
    private String cor;
    private int kmAtual;
    private int anoModelo;
    private int anoFabricacao;
    private int numeroPortas;
    private String categoria;
    private String responsavel;
    private int numeroOcupantes;
    private String revisoes;
    private String limpeza;
    private String status;

    public Veiculo(String placa, int numeroChassi, String numeroMotor, String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo, int anoFabricacao, int numeroPortas, String categoria, String responsavel, int numeroOcupantes, String revisoes, String limpeza, String status) {
        this.placa = placa;
        this.numeroChassi = numeroChassi;
        this.numeroMotor = numeroMotor;
        this.tipoCombustivel = tipoCombustivel;
        this.tork = tork;
        this.cor = cor;
        this.kmAtual = kmAtual;
        this.anoModelo = anoModelo;
        this.anoFabricacao = anoFabricacao;
        this.numeroPortas = numeroPortas;
        this.categoria = categoria;
        this.responsavel = responsavel;
        this.numeroOcupantes = numeroOcupantes;
        this.revisoes = revisoes;
        this.limpeza = limpeza;
        this.status = status;
    }
    public Veiculo(){
    
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getNumeroChassi() {
        return numeroChassi;
    }

    public void setNumeroChassi(int numeroChassi) {
        this.numeroChassi = numeroChassi;
    }

    public String getNumeroMotor() {
        return numeroMotor;
    }

    public void setNumeroMotor(String numeroMotor) {
        this.numeroMotor = numeroMotor;
    }

    public String getTipoCombustivel() {
        return tipoCombustivel;
    }

    public void setTipoCombustivel(String tipoCombustivel) {
        this.tipoCombustivel = tipoCombustivel;
    }

    public int getTork() {
        return tork;
    }

    public void setTork(int tork) {
        this.tork = tork;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public int getKmAtual() {
        return kmAtual;
    }

    public void setKmAtual(int kmAtual) {
        this.kmAtual = kmAtual;
    }

    public int getAnoModelo() {
        return anoModelo;
    }

    public void setAnoModelo(int anoModelo) {
        this.anoModelo = anoModelo;
    }

    public int getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(int anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public int getNumeroPortas() {
        return numeroPortas;
    }

    public void setNumeroPortas(int numeroPortas) {
        this.numeroPortas = numeroPortas;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public int getNumeroOcupantes() {
        return numeroOcupantes;
    }

    public void setNumeroOcupantes(int numeroOcupantes) {
        this.numeroOcupantes = numeroOcupantes;
    }

    public String getRevisoes() {
        return revisoes;
    }

    public void setRevisoes(String revisoes) {
        this.revisoes = revisoes;
    }

    public String getLimpeza() {
        return limpeza;
    }

    public void setLimpeza(String limpeza) {
        this.limpeza = limpeza;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
}
