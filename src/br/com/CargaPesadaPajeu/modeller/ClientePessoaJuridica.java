/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.modeller;

/**
 *
 * @author root
 */
public class ClientePessoaJuridica extends Cliente{
    private String cnpj;
    private String inscricaoEstadual;

    public String getCnpj() {
        return cnpj;
    }

    public ClientePessoaJuridica(String cnpj, String inscricaoEstadual, String nome, String nomeRua, String bairro, String cidade, String estado,String cep) {
        super(nome, nomeRua, bairro, cidade, estado,cep);
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
    }

    
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

}
