/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.view;

import br.com.CargaPesadaPajeu.control.Controle_Cliente;
import br.com.CargaPesadaPajeu.control.Utilitarios;
import br.com.CargaPesadaPajeu.modeller.ClientePessoaFisica;
import br.com.CargaPesadaPajeu.modeller.ClientePessoaJuridica;
import java.awt.Color;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author root
 */
public class GerenciarCliente extends javax.swing.JFrame {

    DateFormat df = DateFormat.getDateInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Date infoMenu = new Date(System.currentTimeMillis());
    SimpleDateFormat fInfoMenu = new SimpleDateFormat("dd/MM/yyyy");
    String idClienteParaAlterar;

    /**
     * Creates new form CadastroCliente
     */
    public GerenciarCliente() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(Color.decode("#0277bd"));
        this.setResizable(false);
        tbListarClienteFisico.setAutoResizeMode(tbListarClienteFisico.AUTO_RESIZE_OFF);
        tbListarClienteJuridico.setAutoResizeMode(tbListarClienteJuridico.AUTO_RESIZE_OFF);
        jPJuridica.setVisible(false);
        jLCNPJ.setVisible(false);
        jLTabelaCNPJ.setVisible(false);
        listaClienteFisico("");
        listarClienteJuridico("");
        limparCampos();
        this.jMInfoTempo.setText(fInfoMenu.format(infoMenu));

    }

    public void limparCampos() {
        this.txtNome.setText("");
        this.cbEstado.setSelectedIndex(0);
        this.txtCidade.setText("");
        this.txtBairro.setText("");
        this.txtCep.setText("");
        this.txtRua.setText("");
        this.txtCpf.setText("");
        this.txtCnpj.setText("");
        this.txtNhabilitacao.setText("");
        this.txtInsEstadual.setText("");
        this.txtDataNascimento.setDate(null);
        this.txtVencimentoHabilitacao.setDate(null);
        this.jrMasculino.setSelected(true);
        this.jrPessoaFisica.setSelected(true);
        jPJuridica.setVisible(false);
        jPFisica.setVisible(true);
        this.btAtualiar.setEnabled(false);
        this.btSalvar.setEnabled(true);

    }

    public void listaClienteFisico(String consulta) {

        Controle_Cliente listarClientes = new Controle_Cliente();
        Utilitarios converCoes = new Utilitarios();
        DefaultTableModel modelo = new DefaultTableModel();
        String[] retorno = new String[14];

        modelo.addColumn("ID");
        modelo.addColumn("Nome");
        modelo.addColumn("CPF");
        modelo.addColumn("Sexo");
        modelo.addColumn("Data Nascimento");
        modelo.addColumn("CNH");
        modelo.addColumn("Vencimento CNH");
        modelo.addColumn("Rua");
        modelo.addColumn("Bairro");
        modelo.addColumn("Cidade");
        modelo.addColumn("CEP");
        modelo.addColumn("Estado");

        tbListarClienteFisico.setModel(modelo);

        tbListarClienteFisico.getColumnModel().getColumn(0).setPreferredWidth(35);
        tbListarClienteFisico.getColumnModel().getColumn(1).setPreferredWidth(140);
        tbListarClienteFisico.getColumnModel().getColumn(2).setPreferredWidth(120);
        tbListarClienteFisico.getColumnModel().getColumn(3).setPreferredWidth(50);
        tbListarClienteFisico.getColumnModel().getColumn(4).setPreferredWidth(118);
        tbListarClienteFisico.getColumnModel().getColumn(5).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(6).setPreferredWidth(115);
        tbListarClienteFisico.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(8).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(9).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(10).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(11).setPreferredWidth(60);

        listarClientes.listarClienteFisico(consulta);

        try {
            while (listarClientes.rs.next()) {
                retorno[0] = Integer.toString(listarClientes.rs.getInt(1));
                retorno[1] = listarClientes.rs.getString(2);
                retorno[2] = listarClientes.rs.getString(3);
                retorno[3] = listarClientes.rs.getString(4);
                String dataNascimentoConvertida = converCoes.desconverteData(listarClientes.rs.getDate(5).toString());
                retorno[4] = dataNascimentoConvertida;
                String numeroCnh = null;
                if (listarClientes.rs.getString(6) == null || listarClientes.rs.getString(6).equals("")) {
                    retorno[5] = "Não tem";
                } else {
                    retorno[5] = listarClientes.rs.getString(6);
                }
                String dataVenciHabilitacao = converCoes.desconverteData(listarClientes.rs.getString(7).toString());
                retorno[6] = dataVenciHabilitacao;
                retorno[7] = listarClientes.rs.getString(8);
                retorno[8] = listarClientes.rs.getString(9);
                retorno[9] = listarClientes.rs.getString(10);
                retorno[10] = listarClientes.rs.getString(11);
                retorno[11] = listarClientes.rs.getString(12);

                modelo.addRow(retorno);
            }
            tbListarClienteFisico.setModel(modelo);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar informações");
        }
    }

    public void listarClienteJuridico(String consulta) {
        Controle_Cliente listarClientes = new Controle_Cliente();
        DefaultTableModel modelo = new DefaultTableModel();
        String[] retorno = new String[9];

        modelo.addColumn("ID");
        modelo.addColumn("Nome");
        modelo.addColumn("CNPJ");
        modelo.addColumn("Ins.Estadual");
        modelo.addColumn("Rua");
        modelo.addColumn("Bairro");
        modelo.addColumn("Cidade");
        modelo.addColumn("CEP");
        modelo.addColumn("Estado");

        tbListarClienteJuridico.setModel(modelo);

        tbListarClienteJuridico.getColumnModel().getColumn(0).setPreferredWidth(35);
        tbListarClienteJuridico.getColumnModel().getColumn(1).setPreferredWidth(140);
        tbListarClienteJuridico.getColumnModel().getColumn(2).setPreferredWidth(145);
        tbListarClienteJuridico.getColumnModel().getColumn(3).setPreferredWidth(100);
        tbListarClienteJuridico.getColumnModel().getColumn(4).setPreferredWidth(110);
        tbListarClienteJuridico.getColumnModel().getColumn(5).setPreferredWidth(110);
        tbListarClienteJuridico.getColumnModel().getColumn(6).setPreferredWidth(110);
        tbListarClienteJuridico.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListarClienteJuridico.getColumnModel().getColumn(8).setPreferredWidth(60);

        listarClientes.listarClienteJuridico(consulta);

        try {
            while (listarClientes.rs.next()) {
                retorno[0] = Integer.toString(listarClientes.rs.getInt(1));
                retorno[1] = listarClientes.rs.getString(2);
                retorno[2] = listarClientes.rs.getString(3);
                if (listarClientes.rs.getString(4) == null) {
                    retorno[3] = "Isento";
                } else {
                    retorno[3] = listarClientes.rs.getString(4);
                }

                retorno[4] = listarClientes.rs.getString(5);
                retorno[5] = listarClientes.rs.getString(6);
                retorno[6] = listarClientes.rs.getString(7);
                retorno[7] = listarClientes.rs.getString(8);
                retorno[8] = listarClientes.rs.getString(9);

                modelo.addRow(retorno);
            }
            tbListarClienteJuridico.setModel(modelo);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar informações");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgTipoPessoa = new javax.swing.ButtonGroup();
        bgSexo = new javax.swing.ButtonGroup();
        bgTipoPesquisa = new javax.swing.ButtonGroup();
        jPMenuPessoaFisica = new javax.swing.JPopupMenu();
        jMEditarPessoaFisica = new javax.swing.JMenuItem();
        jMenuExcluirPessoaFisica = new javax.swing.JMenuItem();
        jPMenuJuridica = new javax.swing.JPopupMenu();
        jMEditarPessoaJuridica = new javax.swing.JMenuItem();
        jMExcluirPessoaJuridica = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jrPessoaFisica = new javax.swing.JRadioButton();
        jrPessoaJuridica = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtCidade = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cbEstado = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtBairro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtCep = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtRua = new javax.swing.JTextField();
        btSalvar = new javax.swing.JButton();
        jPTiposdePessoa = new javax.swing.JPanel();
        jLFisica = new javax.swing.JLayeredPane();
        jPFisica = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jrMasculino = new javax.swing.JRadioButton();
        jrFeminino = new javax.swing.JRadioButton();
        jLabel12 = new javax.swing.JLabel();
        txtDataNascimento = new com.toedter.calendar.JDateChooser();
        jLabel13 = new javax.swing.JLabel();
        txtNhabilitacao = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtVencimentoHabilitacao = new com.toedter.calendar.JDateChooser();
        txtCpf = new javax.swing.JFormattedTextField();
        jLJuridica = new javax.swing.JLayeredPane();
        jPJuridica = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtInsEstadual = new javax.swing.JTextField();
        jrIsento = new javax.swing.JRadioButton();
        txtCnpj = new javax.swing.JFormattedTextField();
        jPPesquisa = new javax.swing.JPanel();
        btPesquisarCliente = new javax.swing.JButton();
        jRPesquisaCpf = new javax.swing.JRadioButton();
        jRPesquisaCNPJ = new javax.swing.JRadioButton();
        jPTipoPesquisa = new javax.swing.JPanel();
        jLCPF = new javax.swing.JLayeredPane();
        txtPesquisarCPF = new javax.swing.JFormattedTextField();
        jLCNPJ = new javax.swing.JLayeredPane();
        txtPesquisaCNPJ = new javax.swing.JFormattedTextField();
        btCancelar = new javax.swing.JButton();
        btAtualiar = new javax.swing.JButton();
        jPTipoTabela = new javax.swing.JPanel();
        jLTabelaCPF = new javax.swing.JLayeredPane();
        jSPaneCPF = new javax.swing.JScrollPane();
        tbListarClienteFisico = new javax.swing.JTable();
        jLTabelaCNPJ = new javax.swing.JLayeredPane();
        jSPaneCNPJ = new javax.swing.JScrollPane();
        tbListarClienteJuridico = new javax.swing.JTable();
        jMenuBar = new javax.swing.JMenuBar();
        jMenu = new javax.swing.JMenu();
        jMDevolucao = new javax.swing.JMenuItem();
        jMUsuario = new javax.swing.JMenuItem();
        jMCategoria = new javax.swing.JMenuItem();
        jMClientes = new javax.swing.JMenuItem();
        jMLocacao = new javax.swing.JMenuItem();
        jMLocadoras = new javax.swing.JMenuItem();
        jMReservas = new javax.swing.JMenuItem();
        jMVeiculos = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMInicios = new javax.swing.JMenuItem();
        jMenuRelatorios = new javax.swing.JMenu();
        jMCliente = new javax.swing.JMenuItem();
        jMReserva = new javax.swing.JMenuItem();
        jMRelatorioLocacao = new javax.swing.JMenuItem();
        jMInfoTempo = new javax.swing.JMenu();

        jMEditarPessoaFisica.setText("Editar");
        jMEditarPessoaFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMEditarPessoaFisicaActionPerformed(evt);
            }
        });
        jPMenuPessoaFisica.add(jMEditarPessoaFisica);

        jMenuExcluirPessoaFisica.setText("Excluir");
        jMenuExcluirPessoaFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuExcluirPessoaFisicaActionPerformed(evt);
            }
        });
        jPMenuPessoaFisica.add(jMenuExcluirPessoaFisica);

        jMEditarPessoaJuridica.setText("Editar");
        jMEditarPessoaJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMEditarPessoaJuridicaActionPerformed(evt);
            }
        });
        jPMenuJuridica.add(jMEditarPessoaJuridica);

        jMExcluirPessoaJuridica.setText("Excluir");
        jMExcluirPessoaJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMExcluirPessoaJuridicaActionPerformed(evt);
            }
        });
        jPMenuJuridica.add(jMExcluirPessoaJuridica);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gerenciar Clientes");

        jLabel1.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Cliente:");

        bgTipoPessoa.add(jrPessoaFisica);
        jrPessoaFisica.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrPessoaFisica.setForeground(new java.awt.Color(255, 255, 255));
        jrPessoaFisica.setSelected(true);
        jrPessoaFisica.setText("Pessoa Fisíca");
        jrPessoaFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrPessoaFisicaActionPerformed(evt);
            }
        });

        bgTipoPessoa.add(jrPessoaJuridica);
        jrPessoaJuridica.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrPessoaJuridica.setForeground(new java.awt.Color(255, 255, 255));
        jrPessoaJuridica.setText("Pessoa Jurídica");
        jrPessoaJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrPessoaJuridicaActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nome:");

        jLabel3.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Estado:");

        jLabel4.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Cidade:");

        cbEstado.setBackground(new java.awt.Color(255, 255, 255));
        cbEstado.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        cbEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO" }));

        jLabel5.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Bairro:");

        jLabel6.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Cep:");

        jLabel7.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Rua:");

        btSalvar.setBackground(new java.awt.Color(255, 255, 255));
        btSalvar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        btSalvar.setText("Salvar");
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        jPTiposdePessoa.setBackground(new java.awt.Color(2, 119, 189));

        jPFisica.setBackground(new java.awt.Color(2, 119, 189));

        jLabel10.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("CPF:");

        jLabel11.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Sexo:");

        bgSexo.add(jrMasculino);
        jrMasculino.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrMasculino.setForeground(new java.awt.Color(255, 255, 255));
        jrMasculino.setSelected(true);
        jrMasculino.setText("M");

        bgSexo.add(jrFeminino);
        jrFeminino.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrFeminino.setForeground(new java.awt.Color(255, 255, 255));
        jrFeminino.setText("F");

        jLabel12.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Data Nascimento:");

        jLabel13.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Nº Habilitação:");

        jLabel14.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Vencimento Habilitação:");

        try {
            txtCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPFisicaLayout = new javax.swing.GroupLayout(jPFisica);
        jPFisica.setLayout(jPFisicaLayout);
        jPFisicaLayout.setHorizontalGroup(
            jPFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFisicaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPFisicaLayout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDataNascimento, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                        .addGap(8, 8, 8)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jrMasculino)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jrFeminino)
                        .addContainerGap())
                    .addGroup(jPFisicaLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNhabilitacao, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtVencimentoHabilitacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(25, 25, 25))))
        );
        jPFisicaLayout.setVerticalGroup(
            jPFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPFisicaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(jrMasculino)
                        .addComponent(jrFeminino))
                    .addComponent(txtDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10)
                        .addComponent(jLabel12)
                        .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(txtNhabilitacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14))
                    .addComponent(txtVencimentoHabilitacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLFisica.setLayer(jPFisica, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLFisicaLayout = new javax.swing.GroupLayout(jLFisica);
        jLFisica.setLayout(jLFisicaLayout);
        jLFisicaLayout.setHorizontalGroup(
            jLFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFisica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jLFisicaLayout.setVerticalGroup(
            jLFisicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPFisica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPJuridica.setBackground(new java.awt.Color(2, 119, 189));

        jLabel8.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("CNPJ:");

        jLabel9.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Ins. Estadual:");

        jrIsento.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrIsento.setForeground(new java.awt.Color(255, 255, 255));
        jrIsento.setText("Isento");

        try {
            txtCnpj.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPJuridicaLayout = new javax.swing.GroupLayout(jPJuridica);
        jPJuridica.setLayout(jPJuridicaLayout);
        jPJuridicaLayout.setHorizontalGroup(
            jPJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPJuridicaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCnpj, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtInsEstadual, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jrIsento)
                .addGap(31, 31, 31))
        );
        jPJuridicaLayout.setVerticalGroup(
            jPJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPJuridicaLayout.createSequentialGroup()
                .addGroup(jPJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrIsento)
                    .addComponent(jLabel9)
                    .addComponent(txtInsEstadual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(txtCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 42, Short.MAX_VALUE))
        );

        jLJuridica.setLayer(jPJuridica, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLJuridicaLayout = new javax.swing.GroupLayout(jLJuridica);
        jLJuridica.setLayout(jLJuridicaLayout);
        jLJuridicaLayout.setHorizontalGroup(
            jLJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPJuridica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jLJuridicaLayout.setVerticalGroup(
            jLJuridicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPJuridica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPTiposdePessoaLayout = new javax.swing.GroupLayout(jPTiposdePessoa);
        jPTiposdePessoa.setLayout(jPTiposdePessoaLayout);
        jPTiposdePessoaLayout.setHorizontalGroup(
            jPTiposdePessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPTiposdePessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTiposdePessoaLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLFisica)
                    .addContainerGap()))
            .addGroup(jPTiposdePessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTiposdePessoaLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLJuridica)
                    .addContainerGap()))
        );
        jPTiposdePessoaLayout.setVerticalGroup(
            jPTiposdePessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 73, Short.MAX_VALUE)
            .addGroup(jPTiposdePessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTiposdePessoaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLFisica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(jPTiposdePessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTiposdePessoaLayout.createSequentialGroup()
                    .addGap(0, 2, Short.MAX_VALUE)
                    .addComponent(jLJuridica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 2, Short.MAX_VALUE)))
        );

        jPPesquisa.setBackground(new java.awt.Color(2, 119, 189));
        jPPesquisa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        btPesquisarCliente.setBackground(new java.awt.Color(255, 255, 255));
        btPesquisarCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btPesquisarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/pesquisar.png"))); // NOI18N
        btPesquisarCliente.setText("Pesquisar");
        btPesquisarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btPesquisarClienteActionPerformed(evt);
            }
        });

        bgTipoPesquisa.add(jRPesquisaCpf);
        jRPesquisaCpf.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jRPesquisaCpf.setForeground(new java.awt.Color(255, 255, 255));
        jRPesquisaCpf.setSelected(true);
        jRPesquisaCpf.setText("CPF");
        jRPesquisaCpf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRPesquisaCpfActionPerformed(evt);
            }
        });

        bgTipoPesquisa.add(jRPesquisaCNPJ);
        jRPesquisaCNPJ.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jRPesquisaCNPJ.setForeground(new java.awt.Color(255, 255, 255));
        jRPesquisaCNPJ.setText("CNPJ");
        jRPesquisaCNPJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRPesquisaCNPJActionPerformed(evt);
            }
        });

        jPTipoPesquisa.setBackground(new java.awt.Color(2, 119, 189));

        try {
            txtPesquisarCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLCPF.setLayer(txtPesquisarCPF, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLCPFLayout = new javax.swing.GroupLayout(jLCPF);
        jLCPF.setLayout(jLCPFLayout);
        jLCPFLayout.setHorizontalGroup(
            jLCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 147, Short.MAX_VALUE)
            .addGroup(jLCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLCPFLayout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(txtPesquisarCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(21, Short.MAX_VALUE)))
        );
        jLCPFLayout.setVerticalGroup(
            jLCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 38, Short.MAX_VALUE)
            .addGroup(jLCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLCPFLayout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(txtPesquisarCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        try {
            txtPesquisaCNPJ.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLCNPJ.setLayer(txtPesquisaCNPJ, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLCNPJLayout = new javax.swing.GroupLayout(jLCNPJ);
        jLCNPJ.setLayout(jLCNPJLayout);
        jLCNPJLayout.setHorizontalGroup(
            jLCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 147, Short.MAX_VALUE)
            .addGroup(jLCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLCNPJLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(txtPesquisaCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(20, Short.MAX_VALUE)))
        );
        jLCNPJLayout.setVerticalGroup(
            jLCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jLCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLCNPJLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(txtPesquisaCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPTipoPesquisaLayout = new javax.swing.GroupLayout(jPTipoPesquisa);
        jPTipoPesquisa.setLayout(jPTipoPesquisaLayout);
        jPTipoPesquisaLayout.setHorizontalGroup(
            jPTipoPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPTipoPesquisaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLCNPJ))
            .addGroup(jPTipoPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPTipoPesquisaLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLCPF)))
        );
        jPTipoPesquisaLayout.setVerticalGroup(
            jPTipoPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPTipoPesquisaLayout.createSequentialGroup()
                .addComponent(jLCNPJ)
                .addContainerGap())
            .addGroup(jPTipoPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTipoPesquisaLayout.createSequentialGroup()
                    .addComponent(jLCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 21, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPPesquisaLayout = new javax.swing.GroupLayout(jPPesquisa);
        jPPesquisa.setLayout(jPPesquisaLayout);
        jPPesquisaLayout.setHorizontalGroup(
            jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPesquisaLayout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(jRPesquisaCpf)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jRPesquisaCNPJ)
                .addGap(14, 14, 14)
                .addComponent(jPTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btPesquisarCliente)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPPesquisaLayout.setVerticalGroup(
            jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPesquisaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btPesquisarCliente)
                    .addGroup(jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jPTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jRPesquisaCNPJ)
                            .addComponent(jRPesquisaCpf))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btCancelar.setBackground(new java.awt.Color(255, 255, 255));
        btCancelar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cancelar.png"))); // NOI18N
        btCancelar.setText("Cancelar");
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        btAtualiar.setBackground(new java.awt.Color(255, 255, 255));
        btAtualiar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btAtualiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/atualizar.png"))); // NOI18N
        btAtualiar.setText("Atualizar");
        btAtualiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAtualiarActionPerformed(evt);
            }
        });

        jPTipoTabela.setBackground(new java.awt.Color(2, 119, 189));

        tbListarClienteFisico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbListarClienteFisico.setComponentPopupMenu(jPMenuPessoaFisica);
        jSPaneCPF.setViewportView(tbListarClienteFisico);

        jLTabelaCPF.setLayer(jSPaneCPF, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLTabelaCPFLayout = new javax.swing.GroupLayout(jLTabelaCPF);
        jLTabelaCPF.setLayout(jLTabelaCPFLayout);
        jLTabelaCPFLayout.setHorizontalGroup(
            jLTabelaCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 640, Short.MAX_VALUE)
            .addGroup(jLTabelaCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSPaneCPF, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE))
        );
        jLTabelaCPFLayout.setVerticalGroup(
            jLTabelaCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 168, Short.MAX_VALUE)
            .addGroup(jLTabelaCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLTabelaCPFLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jSPaneCPF, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)))
        );

        tbListarClienteJuridico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbListarClienteJuridico.setComponentPopupMenu(jPMenuJuridica);
        jSPaneCNPJ.setViewportView(tbListarClienteJuridico);

        jLTabelaCNPJ.setLayer(jSPaneCNPJ, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLTabelaCNPJLayout = new javax.swing.GroupLayout(jLTabelaCNPJ);
        jLTabelaCNPJ.setLayout(jLTabelaCNPJLayout);
        jLTabelaCNPJLayout.setHorizontalGroup(
            jLTabelaCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 640, Short.MAX_VALUE)
            .addGroup(jLTabelaCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSPaneCNPJ, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE))
        );
        jLTabelaCNPJLayout.setVerticalGroup(
            jLTabelaCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 164, Short.MAX_VALUE)
            .addGroup(jLTabelaCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLTabelaCNPJLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jSPaneCNPJ, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPTipoTabelaLayout = new javax.swing.GroupLayout(jPTipoTabela);
        jPTipoTabela.setLayout(jPTipoTabelaLayout);
        jPTipoTabelaLayout.setHorizontalGroup(
            jPTipoTabelaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLTabelaCNPJ)
            .addGroup(jPTipoTabelaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLTabelaCPF, javax.swing.GroupLayout.Alignment.TRAILING))
        );
        jPTipoTabelaLayout.setVerticalGroup(
            jPTipoTabelaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLTabelaCNPJ)
            .addGroup(jPTipoTabelaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLTabelaCPF, javax.swing.GroupLayout.Alignment.TRAILING))
        );

        jMenu.setBackground(new java.awt.Color(255, 255, 255));
        jMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Menu.png"))); // NOI18N
        jMenu.setText("Menu");
        jMenu.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMDevolucao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMDevolucao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/devolucao.png"))); // NOI18N
        jMDevolucao.setText("Efetuar Devolução");
        jMDevolucao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMDevolucaoActionPerformed(evt);
            }
        });
        jMenu.add(jMDevolucao);

        jMUsuario.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/AdicionarFuncionario.png"))); // NOI18N
        jMUsuario.setText("Gerenciar Usuários");
        jMUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMUsuarioActionPerformed(evt);
            }
        });
        jMenu.add(jMUsuario);

        jMCategoria.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCategoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarCategoria.png"))); // NOI18N
        jMCategoria.setText("Gerenciar Categoria");
        jMCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMCategoriaActionPerformed(evt);
            }
        });
        jMenu.add(jMCategoria);

        jMClientes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/login.png"))); // NOI18N
        jMClientes.setText("Gerenciar Clientes");
        jMClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClientesActionPerformed(evt);
            }
        });
        jMenu.add(jMClientes);

        jMLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocao.png"))); // NOI18N
        jMLocacao.setText("Gerenciar Locações");
        jMLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocacaoActionPerformed(evt);
            }
        });
        jMenu.add(jMLocacao);

        jMLocadoras.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocadoras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocadoras.png"))); // NOI18N
        jMLocadoras.setText("Gerenciar Locadoras");
        jMLocadoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocadorasActionPerformed(evt);
            }
        });
        jMenu.add(jMLocadoras);

        jMReservas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarReserva.png"))); // NOI18N
        jMReservas.setText("Gerenciar Reservas");
        jMReservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservasActionPerformed(evt);
            }
        });
        jMenu.add(jMReservas);

        jMVeiculos.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMVeiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarVeiculo.png"))); // NOI18N
        jMVeiculos.setText("Gerenciar Veículos");
        jMVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMVeiculosActionPerformed(evt);
            }
        });
        jMenu.add(jMVeiculos);
        jMenu.add(jSeparator2);

        jMInicios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMInicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/home.png"))); // NOI18N
        jMInicios.setText("Tela Principal");
        jMInicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIniciosActionPerformed(evt);
            }
        });
        jMenu.add(jMInicios);

        jMenuBar.add(jMenu);

        jMenuRelatorios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/MenuRelatorios.png"))); // NOI18N
        jMenuRelatorios.setText("Relatórios");
        jMenuRelatorios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cliente.png"))); // NOI18N
        jMCliente.setText("Clientes");
        jMCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClienteActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMCliente);

        jMReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioReservas.png"))); // NOI18N
        jMReserva.setText("Reservas");
        jMReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservaActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMReserva);

        jMRelatorioLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMRelatorioLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioLocacao.png"))); // NOI18N
        jMRelatorioLocacao.setText("Locações");
        jMRelatorioLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMRelatorioLocacaoActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMRelatorioLocacao);

        jMenuBar.add(jMenuRelatorios);

        jMInfoTempo.setForeground(new java.awt.Color(0, 0, 0));
        jMInfoTempo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dataMenu.png"))); // NOI18N
        jMInfoTempo.setText("tempo");
        jMInfoTempo.setEnabled(false);
        jMInfoTempo.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jMenuBar.add(jMInfoTempo);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtBairro)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(cbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel4)))
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCidade)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtCep, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtRua))))
                            .addComponent(txtNome))
                        .addGap(27, 27, 27))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPPesquisa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPTiposdePessoa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jrPessoaFisica)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jrPessoaJuridica)
                        .addGap(129, 129, 129))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(139, Short.MAX_VALUE)
                .addComponent(btSalvar)
                .addGap(18, 18, 18)
                .addComponent(btAtualiar)
                .addGap(18, 18, 18)
                .addComponent(btCancelar)
                .addGap(105, 105, 105))
            .addComponent(jPTipoTabela, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jrPessoaFisica)
                    .addComponent(jrPessoaJuridica))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(cbEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtCep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txtRua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPTiposdePessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSalvar)
                    .addComponent(btCancelar)
                    .addComponent(btAtualiar))
                .addGap(30, 30, 30)
                .addComponent(jPPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPTipoTabela, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jrPessoaFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrPessoaFisicaActionPerformed
        // TODO add your handling code here:
        if (jrPessoaFisica.isSelected()) {
            jPJuridica.setVisible(false);
            jPFisica.setVisible(true);

        }
    }//GEN-LAST:event_jrPessoaFisicaActionPerformed

    private void jrPessoaJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrPessoaJuridicaActionPerformed
        // TODO add your handling code here:
        if (jrPessoaJuridica.isSelected()) {
            jPJuridica.setVisible(true);
            jPFisica.setVisible(false);

        }
    }//GEN-LAST:event_jrPessoaJuridicaActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        // TODO add your handling code here:
        Utilitarios converCoes = new Utilitarios();
        if (jrPessoaJuridica.isSelected()) {
            String nome = this.txtNome.getText();
            String nomeRua = this.txtRua.getText();
            String bairro = this.txtBairro.getText();
            String cidade = this.txtBairro.getText();
            String estado = this.cbEstado.getSelectedItem().toString();
            String cnpj = this.txtCnpj.getText();
            String cep = this.txtCep.getText();
            String inscricaoEstadual = this.txtInsEstadual.getText();
            if (jrIsento.isSelected()) {
                inscricaoEstadual = null;
            }

            ClientePessoaJuridica clienteJuridico = new ClientePessoaJuridica(cnpj, inscricaoEstadual,
                    nome, nomeRua, bairro, cidade, estado, cep);

            Controle_Cliente novoClienteJuridico = new Controle_Cliente();

            novoClienteJuridico.adicionarClienteJuridico(clienteJuridico);

        } else if (jrPessoaFisica.isSelected()) {
            try {

                if (this.txtNhabilitacao.getText().isEmpty() && this.txtVencimentoHabilitacao.getDate() == null) {

                    String nome = this.txtNome.getText();
                    String nomeRua = this.txtRua.getText();
                    String bairro = this.txtBairro.getText();
                    String cidade = this.txtCidade.getText();
                    String estado = this.cbEstado.getSelectedItem().toString();
                    String cep = this.txtCep.getText();
                    String sexo;
                    String cpf = this.txtCpf.getText();
                    String dataNascimento = null;

                    if (jrMasculino.isSelected()) {
                        sexo = this.jrMasculino.getText();
                    } else {
                        sexo = this.jrFeminino.getText();
                    }
                    if (this.txtDataNascimento.getDate() != null) {
                        dataNascimento = df.format(txtDataNascimento.getDate());
                        Controle_Cliente novoClienteFisico = new Controle_Cliente();
                        String dataParaSql = converCoes.converteData(dataNascimento);

                        ClientePessoaFisica clienteFisico = new ClientePessoaFisica(sexo, dataParaSql, cpf, "0000-00-00", "Não tem", nome,
                                nomeRua, bairro, cidade, estado, cep);
                        novoClienteFisico.adicionarClienteFisico(clienteFisico);

                    } else {
                        JOptionPane.showMessageDialog(null, "Verifique a data de nascimento!");
                    }

                } else if (this.txtNhabilitacao.getText().isEmpty() || this.txtVencimentoHabilitacao.getDate() == null) {
                    JOptionPane.showMessageDialog(null, "Ops está faltando informações sobre a CNH");
                } else if (this.txtVencimentoHabilitacao.getDate() != null) {

                    String nome = this.txtNome.getText();
                    String nomeRua = this.txtRua.getText();
                    String bairro = this.txtBairro.getText();
                    String cidade = this.txtBairro.getText();
                    String estado = this.cbEstado.getSelectedItem().toString();
                    String cep = this.txtCep.getText();
                    String sexo;
                    String cpf = this.txtCpf.getText();
                    String dataNascimento = null;
                    String vencimentoHabilitacao = null;
                    String numHabilitacao = this.txtNhabilitacao.getText();

                    if (jrMasculino.isSelected()) {
                        sexo = this.jrMasculino.getText();
                    } else {
                        sexo = this.jrFeminino.getText();
                    }
                    if (this.txtDataNascimento.getDate() != null && this.txtVencimentoHabilitacao.getDate() != null) {
                        dataNascimento = df.format(txtDataNascimento.getDate());
                        vencimentoHabilitacao = df.format(txtVencimentoHabilitacao.getDate());
                        Controle_Cliente novoClienteFisico = new Controle_Cliente();
                        String dataNascimentoParaSql = converCoes.converteData(dataNascimento);
                        String dataVencimentoHabilit = converCoes.converteData(vencimentoHabilitacao);

                        ClientePessoaFisica clienteFisico = new ClientePessoaFisica(sexo, dataNascimentoParaSql, cpf,
                                dataVencimentoHabilit, numHabilitacao, nome, nomeRua, bairro, cidade, estado, cep);
                        novoClienteFisico.adicionarClienteFisico(clienteFisico);

                    } else {
                        JOptionPane.showMessageDialog(null, "Verifique a data de nascimento!");
                    }

                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Algo deu errado:" + e.getMessage());
            }

        }
        listaClienteFisico("");
        limparCampos();
    }//GEN-LAST:event_btSalvarActionPerformed

    private void btAtualiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAtualiarActionPerformed
        // TODO add your handling code here:
        if (jrPessoaFisica.isSelected()) {
            Utilitarios convercoes = new Utilitarios();
            String vencimentoHabilitacao = null;
            String dataConverHabilitacao;
            String numHabilitacao;
            numHabilitacao = this.txtNhabilitacao.getText();
            if (this.txtVencimentoHabilitacao.getDate() == null) {
                vencimentoHabilitacao = "0000-00-00";
            } else {
                vencimentoHabilitacao = df.format(txtVencimentoHabilitacao.getDate());
                dataConverHabilitacao = convercoes.converteData(vencimentoHabilitacao);
                vencimentoHabilitacao = dataConverHabilitacao;

            }
            if (this.txtNhabilitacao.getText().equals("") || this.txtNhabilitacao.getText() == null) {
                numHabilitacao = null;
            }

            Controle_Cliente editarClienteFisico = new Controle_Cliente();
            String nome = this.txtNome.getText();
            String nomeRua = this.txtRua.getText();
            String bairro = this.txtBairro.getText();
            String cidade = this.txtBairro.getText();
            String estado = this.cbEstado.getSelectedItem().toString();
            String cep = this.txtCep.getText();
            String sexo;
            String cpf = this.txtCpf.getText();
            String dataNascimento = null;

            if (jrMasculino.isSelected()) {
                sexo = this.jrMasculino.getText();
            } else {
                sexo = this.jrFeminino.getText();
            }
            if (this.txtDataNascimento.getDate() != null) {
                dataNascimento = df.format(txtDataNascimento.getDate());

                String dataNascimentoParaSql = convercoes.converteData(dataNascimento);

                editarClienteFisico.alterarClienteFisico(nome, cpf, sexo, dataNascimentoParaSql, numHabilitacao,
                        vencimentoHabilitacao, nomeRua, bairro, cidade, cep, estado, idClienteParaAlterar);

            } else {
                JOptionPane.showMessageDialog(null, "Verifique a data de nascimento!");
            }
            listaClienteFisico("");
        } else if (jrPessoaJuridica.isSelected()) {
            String nome = this.txtNome.getText();
            String nomeRua = this.txtRua.getText();
            String bairro = this.txtBairro.getText();
            String cidade = this.txtCidade.getText();
            String estado = this.cbEstado.getSelectedItem().toString();
            String cnpj = this.txtCnpj.getText();
            String cep = this.txtCep.getText();
            String inscricaoEstadual = this.txtInsEstadual.getText();
            if (jrIsento.isSelected()) {
                inscricaoEstadual = null;
            }
            Controle_Cliente novoClienteJuridico = new Controle_Cliente();

            novoClienteJuridico.alterarClienteJuridico(nome, cnpj, inscricaoEstadual, cep, bairro,
                    cidade, cep, estado, idClienteParaAlterar);

        }
        limparCampos();

    }//GEN-LAST:event_btAtualiarActionPerformed

    private void jRPesquisaCpfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRPesquisaCpfActionPerformed
        // TODO add your handling code here:
        if (jRPesquisaCpf.isSelected()) {
            listaClienteFisico("");
            jLCNPJ.setVisible(false);
            jLTabelaCNPJ.setVisible(false);

            jLCPF.setVisible(true);
            jLTabelaCPF.setVisible(true);
            txtPesquisaCNPJ.setText("");
        }
    }//GEN-LAST:event_jRPesquisaCpfActionPerformed

    private void jRPesquisaCNPJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRPesquisaCNPJActionPerformed
        // TODO add your handling code here:
        listarClienteJuridico("");
        jLCNPJ.setVisible(true);
        jLTabelaCNPJ.setVisible(true);

        jLCPF.setVisible(false);
        jLTabelaCPF.setVisible(false);
        txtPesquisarCPF.setText("");
    }//GEN-LAST:event_jRPesquisaCNPJActionPerformed

    private void jMenuExcluirPessoaFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuExcluirPessoaFisicaActionPerformed
        // TODO add your handling code here:
        Controle_Cliente deletarCliente = new Controle_Cliente();
        int itemSelecionado = tbListarClienteFisico.getSelectedRow();
        if (itemSelecionado >= 0) {
            String deletar = tbListarClienteFisico.getValueAt(itemSelecionado, 2).toString();
            int id = Integer.parseInt(tbListarClienteFisico.getValueAt(itemSelecionado, 0).toString());
            Object[] botoes = {"Sim", "Não"};
            int resposta = JOptionPane.showOptionDialog(null,
                    "Deseja Excluir esse item?",
                    "Confirmação", // o título da janela
                    JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    botoes, botoes[0]);
            if (resposta == 0) {
                deletarCliente.deletarClienteFisico(deletar, resposta, id);
                JOptionPane.showMessageDialog(null, "Excluído!");
                listaClienteFisico("");
            }

        } else {

            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }
    }//GEN-LAST:event_jMenuExcluirPessoaFisicaActionPerformed

    private void btPesquisarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btPesquisarClienteActionPerformed
        // TODO add your handling code here:
        if (jRPesquisaCpf.isSelected()) {
            listaClienteFisico(this.txtPesquisarCPF.getText());

        } else if (jRPesquisaCNPJ.isSelected()) {
            listarClienteJuridico(this.txtPesquisaCNPJ.getText());
        } else {
            JOptionPane.showMessageDialog(null, "Selecione uma opção!");

        }
    }//GEN-LAST:event_btPesquisarClienteActionPerformed

    private void jMExcluirPessoaJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMExcluirPessoaJuridicaActionPerformed
        // TODO add your handling code here:
        Controle_Cliente deletarCliente = new Controle_Cliente();
        int itemSelecionado = tbListarClienteJuridico.getSelectedRow();
        if (itemSelecionado >= 0) {
            String deletar = tbListarClienteJuridico.getValueAt(itemSelecionado, 2).toString();
            int id = Integer.parseInt(tbListarClienteJuridico.getValueAt(itemSelecionado, 0).toString());
            Object[] botoes = {"Sim", "Não"};
            int resposta = JOptionPane.showOptionDialog(null,
                    "Deseja Excluir esse item?",
                    "Confirmação", // o título da janela
                    JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    botoes, botoes[0]);
            if (resposta == 0) {
                deletarCliente.deletarClienteFisico(deletar, resposta, id);
                JOptionPane.showMessageDialog(null, "Excluído!");
                listarClienteJuridico("");
            }

        } else {

            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }

    }//GEN-LAST:event_jMExcluirPessoaJuridicaActionPerformed

    private void jMEditarPessoaFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMEditarPessoaFisicaActionPerformed
        // TODO add your handling code here:
        this.btAtualiar.setEnabled(true);
        this.btSalvar.setEnabled(false);
        jrPessoaFisica.setSelected(true);

        Utilitarios convercoes = new Utilitarios();
        int itemSelecionado = tbListarClienteFisico.getSelectedRow();
        idClienteParaAlterar = tbListarClienteFisico.getValueAt(itemSelecionado, 0).toString();
        if (itemSelecionado >= 0) {
            this.txtNome.setText(tbListarClienteFisico.getValueAt(itemSelecionado, 1).toString());
            String cpf = convercoes.converteCpf(tbListarClienteFisico.getValueAt(itemSelecionado, 2).toString());
            this.txtCpf.setText(cpf);
            if (tbListarClienteFisico.getValueAt(itemSelecionado, 3).toString().equals("M")) {
                this.jrMasculino.setSelected(true);
            } else {
                this.jrFeminino.setSelected(true);
            }
            String dataNascimento = tbListarClienteFisico.getValueAt(itemSelecionado, 4).toString();
            Date dataRecebidaConvertida = null;
            String vencimentoHabilitacao = tbListarClienteFisico.getValueAt(itemSelecionado, 6).toString();
            Date vencimentoConvertido = null;
            try {
                dataRecebidaConvertida = formatter.parse(dataNascimento);
                if (vencimentoHabilitacao.equals("Não tem")) {
                    vencimentoConvertido = null;
                } else {
                    vencimentoConvertido = formatter.parse(vencimentoHabilitacao);
                }
            } catch (ParseException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao converter data!");
            }
            this.txtDataNascimento.setDate(dataRecebidaConvertida);
            this.txtNhabilitacao.setText(tbListarClienteFisico.getValueAt(itemSelecionado, 5).toString());
            this.txtVencimentoHabilitacao.setDate(vencimentoConvertido);
            this.txtRua.setText(tbListarClienteFisico.getValueAt(itemSelecionado, 7).toString());
            this.txtBairro.setText(tbListarClienteFisico.getValueAt(itemSelecionado, 8).toString());
            this.txtCidade.setText(tbListarClienteFisico.getValueAt(itemSelecionado, 9).toString());
            this.txtCep.setText(tbListarClienteFisico.getValueAt(itemSelecionado, 10).toString());
            this.cbEstado.setSelectedItem(tbListarClienteFisico.getValueAt(itemSelecionado, 11).toString());

        } else {

            JOptionPane.showMessageDialog(null, "Item não selecionado!");

        }

    }//GEN-LAST:event_jMEditarPessoaFisicaActionPerformed

    private void jMEditarPessoaJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMEditarPessoaJuridicaActionPerformed
        // TODO add your handling code here:
        this.btAtualiar.setEnabled(true);
        this.btSalvar.setEnabled(false);

        jPJuridica.setVisible(true);
        jPFisica.setVisible(false);
        jrPessoaJuridica.setSelected(true);

        Utilitarios convercoes = new Utilitarios();
        int itemSelecionado = tbListarClienteJuridico.getSelectedRow();
        idClienteParaAlterar = tbListarClienteJuridico.getValueAt(itemSelecionado, 0).toString();
        if (itemSelecionado >= 0) {
            this.txtNome.setText(tbListarClienteJuridico.getValueAt(itemSelecionado, 1).toString());

            String cnpj = convercoes.converteCnpj(tbListarClienteJuridico.getValueAt(itemSelecionado, 2).toString());
            this.txtCnpj.setText(cnpj);
            if (tbListarClienteJuridico.getValueAt(itemSelecionado, 3).toString().equals("Isento")) {
                this.jrIsento.setSelected(true);
            } else {
                this.txtInsEstadual.setText(tbListarClienteJuridico.getValueAt(itemSelecionado, 3).toString());
                this.jrIsento.setSelected(false);

            }

            this.txtRua.setText(tbListarClienteJuridico.getValueAt(itemSelecionado, 4).toString());
            this.txtBairro.setText(tbListarClienteJuridico.getValueAt(itemSelecionado, 5).toString());
            this.txtCidade.setText(tbListarClienteJuridico.getValueAt(itemSelecionado, 6).toString());
            this.txtCep.setText(tbListarClienteJuridico.getValueAt(itemSelecionado, 7).toString());
            this.cbEstado.setSelectedItem(tbListarClienteJuridico.getValueAt(itemSelecionado, 8).toString());

        } else {

            JOptionPane.showMessageDialog(null, "Item não selecionado!");

        }


    }//GEN-LAST:event_jMEditarPessoaJuridicaActionPerformed

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        // TODO add your handling code here:
        limparCampos();
    }//GEN-LAST:event_btCancelarActionPerformed

    private void jMDevolucaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMDevolucaoActionPerformed
        // TODO add your handling code here:
        Devolucao devolucao = new Devolucao();
        devolucao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMDevolucaoActionPerformed

    private void jMUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMUsuarioActionPerformed
        // TODO add your handling code here:
        GerencialUsuario usuario = new GerencialUsuario();
        usuario.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMUsuarioActionPerformed

    private void jMCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMCategoriaActionPerformed
        // TODO add your handling code here:
        GerenciarCategoria categoria = new GerenciarCategoria();
        categoria.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMCategoriaActionPerformed

    private void jMClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClientesActionPerformed
        // TODO add your handling code here:
        GerenciarCliente clientes = new GerenciarCliente();
        clientes.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClientesActionPerformed

    private void jMLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocacaoActionPerformed
        // TODO add your handling code here:
        GerenciarLocacao locacao = new GerenciarLocacao();
        locacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocacaoActionPerformed

    private void jMLocadorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocadorasActionPerformed
        // TODO add your handling code here:
        GerenciarLocadora locadora = new GerenciarLocadora();
        locadora.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocadorasActionPerformed

    private void jMReservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservasActionPerformed
        // TODO add your handling code here:
        GerenciarReserva reservas = new GerenciarReserva();
        reservas.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservasActionPerformed

    private void jMVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMVeiculosActionPerformed
        // TODO add your handling code here:
        GerenciarVeiculos gerenciarVeiculo = new GerenciarVeiculos();
        gerenciarVeiculo.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMVeiculosActionPerformed

    private void jMIniciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIniciosActionPerformed
        // TODO add your handling code here:
        Menu menu = new Menu();
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMIniciosActionPerformed

    private void jMClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClienteActionPerformed
        // TODO add your handling code here:
        RelatorioPorCliente relatorioCliente = new RelatorioPorCliente();
        relatorioCliente.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClienteActionPerformed

    private void jMReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservaActionPerformed
        // TODO add your handling code here:
        RelatorioReserva relatorioReserva = new RelatorioReserva();
        relatorioReserva.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservaActionPerformed

    private void jMRelatorioLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMRelatorioLocacaoActionPerformed
        // TODO add your handling code here:
        RelatorioLocacao relatorioLocacao = new RelatorioLocacao();
        relatorioLocacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMRelatorioLocacaoActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(GerenciarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(GerenciarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(GerenciarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(GerenciarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new GerenciarCliente().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgSexo;
    private javax.swing.ButtonGroup bgTipoPesquisa;
    private javax.swing.ButtonGroup bgTipoPessoa;
    private javax.swing.JButton btAtualiar;
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btPesquisarCliente;
    private javax.swing.JButton btSalvar;
    private javax.swing.JComboBox<String> cbEstado;
    private javax.swing.JLayeredPane jLCNPJ;
    private javax.swing.JLayeredPane jLCPF;
    private javax.swing.JLayeredPane jLFisica;
    private javax.swing.JLayeredPane jLJuridica;
    private javax.swing.JLayeredPane jLTabelaCNPJ;
    private javax.swing.JLayeredPane jLTabelaCPF;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMCategoria;
    private javax.swing.JMenuItem jMCliente;
    private javax.swing.JMenuItem jMClientes;
    private javax.swing.JMenuItem jMDevolucao;
    private javax.swing.JMenuItem jMEditarPessoaFisica;
    private javax.swing.JMenuItem jMEditarPessoaJuridica;
    private javax.swing.JMenuItem jMExcluirPessoaJuridica;
    private javax.swing.JMenu jMInfoTempo;
    private javax.swing.JMenuItem jMInicios;
    private javax.swing.JMenuItem jMLocacao;
    private javax.swing.JMenuItem jMLocadoras;
    private javax.swing.JMenuItem jMRelatorioLocacao;
    private javax.swing.JMenuItem jMReserva;
    private javax.swing.JMenuItem jMReservas;
    private javax.swing.JMenuItem jMUsuario;
    private javax.swing.JMenuItem jMVeiculos;
    private javax.swing.JMenu jMenu;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenuItem jMenuExcluirPessoaFisica;
    private javax.swing.JMenu jMenuRelatorios;
    private javax.swing.JPanel jPFisica;
    private javax.swing.JPanel jPJuridica;
    private javax.swing.JPopupMenu jPMenuJuridica;
    private javax.swing.JPopupMenu jPMenuPessoaFisica;
    private javax.swing.JPanel jPPesquisa;
    private javax.swing.JPanel jPTipoPesquisa;
    private javax.swing.JPanel jPTipoTabela;
    private javax.swing.JPanel jPTiposdePessoa;
    private javax.swing.JRadioButton jRPesquisaCNPJ;
    private javax.swing.JRadioButton jRPesquisaCpf;
    private javax.swing.JScrollPane jSPaneCNPJ;
    private javax.swing.JScrollPane jSPaneCPF;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JRadioButton jrFeminino;
    private javax.swing.JRadioButton jrIsento;
    private javax.swing.JRadioButton jrMasculino;
    private javax.swing.JRadioButton jrPessoaFisica;
    private javax.swing.JRadioButton jrPessoaJuridica;
    private javax.swing.JTable tbListarClienteFisico;
    private javax.swing.JTable tbListarClienteJuridico;
    private javax.swing.JTextField txtBairro;
    private javax.swing.JTextField txtCep;
    private javax.swing.JTextField txtCidade;
    private javax.swing.JFormattedTextField txtCnpj;
    private javax.swing.JFormattedTextField txtCpf;
    private com.toedter.calendar.JDateChooser txtDataNascimento;
    private javax.swing.JTextField txtInsEstadual;
    private javax.swing.JTextField txtNhabilitacao;
    private javax.swing.JTextField txtNome;
    private javax.swing.JFormattedTextField txtPesquisaCNPJ;
    private javax.swing.JFormattedTextField txtPesquisarCPF;
    private javax.swing.JTextField txtRua;
    private com.toedter.calendar.JDateChooser txtVencimentoHabilitacao;
    // End of variables declaration//GEN-END:variables
}
