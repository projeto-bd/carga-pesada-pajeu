/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.view;

import br.com.CargaPesadaPajeu.control.Controle_Relatorio;
import br.com.CargaPesadaPajeu.control.Utilitarios;
import java.awt.Color;
import java.io.File;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author root
 */
public class RelatorioPorCliente extends javax.swing.JFrame {

    Utilitarios gerenciarUtilitarios = new Utilitarios();
    Controle_Relatorio gerenciarRelatorio = new Controle_Relatorio();
    DateFormat df = DateFormat.getDateInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
     Date infoMenu = new Date(System.currentTimeMillis());
    SimpleDateFormat fInfoMenu = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Creates new form RelatorioPorCliente
     */
    public RelatorioPorCliente() {
        initComponents();
        this.getContentPane().setBackground(Color.decode("#0277bd"));
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        tbListarClienteFisico.setAutoResizeMode(tbListarClienteFisico.AUTO_RESIZE_OFF);
        tbListarClienteJuridico.setAutoResizeMode(tbListarClienteJuridico.AUTO_RESIZE_OFF);
        jLCNPJ.setVisible(false);
        jLTabelaCNPJ.setVisible(false);
        this.jMInfoTempo.setText(fInfoMenu.format(infoMenu));
    }

    public void listaClienteFisico(String consulta) {
        ResultSet rs;
        rs = gerenciarRelatorio.listarClienteFisico(consulta);
        DefaultTableModel modelo = new DefaultTableModel();
        String[] retorno = new String[14];

        modelo.addColumn("ID");
        modelo.addColumn("Nome");
        modelo.addColumn("CPF");
        modelo.addColumn("Sexo");
        modelo.addColumn("Data Nascimento");
        modelo.addColumn("CNH");
        modelo.addColumn("Vencimento CNH");
        modelo.addColumn("Rua");
        modelo.addColumn("Bairro");
        modelo.addColumn("Cidade");
        modelo.addColumn("CEP");
        modelo.addColumn("Estado");

        tbListarClienteFisico.setModel(modelo);

        tbListarClienteFisico.getColumnModel().getColumn(0).setPreferredWidth(35);
        tbListarClienteFisico.getColumnModel().getColumn(1).setPreferredWidth(140);
        tbListarClienteFisico.getColumnModel().getColumn(2).setPreferredWidth(120);
        tbListarClienteFisico.getColumnModel().getColumn(3).setPreferredWidth(50);
        tbListarClienteFisico.getColumnModel().getColumn(4).setPreferredWidth(118);
        tbListarClienteFisico.getColumnModel().getColumn(5).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(6).setPreferredWidth(115);
        tbListarClienteFisico.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(8).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(9).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(10).setPreferredWidth(110);
        tbListarClienteFisico.getColumnModel().getColumn(11).setPreferredWidth(60);

        try {
            while (rs.next()) {
                retorno[0] = Integer.toString(rs.getInt(1));
                retorno[1] = rs.getString(2);
                retorno[2] = rs.getString(3);
                retorno[3] = rs.getString(4);
                String dataNascimentoConvertida = gerenciarUtilitarios.desconverteData(rs.getDate(5).toString());
                retorno[4] = dataNascimentoConvertida;
                String numeroCnh = null;
                if (rs.getString(6) == null || rs.getString(6).equals("")) {
                    retorno[5] = "Não tem";
                } else {
                    retorno[5] = rs.getString(6);
                }
                String dataVenciHabilitacao = gerenciarUtilitarios.desconverteData(rs.getString(7).toString());
                retorno[6] = dataVenciHabilitacao;
                retorno[7] = rs.getString(8);
                retorno[8] = rs.getString(9);
                retorno[9] = rs.getString(10);
                retorno[10] = rs.getString(11);
                retorno[11] = rs.getString(12);

                modelo.addRow(retorno);
            }
            tbListarClienteFisico.setModel(modelo);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar informações");
        }
    }

    public void listarClienteJuridico(String consulta) {
        ResultSet rs;
        rs = gerenciarRelatorio.listarClienteJuridico(consulta);
        DefaultTableModel modelo = new DefaultTableModel();
        String[] retorno = new String[9];

        modelo.addColumn("ID");
        modelo.addColumn("Nome");
        modelo.addColumn("CNPJ");
        modelo.addColumn("Ins.Estadual");
        modelo.addColumn("Rua");
        modelo.addColumn("Bairro");
        modelo.addColumn("Cidade");
        modelo.addColumn("CEP");
        modelo.addColumn("Estado");

        tbListarClienteJuridico.setModel(modelo);

        tbListarClienteJuridico.getColumnModel().getColumn(0).setPreferredWidth(35);
        tbListarClienteJuridico.getColumnModel().getColumn(1).setPreferredWidth(140);
        tbListarClienteJuridico.getColumnModel().getColumn(2).setPreferredWidth(145);
        tbListarClienteJuridico.getColumnModel().getColumn(3).setPreferredWidth(100);
        tbListarClienteJuridico.getColumnModel().getColumn(4).setPreferredWidth(110);
        tbListarClienteJuridico.getColumnModel().getColumn(5).setPreferredWidth(110);
        tbListarClienteJuridico.getColumnModel().getColumn(6).setPreferredWidth(110);
        tbListarClienteJuridico.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListarClienteJuridico.getColumnModel().getColumn(8).setPreferredWidth(60);

        try {
            while (rs.next()) {
                retorno[0] = Integer.toString(rs.getInt(1));
                retorno[1] = rs.getString(2);
                retorno[2] = rs.getString(3);
                if (rs.getString(4) == null) {
                    retorno[3] = "Isento";
                } else {
                    retorno[3] = rs.getString(4);
                }

                retorno[4] = rs.getString(5);
                retorno[5] = rs.getString(6);
                retorno[6] = rs.getString(7);
                retorno[7] = rs.getString(8);
                retorno[8] = rs.getString(9);

                modelo.addRow(retorno);
            }
            tbListarClienteJuridico.setModel(modelo);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar informações");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgTipoCliente = new javax.swing.ButtonGroup();
        jPPesquisa = new javax.swing.JPanel();
        btPesquisarCliente = new javax.swing.JButton();
        jRPesquisaCpf = new javax.swing.JRadioButton();
        jRPesquisaCNPJ = new javax.swing.JRadioButton();
        jPTipoPesquisa = new javax.swing.JPanel();
        jLCPF = new javax.swing.JLayeredPane();
        txtPesquisarCPF = new javax.swing.JFormattedTextField();
        jLCNPJ = new javax.swing.JLayeredPane();
        txtPesquisaCNPJ = new javax.swing.JFormattedTextField();
        btListarTudo = new javax.swing.JButton();
        jPTipoTabela = new javax.swing.JPanel();
        jLTabelaCPF = new javax.swing.JLayeredPane();
        jSPaneCPF = new javax.swing.JScrollPane();
        tbListarClienteFisico = new javax.swing.JTable();
        jLTabelaCNPJ = new javax.swing.JLayeredPane();
        jSPaneCNPJ = new javax.swing.JScrollPane();
        tbListarClienteJuridico = new javax.swing.JTable();
        btExportar = new javax.swing.JButton();
        jMenuBar = new javax.swing.JMenuBar();
        jMenu = new javax.swing.JMenu();
        jMDevolucao = new javax.swing.JMenuItem();
        jMUsuario = new javax.swing.JMenuItem();
        jMCategoria = new javax.swing.JMenuItem();
        jMClientes = new javax.swing.JMenuItem();
        jMLocacao = new javax.swing.JMenuItem();
        jMLocadoras = new javax.swing.JMenuItem();
        jMReservas = new javax.swing.JMenuItem();
        jMVeiculos = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMInicios = new javax.swing.JMenuItem();
        jMenuRelatorios = new javax.swing.JMenu();
        jMCliente = new javax.swing.JMenuItem();
        jMReserva = new javax.swing.JMenuItem();
        jMRelatorioLocacao = new javax.swing.JMenuItem();
        jMInfoTempo = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPPesquisa.setBackground(new java.awt.Color(2, 119, 189));
        jPPesquisa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        btPesquisarCliente.setBackground(new java.awt.Color(255, 255, 255));
        btPesquisarCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btPesquisarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/pesquisar.png"))); // NOI18N
        btPesquisarCliente.setText("Buscar");
        btPesquisarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btPesquisarClienteActionPerformed(evt);
            }
        });

        bgTipoCliente.add(jRPesquisaCpf);
        jRPesquisaCpf.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jRPesquisaCpf.setForeground(new java.awt.Color(255, 255, 255));
        jRPesquisaCpf.setSelected(true);
        jRPesquisaCpf.setText("CPF");
        jRPesquisaCpf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRPesquisaCpfActionPerformed(evt);
            }
        });

        bgTipoCliente.add(jRPesquisaCNPJ);
        jRPesquisaCNPJ.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jRPesquisaCNPJ.setForeground(new java.awt.Color(255, 255, 255));
        jRPesquisaCNPJ.setText("CNPJ");
        jRPesquisaCNPJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRPesquisaCNPJActionPerformed(evt);
            }
        });

        jPTipoPesquisa.setBackground(new java.awt.Color(2, 119, 189));

        try {
            txtPesquisarCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLCPF.setLayer(txtPesquisarCPF, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLCPFLayout = new javax.swing.GroupLayout(jLCPF);
        jLCPF.setLayout(jLCPFLayout);
        jLCPFLayout.setHorizontalGroup(
            jLCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 147, Short.MAX_VALUE)
            .addGroup(jLCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLCPFLayout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(txtPesquisarCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(21, Short.MAX_VALUE)))
        );
        jLCPFLayout.setVerticalGroup(
            jLCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 38, Short.MAX_VALUE)
            .addGroup(jLCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLCPFLayout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(txtPesquisarCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        try {
            txtPesquisaCNPJ.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLCNPJ.setLayer(txtPesquisaCNPJ, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLCNPJLayout = new javax.swing.GroupLayout(jLCNPJ);
        jLCNPJ.setLayout(jLCNPJLayout);
        jLCNPJLayout.setHorizontalGroup(
            jLCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 147, Short.MAX_VALUE)
            .addGroup(jLCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLCNPJLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(txtPesquisaCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(20, Short.MAX_VALUE)))
        );
        jLCNPJLayout.setVerticalGroup(
            jLCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jLCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLCNPJLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(txtPesquisaCNPJ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPTipoPesquisaLayout = new javax.swing.GroupLayout(jPTipoPesquisa);
        jPTipoPesquisa.setLayout(jPTipoPesquisaLayout);
        jPTipoPesquisaLayout.setHorizontalGroup(
            jPTipoPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPTipoPesquisaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLCNPJ))
            .addGroup(jPTipoPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPTipoPesquisaLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLCPF)))
        );
        jPTipoPesquisaLayout.setVerticalGroup(
            jPTipoPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPTipoPesquisaLayout.createSequentialGroup()
                .addComponent(jLCNPJ)
                .addContainerGap())
            .addGroup(jPTipoPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTipoPesquisaLayout.createSequentialGroup()
                    .addComponent(jLCPF, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 21, Short.MAX_VALUE)))
        );

        btListarTudo.setBackground(new java.awt.Color(255, 255, 255));
        btListarTudo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btListarTudo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/listar.png"))); // NOI18N
        btListarTudo.setText("Listar");
        btListarTudo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btListarTudoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPPesquisaLayout = new javax.swing.GroupLayout(jPPesquisa);
        jPPesquisa.setLayout(jPPesquisaLayout);
        jPPesquisaLayout.setHorizontalGroup(
            jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPesquisaLayout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(jRPesquisaCpf)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jRPesquisaCNPJ)
                .addGap(14, 14, 14)
                .addComponent(jPTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(btPesquisarCliente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btListarTudo)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPPesquisaLayout.setVerticalGroup(
            jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPPesquisaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPTipoPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jRPesquisaCNPJ)
                        .addComponent(jRPesquisaCpf))
                    .addGroup(jPPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btPesquisarCliente)
                        .addComponent(btListarTudo)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPTipoTabela.setBackground(new java.awt.Color(2, 119, 189));

        tbListarClienteFisico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jSPaneCPF.setViewportView(tbListarClienteFisico);

        jLTabelaCPF.setLayer(jSPaneCPF, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLTabelaCPFLayout = new javax.swing.GroupLayout(jLTabelaCPF);
        jLTabelaCPF.setLayout(jLTabelaCPFLayout);
        jLTabelaCPFLayout.setHorizontalGroup(
            jLTabelaCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 640, Short.MAX_VALUE)
            .addGroup(jLTabelaCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSPaneCPF, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE))
        );
        jLTabelaCPFLayout.setVerticalGroup(
            jLTabelaCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 269, Short.MAX_VALUE)
            .addGroup(jLTabelaCPFLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLTabelaCPFLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jSPaneCPF, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)))
        );

        tbListarClienteJuridico.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jSPaneCNPJ.setViewportView(tbListarClienteJuridico);

        jLTabelaCNPJ.setLayer(jSPaneCNPJ, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLTabelaCNPJLayout = new javax.swing.GroupLayout(jLTabelaCNPJ);
        jLTabelaCNPJ.setLayout(jLTabelaCNPJLayout);
        jLTabelaCNPJLayout.setHorizontalGroup(
            jLTabelaCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jLTabelaCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSPaneCNPJ, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE))
        );
        jLTabelaCNPJLayout.setVerticalGroup(
            jLTabelaCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 269, Short.MAX_VALUE)
            .addGroup(jLTabelaCNPJLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jLTabelaCNPJLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jSPaneCNPJ, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPTipoTabelaLayout = new javax.swing.GroupLayout(jPTipoTabela);
        jPTipoTabela.setLayout(jPTipoTabelaLayout);
        jPTipoTabelaLayout.setHorizontalGroup(
            jPTipoTabelaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLTabelaCNPJ)
            .addGroup(jPTipoTabelaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLTabelaCPF, javax.swing.GroupLayout.Alignment.TRAILING))
        );
        jPTipoTabelaLayout.setVerticalGroup(
            jPTipoTabelaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLTabelaCNPJ, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPTipoTabelaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLTabelaCPF, javax.swing.GroupLayout.Alignment.TRAILING))
        );

        btExportar.setBackground(new java.awt.Color(255, 255, 255));
        btExportar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/excel.png"))); // NOI18N
        btExportar.setText("Exportar");

        jMenu.setBackground(new java.awt.Color(255, 255, 255));
        jMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Menu.png"))); // NOI18N
        jMenu.setText("Menu");
        jMenu.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMDevolucao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMDevolucao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/devolucao.png"))); // NOI18N
        jMDevolucao.setText("Efetuar Devolução");
        jMDevolucao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMDevolucaoActionPerformed(evt);
            }
        });
        jMenu.add(jMDevolucao);

        jMUsuario.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/AdicionarFuncionario.png"))); // NOI18N
        jMUsuario.setText("Gerenciar Usuários");
        jMUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMUsuarioActionPerformed(evt);
            }
        });
        jMenu.add(jMUsuario);

        jMCategoria.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCategoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarCategoria.png"))); // NOI18N
        jMCategoria.setText("Gerenciar Categoria");
        jMCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMCategoriaActionPerformed(evt);
            }
        });
        jMenu.add(jMCategoria);

        jMClientes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/login.png"))); // NOI18N
        jMClientes.setText("Gerenciar Clientes");
        jMClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClientesActionPerformed(evt);
            }
        });
        jMenu.add(jMClientes);

        jMLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocao.png"))); // NOI18N
        jMLocacao.setText("Gerenciar Locações");
        jMLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocacaoActionPerformed(evt);
            }
        });
        jMenu.add(jMLocacao);

        jMLocadoras.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocadoras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocadoras.png"))); // NOI18N
        jMLocadoras.setText("Gerenciar Locadoras");
        jMLocadoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocadorasActionPerformed(evt);
            }
        });
        jMenu.add(jMLocadoras);

        jMReservas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarReserva.png"))); // NOI18N
        jMReservas.setText("Gerenciar Reservas");
        jMReservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservasActionPerformed(evt);
            }
        });
        jMenu.add(jMReservas);

        jMVeiculos.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMVeiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarVeiculo.png"))); // NOI18N
        jMVeiculos.setText("Gerenciar Veículos");
        jMVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMVeiculosActionPerformed(evt);
            }
        });
        jMenu.add(jMVeiculos);
        jMenu.add(jSeparator2);

        jMInicios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMInicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/home.png"))); // NOI18N
        jMInicios.setText("Tela Principal");
        jMInicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIniciosActionPerformed(evt);
            }
        });
        jMenu.add(jMInicios);

        jMenuBar.add(jMenu);

        jMenuRelatorios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/MenuRelatorios.png"))); // NOI18N
        jMenuRelatorios.setText("Relatórios");
        jMenuRelatorios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cliente.png"))); // NOI18N
        jMCliente.setText("Clientes");
        jMCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClienteActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMCliente);

        jMReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioReservas.png"))); // NOI18N
        jMReserva.setText("Reservas");
        jMReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservaActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMReserva);

        jMRelatorioLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMRelatorioLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioLocacao.png"))); // NOI18N
        jMRelatorioLocacao.setText("Locações");
        jMRelatorioLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMRelatorioLocacaoActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMRelatorioLocacao);

        jMenuBar.add(jMenuRelatorios);

        jMInfoTempo.setForeground(new java.awt.Color(0, 0, 0));
        jMInfoTempo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dataMenu.png"))); // NOI18N
        jMInfoTempo.setText("tempo");
        jMInfoTempo.setEnabled(false);
        jMInfoTempo.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jMenuBar.add(jMInfoTempo);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jPPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPTipoTabela, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btExportar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btExportar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPTipoTabela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btPesquisarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btPesquisarClienteActionPerformed
        // TODO add your handling code here:
        if (jRPesquisaCpf.isSelected()) {
            listaClienteFisico(this.txtPesquisarCPF.getText());

        } else if (jRPesquisaCNPJ.isSelected()) {
            listarClienteJuridico(this.txtPesquisaCNPJ.getText());
        } else {
            JOptionPane.showMessageDialog(null, "Selecione uma opção!");

        }
    }//GEN-LAST:event_btPesquisarClienteActionPerformed

    private void jRPesquisaCpfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRPesquisaCpfActionPerformed
        // TODO add your handling code here:
        if (jRPesquisaCpf.isSelected()) {
            listaClienteFisico("");
            jLCNPJ.setVisible(false);
            jLTabelaCNPJ.setVisible(false);

            jLCPF.setVisible(true);
            jLTabelaCPF.setVisible(true);
            txtPesquisaCNPJ.setText("");
        }
    }//GEN-LAST:event_jRPesquisaCpfActionPerformed

    private void jRPesquisaCNPJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRPesquisaCNPJActionPerformed
        // TODO add your handling code here:
        listarClienteJuridico("");
        jLCNPJ.setVisible(true);
        jLTabelaCNPJ.setVisible(true);

        jLCPF.setVisible(false);
        jLTabelaCPF.setVisible(false);
        txtPesquisarCPF.setText("");
    }//GEN-LAST:event_jRPesquisaCNPJActionPerformed

    private void btListarTudoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btListarTudoActionPerformed
        // TODO add your handling code here:
        listaClienteFisico("");
        this.jRPesquisaCpf.setSelected(true);
        if (jRPesquisaCpf.isSelected()) {
            listaClienteFisico("");
            jLCNPJ.setVisible(false);
            jLTabelaCNPJ.setVisible(false);

            jLCPF.setVisible(true);
            jLTabelaCPF.setVisible(true);
            txtPesquisaCNPJ.setText("");
        }
        this.txtPesquisaCNPJ.setText("");
        this.txtPesquisarCPF.setText("");
    }//GEN-LAST:event_btListarTudoActionPerformed

    private void jMDevolucaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMDevolucaoActionPerformed
        // TODO add your handling code here:
        Devolucao devolucao = new Devolucao();
        devolucao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMDevolucaoActionPerformed

    private void jMUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMUsuarioActionPerformed
        // TODO add your handling code here:
        GerencialUsuario usuario = new GerencialUsuario();
        usuario.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMUsuarioActionPerformed

    private void jMCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMCategoriaActionPerformed
        // TODO add your handling code here:
        GerenciarCategoria categoria = new GerenciarCategoria();
        categoria.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMCategoriaActionPerformed

    private void jMClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClientesActionPerformed
        // TODO add your handling code here:
        GerenciarCliente clientes = new GerenciarCliente();
        clientes.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClientesActionPerformed

    private void jMLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocacaoActionPerformed
        // TODO add your handling code here:
        GerenciarLocacao locacao = new GerenciarLocacao();
        locacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocacaoActionPerformed

    private void jMLocadorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocadorasActionPerformed
        // TODO add your handling code here:
        GerenciarLocadora locadora = new GerenciarLocadora();
        locadora.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocadorasActionPerformed

    private void jMReservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservasActionPerformed
        // TODO add your handling code here:
        GerenciarReserva reservas = new GerenciarReserva();
        reservas.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservasActionPerformed

    private void jMVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMVeiculosActionPerformed
        // TODO add your handling code here:
        GerenciarVeiculos gerenciarVeiculo = new GerenciarVeiculos();
        gerenciarVeiculo.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMVeiculosActionPerformed

    private void jMIniciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIniciosActionPerformed
        // TODO add your handling code here:
        Menu menu = new Menu();
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMIniciosActionPerformed

    private void jMClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClienteActionPerformed
        // TODO add your handling code here:
        RelatorioPorCliente relatorioCliente = new RelatorioPorCliente();
        relatorioCliente.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClienteActionPerformed

    private void jMReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservaActionPerformed
        // TODO add your handling code here:
        RelatorioReserva relatorioReserva = new RelatorioReserva();
        relatorioReserva.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservaActionPerformed

    private void jMRelatorioLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMRelatorioLocacaoActionPerformed
        // TODO add your handling code here:
        RelatorioLocacao relatorioLocacao = new RelatorioLocacao();
        relatorioLocacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMRelatorioLocacaoActionPerformed

    /**
//     * @param args the command line arguments
//     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(RelatorioPorCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(RelatorioPorCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(RelatorioPorCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(RelatorioPorCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new RelatorioPorCliente().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgTipoCliente;
    private javax.swing.JButton btExportar;
    private javax.swing.JButton btListarTudo;
    private javax.swing.JButton btPesquisarCliente;
    private javax.swing.JLayeredPane jLCNPJ;
    private javax.swing.JLayeredPane jLCPF;
    private javax.swing.JLayeredPane jLTabelaCNPJ;
    private javax.swing.JLayeredPane jLTabelaCPF;
    private javax.swing.JMenuItem jMCategoria;
    private javax.swing.JMenuItem jMCliente;
    private javax.swing.JMenuItem jMClientes;
    private javax.swing.JMenuItem jMDevolucao;
    private javax.swing.JMenu jMInfoTempo;
    private javax.swing.JMenuItem jMInicios;
    private javax.swing.JMenuItem jMLocacao;
    private javax.swing.JMenuItem jMLocadoras;
    private javax.swing.JMenuItem jMRelatorioLocacao;
    private javax.swing.JMenuItem jMReserva;
    private javax.swing.JMenuItem jMReservas;
    private javax.swing.JMenuItem jMUsuario;
    private javax.swing.JMenuItem jMVeiculos;
    private javax.swing.JMenu jMenu;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuRelatorios;
    private javax.swing.JPanel jPPesquisa;
    private javax.swing.JPanel jPTipoPesquisa;
    private javax.swing.JPanel jPTipoTabela;
    private javax.swing.JRadioButton jRPesquisaCNPJ;
    private javax.swing.JRadioButton jRPesquisaCpf;
    private javax.swing.JScrollPane jSPaneCNPJ;
    private javax.swing.JScrollPane jSPaneCPF;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JTable tbListarClienteFisico;
    private javax.swing.JTable tbListarClienteJuridico;
    private javax.swing.JFormattedTextField txtPesquisaCNPJ;
    private javax.swing.JFormattedTextField txtPesquisarCPF;
    // End of variables declaration//GEN-END:variables
}
