/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.view;

import br.com.CargaPesadaPajeu.control.Controle_Reserva;
import br.com.CargaPesadaPajeu.control.Controle_Veiculo;
import br.com.CargaPesadaPajeu.control.Utilitarios;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author root
 */
public class GerenciarReserva extends javax.swing.JFrame {

    Controle_Veiculo gerenciarVeiculo = new Controle_Veiculo();
    Controle_Reserva gerenciarReserva = new Controle_Reserva();
    Utilitarios utiliatiro = new Utilitarios();
    int idCliente;
    String nomeCliente;
    String idReservaAlterada;
    DateFormat df = DateFormat.getDateInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    String valorCategoria;
     Date infoMenu = new Date(System.currentTimeMillis());
    SimpleDateFormat fInfoMenu = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Creates new form GerenciarLocacao
     */
    public GerenciarReserva() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(Color.decode("#0277bd"));
        tbListarReservas.setAutoResizeMode(tbListarReservas.AUTO_RESIZE_OFF);
        this.setResizable(false);
        carregarLocadora();
        carregarCategoria();
        jLClienteJuridico.setVisible(false);
        listarReservas("");
        desativarCampos();
        limparCampos();
        this.jMInfoTempo.setText(fInfoMenu.format(infoMenu));
    }

    public void limparCampos(){
        this.txtCpf.setText("");
        this.txtCnpj.setText("");
        this.jrClienteFisico.setSelected(true);
        this.txtNome.setText("");
        this.txtDataEntrega.setDate(null);
        this.txtDataRetirada.setDate(null);
        this.btAtualizar.setEnabled(false);
        this.btSalvar.setEnabled(false);
        
    }
    public void desativarCampos(){
        this.txtDataRetirada.setEnabled(false);
        this.txtDataEntrega.setEnabled(false);
        this.jrKmControle.setEnabled(false);
        this.jrKmLivre.setEnabled(false);
        this.btCalcularReserva.setEnabled(false);
        this.cbCategoria.setEnabled(false);
        this.cbLocadora.setEnabled(false);
        this.cbStatus.setEnabled(false);
    }
     public void ativarCampos(){
        this.txtDataRetirada.setEnabled(true);
        this.txtDataEntrega.setEnabled(true);
        this.jrKmControle.setEnabled(true);
        this.jrKmLivre.setEnabled(true);
        this.btCalcularReserva.setEnabled(true);
        this.cbCategoria.setEnabled(true);
        this.cbLocadora.setEnabled(true);
        this.cbStatus.setEnabled(true);
    }
    public void listarReservas(String consulta) {
        ResultSet rs;
        rs = gerenciarReserva.listarReservas(consulta);
        DefaultTableModel modelo = new DefaultTableModel();

        String[] retorno = new String[10];

        modelo.addColumn("Nome");
        modelo.addColumn("Nº Reserva");
        modelo.addColumn("Data Retirada");
        modelo.addColumn("Status");
        modelo.addColumn("Data Entrega");
        modelo.addColumn("Categoria de Carro");
        modelo.addColumn("Locadora");
        modelo.addColumn("Tipo Cliente");
        modelo.addColumn("Valor");
        modelo.addColumn("Tipo de Locação");

        tbListarReservas.setModel(modelo);

        tbListarReservas.getColumnModel().getColumn(0).setPreferredWidth(125);
        tbListarReservas.getColumnModel().getColumn(1).setPreferredWidth(130);
        tbListarReservas.getColumnModel().getColumn(2).setPreferredWidth(145);
        tbListarReservas.getColumnModel().getColumn(3).setPreferredWidth(110);
        tbListarReservas.getColumnModel().getColumn(4).setPreferredWidth(110);
        tbListarReservas.getColumnModel().getColumn(5).setPreferredWidth(130);
        tbListarReservas.getColumnModel().getColumn(6).setPreferredWidth(130);
        tbListarReservas.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListarReservas.getColumnModel().getColumn(8).setPreferredWidth(110);
        tbListarReservas.getColumnModel().getColumn(9).setPreferredWidth(120);

        try {
            while (rs.next()) {

                retorno[0] = rs.getString(1);
                retorno[1] = Integer.toString(rs.getInt(2));
                String dataRetirada = utiliatiro.desconverteData(rs.getDate(3).toString());
                retorno[2] = dataRetirada;
                retorno[3] = rs.getString(4);
                String dataEntrega = utiliatiro.desconverteData(rs.getDate(5).toString());
                retorno[4] = dataEntrega;
                retorno[5] = rs.getString(6);
                retorno[6] = rs.getString(7);
                retorno[7] = rs.getString(8);
                retorno[8] = Float.toString(rs.getFloat(9));
                retorno[9] = rs.getString(10);

                modelo.addRow(retorno);
            }
            tbListarReservas.setModel(modelo);
            try {
                if (rs != null) {
                    rs.close();

                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao encerrar as conexões:");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar dados!!:");
        }

    }

    public void carregarLocadora() {
        ResultSet rs;
        rs = gerenciarVeiculo.carregarLocadora();
        if (rs != null) {
            try {

                while (rs.next()) {
                    cbLocadora.addItem(rs.getString(1));
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");

                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Não foi possível carregar as locadoras");

        }

    }

    public void carregarCategoria() {
        ResultSet rs;
        rs = gerenciarVeiculo.carregarCategoria();
        if (rs != null) {
            try {

                while (rs.next()) {
                    cbCategoria.addItem(rs.getString(1));
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");

                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Não foi possível carregar as categorias");

        }

    }

    public void carregarClienteFisico(String cpf) {
        ResultSet rs;
        String retorno="";
        rs = gerenciarReserva.verificarClienteFisico(cpf);
        if (rs != null) {
            try {

                while (rs.next()) {
                    retorno = rs.getString(2);
                    this.txtNome.setText(retorno);
                    idCliente = rs.getInt(1);
                    ativarCampos();
                }
                if(retorno.equals("")||retorno==null){
                    JOptionPane.showMessageDialog(null, "Cliente não cadastrado!");
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");
                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Erro ao buscar informações!");

        }

    }

    public void carregarClienteJuridico(String cnpj) {
        ResultSet rs;
        String retorno="";
        rs = gerenciarReserva.verificarClienteJuridico(cnpj);
        if (rs != null) {
            try {

                while (rs.next()) {
                    retorno = rs.getString(2);
                    this.txtNome.setText(rs.getString(2));
                    idCliente = rs.getInt(1);
                }
                if(retorno.equals("")||retorno==null){
                    JOptionPane.showMessageDialog(null, "Cliente não cadastrado!");
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");
                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Cliente não tem cadastro!");

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgTipoCliente = new javax.swing.ButtonGroup();
        jPopupMenu = new javax.swing.JPopupMenu();
        jMEditar = new javax.swing.JMenuItem();
        jMExcluir = new javax.swing.JMenuItem();
        bgTipoReserva = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jrClienteFisico = new javax.swing.JRadioButton();
        jrClienteJuridico = new javax.swing.JRadioButton();
        jPTipoCliente = new javax.swing.JPanel();
        jLClienteFisico = new javax.swing.JLayeredPane();
        jLabel2 = new javax.swing.JLabel();
        txtCpf = new javax.swing.JFormattedTextField();
        jLClienteJuridico = new javax.swing.JLayeredPane();
        jLabel3 = new javax.swing.JLabel();
        txtCnpj = new javax.swing.JFormattedTextField();
        btVerificar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtDataRetirada = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        txtDataEntrega = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        cbCategoria = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        cbLocadora = new javax.swing.JComboBox<>();
        btSalvar = new javax.swing.JButton();
        btAtualizar = new javax.swing.JButton();
        btCancelar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        cbStatus = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        btPesquisar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbListarReservas = new javax.swing.JTable();
        jrKmLivre = new javax.swing.JRadioButton();
        jrKmControle = new javax.swing.JRadioButton();
        jLabel11 = new javax.swing.JLabel();
        btCalcularReserva = new javax.swing.JButton();
        jMenuBar = new javax.swing.JMenuBar();
        jMenu = new javax.swing.JMenu();
        jMDevolucao = new javax.swing.JMenuItem();
        jMUsuario = new javax.swing.JMenuItem();
        jMCategoria = new javax.swing.JMenuItem();
        jMClientes = new javax.swing.JMenuItem();
        jMLocacao = new javax.swing.JMenuItem();
        jMLocadoras = new javax.swing.JMenuItem();
        jMReservas = new javax.swing.JMenuItem();
        jMVeiculos = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMInicios = new javax.swing.JMenuItem();
        jMenuRelatorios = new javax.swing.JMenu();
        jMCliente = new javax.swing.JMenuItem();
        jMReserva = new javax.swing.JMenuItem();
        jMRelatorioLocacao = new javax.swing.JMenuItem();
        jMInfoTempo = new javax.swing.JMenu();

        jMEditar.setText("Editar");
        jMEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMEditarActionPerformed(evt);
            }
        });
        jPopupMenu.add(jMEditar);

        jMExcluir.setText("Excluir");
        jMExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMExcluirActionPerformed(evt);
            }
        });
        jPopupMenu.add(jMExcluir);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gerenciar Reserva");

        jLabel1.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Tipo Cliente:");

        bgTipoCliente.add(jrClienteFisico);
        jrClienteFisico.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jrClienteFisico.setForeground(new java.awt.Color(255, 255, 255));
        jrClienteFisico.setSelected(true);
        jrClienteFisico.setText("Cliente Físico");
        jrClienteFisico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrClienteFisicoActionPerformed(evt);
            }
        });

        bgTipoCliente.add(jrClienteJuridico);
        jrClienteJuridico.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jrClienteJuridico.setForeground(new java.awt.Color(255, 255, 255));
        jrClienteJuridico.setText("Cliente Jurídico");
        jrClienteJuridico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrClienteJuridicoActionPerformed(evt);
            }
        });

        jPTipoCliente.setBackground(new java.awt.Color(2, 119, 189));

        jLabel2.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("CPF:");

        try {
            txtCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLClienteFisico.setLayer(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLClienteFisico.setLayer(txtCpf, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLClienteFisicoLayout = new javax.swing.GroupLayout(jLClienteFisico);
        jLClienteFisico.setLayout(jLClienteFisicoLayout);
        jLClienteFisicoLayout.setHorizontalGroup(
            jLClienteFisicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLClienteFisicoLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jLClienteFisicoLayout.setVerticalGroup(
            jLClienteFisicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLClienteFisicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLClienteFisicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel3.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("CNPJ:");

        try {
            txtCnpj.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLClienteJuridico.setLayer(jLabel3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLClienteJuridico.setLayer(txtCnpj, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLClienteJuridicoLayout = new javax.swing.GroupLayout(jLClienteJuridico);
        jLClienteJuridico.setLayout(jLClienteJuridicoLayout);
        jLClienteJuridicoLayout.setHorizontalGroup(
            jLClienteJuridicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLClienteJuridicoLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );
        jLClienteJuridicoLayout.setVerticalGroup(
            jLClienteJuridicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLClienteJuridicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLClienteJuridicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btVerificar.setBackground(new java.awt.Color(255, 255, 255));
        btVerificar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btVerificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/check.png"))); // NOI18N
        btVerificar.setText("Verificar");
        btVerificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVerificarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPTipoClienteLayout = new javax.swing.GroupLayout(jPTipoCliente);
        jPTipoCliente.setLayout(jPTipoClienteLayout);
        jPTipoClienteLayout.setHorizontalGroup(
            jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPTipoClienteLayout.createSequentialGroup()
                .addComponent(jLClienteFisico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btVerificar))
            .addGroup(jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTipoClienteLayout.createSequentialGroup()
                    .addComponent(jLClienteJuridico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 93, Short.MAX_VALUE)))
        );
        jPTipoClienteLayout.setVerticalGroup(
            jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPTipoClienteLayout.createSequentialGroup()
                .addGroup(jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLClienteFisico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPTipoClienteLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btVerificar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTipoClienteLayout.createSequentialGroup()
                    .addComponent(jLClienteJuridico)
                    .addContainerGap()))
        );

        jLabel4.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nome:");

        txtNome.setEditable(false);

        jLabel5.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Retirada:");

        jLabel6.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Entrega:");

        jLabel7.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Categoria:");

        cbCategoria.setBackground(new java.awt.Color(255, 255, 255));
        cbCategoria.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbCategoriaActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Locadora:");

        cbLocadora.setBackground(new java.awt.Color(255, 255, 255));
        cbLocadora.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N

        btSalvar.setBackground(new java.awt.Color(255, 255, 255));
        btSalvar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        btSalvar.setText("Salvar");
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        btAtualizar.setBackground(new java.awt.Color(255, 255, 255));
        btAtualizar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/atualizar.png"))); // NOI18N
        btAtualizar.setText("Atualizar");
        btAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAtualizarActionPerformed(evt);
            }
        });

        btCancelar.setBackground(new java.awt.Color(255, 255, 255));
        btCancelar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cancelar.png"))); // NOI18N
        btCancelar.setText("Cancelar");
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Total R$:");

        txtValor.setEditable(false);

        jLabel10.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Status:");

        cbStatus.setBackground(new java.awt.Color(255, 255, 255));
        cbStatus.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Aguardando", "Efetivada", "Cancelada" }));
        cbStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbStatusActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(2, 119, 189));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        btPesquisar.setBackground(new java.awt.Color(255, 255, 255));
        btPesquisar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/pesquisar.png"))); // NOI18N
        btPesquisar.setText("Pesquisar");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btPesquisar)
                .addGap(139, 139, 139))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btPesquisar))
                .addContainerGap())
        );

        tbListarReservas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbListarReservas.setComponentPopupMenu(jPopupMenu);
        jScrollPane1.setViewportView(tbListarReservas);

        bgTipoReserva.add(jrKmLivre);
        jrKmLivre.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jrKmLivre.setForeground(new java.awt.Color(255, 255, 255));
        jrKmLivre.setText("KM Livre");

        bgTipoReserva.add(jrKmControle);
        jrKmControle.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jrKmControle.setForeground(new java.awt.Color(255, 255, 255));
        jrKmControle.setSelected(true);
        jrKmControle.setText("KM Controle");

        jLabel11.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Reserva:");

        btCalcularReserva.setBackground(new java.awt.Color(255, 255, 255));
        btCalcularReserva.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        btCalcularReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/calculadora.png"))); // NOI18N
        btCalcularReserva.setText("Calcular");
        btCalcularReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCalcularReservaActionPerformed(evt);
            }
        });

        jMenu.setBackground(new java.awt.Color(255, 255, 255));
        jMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Menu.png"))); // NOI18N
        jMenu.setText("Menu");
        jMenu.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMDevolucao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMDevolucao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/devolucao.png"))); // NOI18N
        jMDevolucao.setText("Efetuar Devolução");
        jMDevolucao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMDevolucaoActionPerformed(evt);
            }
        });
        jMenu.add(jMDevolucao);

        jMUsuario.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/AdicionarFuncionario.png"))); // NOI18N
        jMUsuario.setText("Gerenciar Usuários");
        jMUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMUsuarioActionPerformed(evt);
            }
        });
        jMenu.add(jMUsuario);

        jMCategoria.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCategoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarCategoria.png"))); // NOI18N
        jMCategoria.setText("Gerenciar Categoria");
        jMCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMCategoriaActionPerformed(evt);
            }
        });
        jMenu.add(jMCategoria);

        jMClientes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/login.png"))); // NOI18N
        jMClientes.setText("Gerenciar Clientes");
        jMClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClientesActionPerformed(evt);
            }
        });
        jMenu.add(jMClientes);

        jMLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocao.png"))); // NOI18N
        jMLocacao.setText("Gerenciar Locações");
        jMLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocacaoActionPerformed(evt);
            }
        });
        jMenu.add(jMLocacao);

        jMLocadoras.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocadoras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocadoras.png"))); // NOI18N
        jMLocadoras.setText("Gerenciar Locadoras");
        jMLocadoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocadorasActionPerformed(evt);
            }
        });
        jMenu.add(jMLocadoras);

        jMReservas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarReserva.png"))); // NOI18N
        jMReservas.setText("Gerenciar Reservas");
        jMReservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservasActionPerformed(evt);
            }
        });
        jMenu.add(jMReservas);

        jMVeiculos.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMVeiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarVeiculo.png"))); // NOI18N
        jMVeiculos.setText("Gerenciar Veículos");
        jMVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMVeiculosActionPerformed(evt);
            }
        });
        jMenu.add(jMVeiculos);
        jMenu.add(jSeparator2);

        jMInicios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMInicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/home.png"))); // NOI18N
        jMInicios.setText("Tela Principal");
        jMInicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIniciosActionPerformed(evt);
            }
        });
        jMenu.add(jMInicios);

        jMenuBar.add(jMenu);

        jMenuRelatorios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/MenuRelatorios.png"))); // NOI18N
        jMenuRelatorios.setText("Relatórios");
        jMenuRelatorios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cliente.png"))); // NOI18N
        jMCliente.setText("Clientes");
        jMCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClienteActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMCliente);

        jMReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioReservas.png"))); // NOI18N
        jMReserva.setText("Reservas");
        jMReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservaActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMReserva);

        jMRelatorioLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMRelatorioLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioLocacao.png"))); // NOI18N
        jMRelatorioLocacao.setText("Locações");
        jMRelatorioLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMRelatorioLocacaoActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMRelatorioLocacao);

        jMenuBar.add(jMenuRelatorios);

        jMInfoTempo.setForeground(new java.awt.Color(0, 0, 0));
        jMInfoTempo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dataMenu.png"))); // NOI18N
        jMInfoTempo.setText("tempo");
        jMInfoTempo.setEnabled(false);
        jMInfoTempo.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jMenuBar.add(jMInfoTempo);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(79, 79, 79)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(txtValor, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtDataEntrega, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                                            .addComponent(txtDataRetirada, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(cbStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cbLocadora, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cbCategoria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(83, 83, 83)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jrKmLivre)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jrKmControle)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btCalcularReserva)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jrClienteFisico)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jrClienteJuridico)
                        .addGap(100, 100, 100)))
                .addContainerGap())
            .addComponent(jScrollPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 135, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPTipoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(101, 101, 101))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btSalvar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btAtualizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btCancelar)
                        .addGap(112, 112, 112))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jrClienteFisico)
                    .addComponent(jrClienteJuridico))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                .addComponent(jPTipoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel5)
                        .addComponent(txtDataRetirada, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(cbCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(cbLocadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(txtDataEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10)
                        .addComponent(cbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(jrKmLivre)
                        .addComponent(jrKmControle))
                    .addComponent(btCalcularReserva))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSalvar)
                    .addComponent(btAtualizar)
                    .addComponent(btCancelar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jrClienteFisicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrClienteFisicoActionPerformed
        // TODO add your handling code here:
        if (jrClienteFisico.isSelected()) {
            this.txtCnpj.setText("");
            this.txtCnpj.setEnabled(false);
            this.txtCpf.setEnabled(true);
            this.jLClienteJuridico.setVisible(false);
            this.jLClienteFisico.setVisible(true);

        }
    }//GEN-LAST:event_jrClienteFisicoActionPerformed

    private void jrClienteJuridicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrClienteJuridicoActionPerformed
        // TODO add your handling code here:
        if (jrClienteJuridico.isSelected()) {
            this.txtCpf.setText("");
            this.txtCpf.setEnabled(false);
            this.txtCnpj.setEnabled(true);
            this.jLClienteFisico.setVisible(false);
            this.jLClienteJuridico.setVisible(true);

        }
    }//GEN-LAST:event_jrClienteJuridicoActionPerformed

    private void btVerificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVerificarActionPerformed
        // TODO add your handling code here:
        if (jrClienteFisico.isSelected()) {
            carregarClienteFisico(this.txtCpf.getText());
        }
        if (jrClienteJuridico.isSelected()) {
            carregarClienteJuridico(this.txtCnpj.getText());
        }
    }//GEN-LAST:event_btVerificarActionPerformed

    private void cbCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbCategoriaActionPerformed
        // TODO add your handling code here:
        ResultSet rs;
        rs = gerenciarReserva.carregarCategoriaVeiculo(cbCategoria.getSelectedItem().toString());
        if (rs != null) {
            try {

                while (rs.next()) {
                    this.txtValor.setText(Float.toString(rs.getFloat(3)));
                    valorCategoria =Float.toString(rs.getFloat(3));
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");
                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Cliente não tem cadastro!");

        }

    }//GEN-LAST:event_cbCategoriaActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        // TODO add your handling code here:
        String dataRetirada = df.format(this.txtDataRetirada.getDate());
        String dataRetiradaSql = utiliatiro.converteData(dataRetirada);
        String dataEntrega = df.format(this.txtDataEntrega.getDate());
        String dataEntregaSql = utiliatiro.converteData(dataEntrega);
        String status = cbStatus.getSelectedItem().toString();
        int idCliente = this.idCliente;
        String nomeCategoriaVeiculo = cbCategoria.getSelectedItem().toString();
        String nomeLocadora = cbLocadora.getSelectedItem().toString();
        String tipoCliente;
        float valor = Float.parseFloat(this.txtValor.getText());
        String tipo_locacao;
        if (this.jrClienteFisico.isSelected()) {
            tipoCliente = this.jrClienteFisico.getText();
        } else {
            tipoCliente = this.jrClienteJuridico.getText();
        }
        if (this.jrKmLivre.isSelected()) {
            tipo_locacao = this.jrKmLivre.getText();
        } else {
            tipo_locacao = jrKmControle.getText();

        }
        if (gerenciarReserva.efeutarReserva(dataRetiradaSql, status, dataEntregaSql, idCliente,
                nomeCategoriaVeiculo, nomeLocadora, tipoCliente, valor, tipo_locacao) == true) {

            JOptionPane.showMessageDialog(null, "Reservado com sucesso");
            listarReservas("");
            limparCampos();
            desativarCampos();
        } else {
            JOptionPane.showMessageDialog(null, "Erro ao gravar dados!");
        }

    }//GEN-LAST:event_btSalvarActionPerformed

    private void jMExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMExcluirActionPerformed
        // TODO add your handling code here:
        int itemSelecionado = tbListarReservas.getSelectedRow();
        boolean confirmacao;
        if (itemSelecionado >= 0) {
            String deletar = tbListarReservas.getValueAt(itemSelecionado, 1).toString();
            System.err.println("");
            Object[] botoes = {"Sim", "Não"};
            int resposta = JOptionPane.showOptionDialog(null,
                    "Deseja Excluir esse item?",
                    "Confirmação", // o título da janela
                    JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    botoes, botoes[0]);
            if (resposta == 0) {
                confirmacao = gerenciarReserva.deletarReserva(deletar, resposta);
                if (confirmacao == true) {
                    JOptionPane.showMessageDialog(null, "Excluído!");
                    listarReservas("");
                } else {
                    JOptionPane.showMessageDialog(null, "Erro ao  tentar excluir!");
                }
            }

        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }
    }//GEN-LAST:event_jMExcluirActionPerformed

    private void jMEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMEditarActionPerformed
        // TODO add your handling code here:
        this.btSalvar.setEnabled(false);
        ativarCampos();
        this.btAtualizar.setEnabled(true);
        int itemSelecionado = tbListarReservas.getSelectedRow();
        if (itemSelecionado >= 0) {
            idReservaAlterada = tbListarReservas.getValueAt(itemSelecionado, 1).toString();
            this.txtNome.setText(tbListarReservas.getValueAt(itemSelecionado, 0).toString());
            String dataRetirada = tbListarReservas.getValueAt(itemSelecionado, 2).toString();
            this.cbStatus.setSelectedItem(tbListarReservas.getValueAt(itemSelecionado, 3).toString());
            String dataEntrega = tbListarReservas.getValueAt(itemSelecionado, 4).toString();
            this.cbCategoria.setSelectedItem(tbListarReservas.getValueAt(itemSelecionado, 5).toString());
            this.cbLocadora.setSelectedItem(tbListarReservas.getValueAt(itemSelecionado, 6).toString());
            String tipoLocacao = tbListarReservas.getValueAt(itemSelecionado, 9).toString();
            if (tipoLocacao.equals("Km Livre")) {
                this.jrKmLivre.setSelected(true);
            } else {
                this.jrKmControle.setSelected(true);

            }
            try {
                Date dataRetiradaConvertida = formatter.parse(dataRetirada);
                this.txtDataRetirada.setDate(dataRetiradaConvertida);
                Date dataEntregaConvertida = formatter.parse(dataEntrega);
                this.txtDataEntrega.setDate(dataEntregaConvertida);
            } catch (ParseException ex) {
                System.err.println("erro ao converter data");
            }

        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }

    }//GEN-LAST:event_jMEditarActionPerformed

    private void btAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAtualizarActionPerformed
        // TODO add your handling code here:

        String dataRetirada = df.format(this.txtDataRetirada.getDate());
        String dataRetiradaSql = utiliatiro.converteData(dataRetirada);
        String dataEntrega = df.format(this.txtDataEntrega.getDate());
        String dataEntregaSql = utiliatiro.converteData(dataEntrega);
        String status = cbStatus.getSelectedItem().toString();
        String nomeCategoriaVeiculo = cbCategoria.getSelectedItem().toString();
        String nomeLocadora = cbLocadora.getSelectedItem().toString();
        String tipo_locacao;
        float valor = Float.parseFloat(this.txtValor.getText());
        if (jrKmLivre.isSelected()) {
            tipo_locacao = jrKmLivre.getText();
        } else {
            tipo_locacao = jrKmControle.getText();
        }

        if (gerenciarReserva.alterarReserva(dataRetiradaSql, status, dataEntregaSql, nomeCategoriaVeiculo,
                nomeLocadora, valor, idReservaAlterada, tipo_locacao) == true) {

            JOptionPane.showMessageDialog(null, "Alterado com sucesso");
            listarReservas("");
            limparCampos();
            desativarCampos();
        } else {
            JOptionPane.showMessageDialog(null, "Erro ao alterar dados!");
        }

    }//GEN-LAST:event_btAtualizarActionPerformed

    private void btCalcularReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCalcularReservaActionPerformed
        // TODO add your handling code here:
        
        if (this.txtDataRetirada.getDate() != null && this.txtDataEntrega.getDate() != null) {
            String dataRetirada = df.format(this.txtDataRetirada.getDate());
            String dataEntrega = df.format(this.txtDataEntrega.getDate());

            int diaRetirada = Integer.parseInt(dataRetirada.substring(0, 2));
            int mesRetirada = Integer.parseInt(dataRetirada.substring(3, 5));
            int anoRetirada = Integer.parseInt(dataRetirada.substring(6));

            int diaEntrega = Integer.parseInt(dataEntrega.substring(0, 2));
            int mesEntrega = Integer.parseInt(dataEntrega.substring(3, 5));
            int anoEntrega = Integer.parseInt(dataEntrega.substring(6));
            
            if (mesEntrega != mesRetirada || anoRetirada != anoEntrega || diaEntrega < diaRetirada) {
                JOptionPane.showMessageDialog(null, "Datas inválida");

            } else if (jrKmLivre.isSelected()) {
                float valorPadrao = Float.parseFloat(valorCategoria);
                int diarias = diaEntrega - diaRetirada;
                String novoValor = Float.toString((valorPadrao * diarias) * 2);
                this.txtValor.setText(novoValor);
                this.btSalvar.setEnabled(true);

            } else if (jrKmControle.isSelected()) {
                float valorPadrao = Float.parseFloat(valorCategoria);
                int diarias = diaEntrega - diaRetirada;
                String novoValor = Float.toString(valorPadrao * diarias);
                this.txtValor.setText(novoValor);
                this.btSalvar.setEnabled(true);

            } else {
                JOptionPane.showMessageDialog(null, "Selecione um tipo de locação!");
            }

        }else{
            JOptionPane.showMessageDialog(null, "Ops! Datas para reserva são invalidas!");
        
        }

    }//GEN-LAST:event_btCalcularReservaActionPerformed

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        // TODO add your handling code here:
        limparCampos();
        desativarCampos();
    }//GEN-LAST:event_btCancelarActionPerformed

    private void cbStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbStatusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbStatusActionPerformed

    private void jMDevolucaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMDevolucaoActionPerformed
        // TODO add your handling code here:
        Devolucao devolucao = new Devolucao();
        devolucao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMDevolucaoActionPerformed

    private void jMUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMUsuarioActionPerformed
        // TODO add your handling code here:
        GerencialUsuario usuario = new GerencialUsuario();
        usuario.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMUsuarioActionPerformed

    private void jMCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMCategoriaActionPerformed
        // TODO add your handling code here:
        GerenciarCategoria categoria = new GerenciarCategoria();
        categoria.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMCategoriaActionPerformed

    private void jMClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClientesActionPerformed
        // TODO add your handling code here:
        GerenciarCliente clientes = new GerenciarCliente();
        clientes.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClientesActionPerformed

    private void jMLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocacaoActionPerformed
        // TODO add your handling code here:
        GerenciarLocacao locacao = new GerenciarLocacao();
        locacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocacaoActionPerformed

    private void jMLocadorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocadorasActionPerformed
        // TODO add your handling code here:
        GerenciarLocadora locadora = new GerenciarLocadora();
        locadora.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocadorasActionPerformed

    private void jMReservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservasActionPerformed
        // TODO add your handling code here:
        GerenciarReserva reservas = new GerenciarReserva();
        reservas.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservasActionPerformed

    private void jMVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMVeiculosActionPerformed
        // TODO add your handling code here:
        GerenciarVeiculos gerenciarVeiculo = new GerenciarVeiculos();
        gerenciarVeiculo.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMVeiculosActionPerformed

    private void jMIniciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIniciosActionPerformed
        // TODO add your handling code here:
        Menu menu = new Menu();
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMIniciosActionPerformed

    private void jMClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClienteActionPerformed
        // TODO add your handling code here:
        RelatorioPorCliente relatorioCliente = new RelatorioPorCliente();
        relatorioCliente.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClienteActionPerformed

    private void jMReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservaActionPerformed
        // TODO add your handling code here:
        RelatorioReserva relatorioReserva = new RelatorioReserva();
        relatorioReserva.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservaActionPerformed

    private void jMRelatorioLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMRelatorioLocacaoActionPerformed
        // TODO add your handling code here:
        RelatorioLocacao relatorioLocacao = new RelatorioLocacao();
        relatorioLocacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMRelatorioLocacaoActionPerformed
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(GerenciarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(GerenciarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(GerenciarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(GerenciarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new GerenciarReserva().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgTipoCliente;
    private javax.swing.ButtonGroup bgTipoReserva;
    private javax.swing.JButton btAtualizar;
    private javax.swing.JButton btCalcularReserva;
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btPesquisar;
    private javax.swing.JButton btSalvar;
    private javax.swing.JButton btVerificar;
    private javax.swing.JComboBox<String> cbCategoria;
    private javax.swing.JComboBox<String> cbLocadora;
    private javax.swing.JComboBox<String> cbStatus;
    private javax.swing.JLayeredPane jLClienteFisico;
    private javax.swing.JLayeredPane jLClienteJuridico;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMCategoria;
    private javax.swing.JMenuItem jMCliente;
    private javax.swing.JMenuItem jMClientes;
    private javax.swing.JMenuItem jMDevolucao;
    private javax.swing.JMenuItem jMEditar;
    private javax.swing.JMenuItem jMExcluir;
    private javax.swing.JMenu jMInfoTempo;
    private javax.swing.JMenuItem jMInicios;
    private javax.swing.JMenuItem jMLocacao;
    private javax.swing.JMenuItem jMLocadoras;
    private javax.swing.JMenuItem jMRelatorioLocacao;
    private javax.swing.JMenuItem jMReserva;
    private javax.swing.JMenuItem jMReservas;
    private javax.swing.JMenuItem jMUsuario;
    private javax.swing.JMenuItem jMVeiculos;
    private javax.swing.JMenu jMenu;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuRelatorios;
    private javax.swing.JPanel jPTipoCliente;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JRadioButton jrClienteFisico;
    private javax.swing.JRadioButton jrClienteJuridico;
    private javax.swing.JRadioButton jrKmControle;
    private javax.swing.JRadioButton jrKmLivre;
    private javax.swing.JTable tbListarReservas;
    private javax.swing.JFormattedTextField txtCnpj;
    private javax.swing.JFormattedTextField txtCpf;
    private com.toedter.calendar.JDateChooser txtDataEntrega;
    private com.toedter.calendar.JDateChooser txtDataRetirada;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
}
