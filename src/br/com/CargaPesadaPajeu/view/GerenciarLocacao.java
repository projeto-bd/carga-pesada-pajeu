/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.view;

import br.com.CargaPesadaPajeu.control.Controle_Locacao;
import br.com.CargaPesadaPajeu.control.Controle_Reserva;
import br.com.CargaPesadaPajeu.control.Controle_Veiculo;
import br.com.CargaPesadaPajeu.control.Utilitarios;
import br.com.CargaPesadaPajeu.dao.Dao_Locacao;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author root
 */
public class GerenciarLocacao extends javax.swing.JFrame {

    DateFormat df = DateFormat.getDateInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Controle_Locacao gerenciarLocacao = new Controle_Locacao();
    Controle_Veiculo gerenciarVeiculo = new Controle_Veiculo();
    Controle_Reserva gerenciarReserva = new Controle_Reserva();
    Utilitarios utilitarios = new Utilitarios();
    String idReservaAlterada;
    String valorCategoria;
    int idCliente;
    String idReservaExcluir;
    Date infoMenu = new Date(System.currentTimeMillis());
    SimpleDateFormat fInfoMenu = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Creates new form GerenciarLocacao
     */
    public GerenciarLocacao() {
        initComponents();
        this.getContentPane().setBackground(Color.decode("#0277bd"));
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        jLClienteJuridico.setVisible(false);
        tbListarReservas.setAutoResizeMode(tbListarReservas.AUTO_RESIZE_OFF);
        carregarCategoria();
        carregarLocadora();
        desativarCamposCliente();
        listarLocacoes("");
        estadoDoCampos(false);
        this.btSalvar.setEnabled(false);
        this.jMInfoTempo.setText(fInfoMenu.format(infoMenu));
    }

    public void desativarCamposCliente() {
        this.txtCnpj.setEnabled(false);
        this.txtCpf.setEnabled(false);
        this.jrClienteFisico.setEnabled(false);
        this.jrClienteJuridico.setEnabled(false);
        this.btVerificarCliente.setEnabled(false);
        this.txtNReserva.setEnabled(true);
        this.btValidarReserva.setEnabled(true);
    }

    public void ativarCamposCliente() {
        this.txtCnpj.setEnabled(true);
        this.txtCpf.setEnabled(true);
        this.jrClienteFisico.setEnabled(true);
        this.jrClienteJuridico.setEnabled(true);
        this.btVerificarCliente.setEnabled(true);
        this.txtNReserva.setEnabled(false);
        this.btValidarReserva.setEnabled(false);
    }

    public void estadoDoCampos(boolean estado) {
        this.txtDataRetirada.setEnabled(estado);
        this.txtDataEntrega.setEnabled(estado);
        this.cbCarros.setEnabled(estado);
        this.cbCategoria.setEnabled(estado);
        this.cbLocadora.setEnabled(estado);
        this.cbStatus.setEnabled(estado);
        this.jrKmControle.setEnabled(estado);
        this.jrKmLivre.setEnabled(estado);
        this.txtCpfMotorista.setEditable(estado);

    }

    public void limparCampos() {
        //Limpar campos
        this.txtNome.setText("");
        this.txtCpfMotorista.setText("");
        this.txtCpf.setText("");
        this.txtCnpj.setText("");
        this.txtNReserva.setText("");
        this.txtDataEntrega.setDate(null);
        this.txtDataRetirada.setDate(null);

    }

    public void listarLocacoes(String consulta) {
        ResultSet rs;
        rs = gerenciarLocacao.listarLocacoes(consulta);
        DefaultTableModel modelo = new DefaultTableModel();

        String[] retorno = new String[11];

        modelo.addColumn("Id Locação");
        modelo.addColumn("Veículo");
        modelo.addColumn("Locatário");
        modelo.addColumn("Data Retirada");
        modelo.addColumn("Data Entrega");
        modelo.addColumn("Categoria");
        modelo.addColumn("Locadora");
        modelo.addColumn("Valor");
        modelo.addColumn("Tipo de locação");
        modelo.addColumn("Status");
        modelo.addColumn("Id Reserva");

        tbListarReservas.setModel(modelo);

        tbListarReservas.getColumnModel().getColumn(0).setPreferredWidth(105);
        tbListarReservas.getColumnModel().getColumn(1).setPreferredWidth(130);
        tbListarReservas.getColumnModel().getColumn(2).setPreferredWidth(145);
        tbListarReservas.getColumnModel().getColumn(3).setPreferredWidth(110);
        tbListarReservas.getColumnModel().getColumn(4).setPreferredWidth(110);
        tbListarReservas.getColumnModel().getColumn(5).setPreferredWidth(130);
        tbListarReservas.getColumnModel().getColumn(6).setPreferredWidth(130);
        tbListarReservas.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListarReservas.getColumnModel().getColumn(8).setPreferredWidth(110);
        tbListarReservas.getColumnModel().getColumn(9).setPreferredWidth(120);

        try {
            while (rs.next()) {

                retorno[0] = Integer.toString(rs.getInt(1));
                retorno[1] = rs.getString(2);
                retorno[2] = rs.getString(3);
                String dataEntrega = utilitarios.desconverteData(rs.getDate(5).toString());
                retorno[3] = dataEntrega;
                String dataRetirada = utilitarios.desconverteData(rs.getDate(4).toString());
                retorno[4] = dataRetirada;
                retorno[5] = rs.getString(6);
                retorno[6] = rs.getString(7);
                retorno[7] = Float.toString(rs.getFloat(8));
                retorno[8] = rs.getString(9);;
                retorno[9] = rs.getString(10);
                retorno[10] = Integer.toString(rs.getInt(11));

                modelo.addRow(retorno);
            }
            tbListarReservas.setModel(modelo);
            try {
                if (rs != null) {
                    rs.close();

                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao encerrar as conexões:");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar dados!:" + ex.getMessage());
        }

    }

    public void carregarClienteFisico(String cpf) {
        ResultSet rs;
        String retorno = "";
        rs = gerenciarReserva.verificarClienteFisico(cpf);
        if (rs != null) {
            try {

                while (rs.next()) {
                    retorno = rs.getString(2);
                    this.txtNome.setText(rs.getString(2));
                    idCliente = rs.getInt(1);
                    estadoDoCampos(true);
                }
                if (retorno.equals("") || retorno == null) {
                    JOptionPane.showMessageDialog(null, "Cliente não cadastrado!");
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");
                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Erro ao buscar informações!");

        }

    }

    public void carregarClienteJuridico(String cnpj) {
        ResultSet rs;
        String retorno = "";
        rs = gerenciarReserva.verificarClienteJuridico(cnpj);
        if (rs != null) {
            try {

                while (rs.next()) {
                    retorno = rs.getString(2);
                    this.txtNome.setText(rs.getString(2));
                    idCliente = rs.getInt(1);
                }
                if (retorno.equals("") || retorno == null) {
                    JOptionPane.showMessageDialog(null, "Cliente não cadastrado!");
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");
                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Erro ao buscar informações!");

        }
    }

    public void carregarLocadora() {
        ResultSet rs;
        rs = gerenciarVeiculo.carregarLocadora();
        if (rs != null) {
            try {

                while (rs.next()) {
                    cbLocadora.addItem(rs.getString(1));
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");

                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Não foi possível carregar as locadoras");

        }

    }

    public void carregarCategoria() {
        ResultSet rs;
        rs = gerenciarVeiculo.carregarCategoria();
        if (rs != null) {
            try {

                while (rs.next()) {
                    cbCategoria.addItem(rs.getString(1));
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");

                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Não foi possível carregar as categorias");

        }

    }

    public void listaVeiculosDisponiveis(String consulta) {
        ResultSet rs;
        rs = gerenciarLocacao.veiculosDisponiveis(consulta);
        try {
            while (rs.next()) {
                this.cbCarros.addItem(rs.getString(1));

            }

            try {
                if (rs != null) {
                    rs.close();

                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao encerrar as conexões:");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar dados!:" + ex.getMessage());
        }
    }

    public void kmAtual(String placa) {
        ResultSet rs;
        rs = gerenciarLocacao.kmAtual(placa);
        try {
            while (rs.next()) {
                this.txtKmAtual.setText(Integer.toString(rs.getInt(1)));
            }

            try {
                if (rs != null) {
                    rs.close();

                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao encerrar as conexões:");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar dados!:" + ex.getMessage());
        }
    }

    public void listarReservas(String consulta) {
        ResultSet rs;
        rs = gerenciarLocacao.listarLocacao(consulta);
        String retorno = "";
        try {
            if (rs == null) {
                JOptionPane.showMessageDialog(null, "Reserva não diponível");
            } else {
                while (rs.next()) {
                    retorno = rs.getString(2);
                    idReservaAlterada = rs.getString(2);
                    this.txtNome.setText(rs.getString(1));
                    this.cbStatus.setSelectedItem(rs.getString(4));
                    this.cbCategoria.setSelectedItem(rs.getString(6));
                    this.cbLocadora.setSelectedItem(rs.getString(7));
                    this.txtValor.setText(Float.toString(rs.getFloat(9)));
                    Date dataEntrega;
                    Date dataRetirada;
                    String tipoLocacao = rs.getString(10);
                    if (tipoLocacao.equals("KM Livre")) {
                        this.jrKmLivre.setSelected(true);
                    } else {
                        this.jrKmControle.setSelected(true);

                    }

                    try {

                        dataEntrega = formatter.parse(utilitarios.desconverteData(rs.getString(5)).toString());
                        dataRetirada = formatter.parse(utilitarios.desconverteData(rs.getString(3)).toString());
                        this.txtDataEntrega.setDate(dataEntrega);
                        this.txtDataRetirada.setDate(dataRetirada);
                        estadoDoCampos(true);

                    } catch (ParseException ex) {
                        JOptionPane.showMessageDialog(null, "Erro ao converter datas!");
                    }
                }
            }
            try {
                if (rs != null) {
                    rs.close();
                    System.out.println("Conexão Encerrada");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao encerrar as conexões:");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao carregar dados!:" + ex.getMessage());
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgTemReserva = new javax.swing.ButtonGroup();
        bgTipoCliente = new javax.swing.ButtonGroup();
        bgTipoLocacao = new javax.swing.ButtonGroup();
        jPopupMenu = new javax.swing.JPopupMenu();
        jMEditar = new javax.swing.JMenuItem();
        jMExcluir = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jrTemReserva = new javax.swing.JRadioButton();
        jrNaoTemReserva = new javax.swing.JRadioButton();
        jPtemReserva = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNReserva = new javax.swing.JTextField();
        btValidarReserva = new javax.swing.JButton();
        jPInfoReservas = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtDataRetirada = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        cbCategoria = new javax.swing.JComboBox<>();
        cbLocadora = new javax.swing.JComboBox<>();
        cbStatus = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtDataEntrega = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jrKmLivre = new javax.swing.JRadioButton();
        jPTipoCliente = new javax.swing.JPanel();
        jLClienteFisico = new javax.swing.JLayeredPane();
        jLabel3 = new javax.swing.JLabel();
        txtCpf = new javax.swing.JFormattedTextField();
        jLClienteJuridico = new javax.swing.JLayeredPane();
        jLabel9 = new javax.swing.JLabel();
        txtCnpj = new javax.swing.JFormattedTextField();
        btVerificarCliente = new javax.swing.JButton();
        jrClienteFisico = new javax.swing.JRadioButton();
        jLabel12 = new javax.swing.JLabel();
        jrClienteJuridico = new javax.swing.JRadioButton();
        jLabel13 = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        btCalcularReserva = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        cbCarros = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jrKmControle = new javax.swing.JRadioButton();
        btSalvar = new javax.swing.JButton();
        btAtualizar = new javax.swing.JButton();
        btCancelar1 = new javax.swing.JButton();
        txtCpfMotorista = new javax.swing.JFormattedTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbListarReservas = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btPesquisarLocacao = new javax.swing.JButton();
        txtPesquisarLocacao = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel17 = new javax.swing.JLabel();
        txtKmAtual = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jMenuBar = new javax.swing.JMenuBar();
        jMenu = new javax.swing.JMenu();
        jMDevolucao = new javax.swing.JMenuItem();
        jMUsuario = new javax.swing.JMenuItem();
        jMCategoria = new javax.swing.JMenuItem();
        jMClientes = new javax.swing.JMenuItem();
        jMLocacao = new javax.swing.JMenuItem();
        jMLocadoras = new javax.swing.JMenuItem();
        jMReservas = new javax.swing.JMenuItem();
        jMVeiculos = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMInicios = new javax.swing.JMenuItem();
        jMenuRelatorios = new javax.swing.JMenu();
        jMCliente = new javax.swing.JMenuItem();
        jMReserva = new javax.swing.JMenuItem();
        jMRelatorioLocacao = new javax.swing.JMenuItem();
        jMInfoTempo = new javax.swing.JMenu();

        jMEditar.setText("Editar");
        jPopupMenu.add(jMEditar);

        jMExcluir.setText("Excluir");
        jMExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMExcluirActionPerformed(evt);
            }
        });
        jPopupMenu.add(jMExcluir);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gerenciar Locação");

        jLabel1.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Tem reserva?");

        bgTemReserva.add(jrTemReserva);
        jrTemReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrTemReserva.setForeground(new java.awt.Color(255, 255, 255));
        jrTemReserva.setSelected(true);
        jrTemReserva.setText("Sim");
        jrTemReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrTemReservaActionPerformed(evt);
            }
        });

        bgTemReserva.add(jrNaoTemReserva);
        jrNaoTemReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrNaoTemReserva.setForeground(new java.awt.Color(255, 255, 255));
        jrNaoTemReserva.setText("Não");
        jrNaoTemReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrNaoTemReservaActionPerformed(evt);
            }
        });

        jPtemReserva.setBackground(new java.awt.Color(2, 119, 189));

        jLabel2.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("NºReserva:");

        txtNReserva.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtNReserva.setForeground(new java.awt.Color(255, 0, 0));

        btValidarReserva.setBackground(new java.awt.Color(255, 255, 255));
        btValidarReserva.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        btValidarReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/check.png"))); // NOI18N
        btValidarReserva.setText("Validar");
        btValidarReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btValidarReservaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPtemReservaLayout = new javax.swing.GroupLayout(jPtemReserva);
        jPtemReserva.setLayout(jPtemReservaLayout);
        jPtemReservaLayout.setHorizontalGroup(
            jPtemReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPtemReservaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btValidarReserva)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPtemReservaLayout.setVerticalGroup(
            jPtemReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPtemReservaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPtemReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btValidarReserva))
                .addContainerGap())
        );

        jPInfoReservas.setBackground(new java.awt.Color(2, 119, 189));

        jLabel4.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nome:");

        txtNome.setEditable(false);
        txtNome.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Retirada:");

        jLabel7.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Categoria:");

        cbCategoria.setBackground(new java.awt.Color(255, 255, 255));
        cbCategoria.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbCategoriaActionPerformed(evt);
            }
        });

        cbLocadora.setBackground(new java.awt.Color(255, 255, 255));
        cbLocadora.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbLocadora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbLocadoraActionPerformed(evt);
            }
        });

        cbStatus.setBackground(new java.awt.Color(255, 255, 255));
        cbStatus.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Efetivada" }));
        cbStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbStatusActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Status:");

        jLabel8.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Locadora:");

        jLabel6.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Entrega:");

        jLabel11.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Reserva:");

        bgTipoLocacao.add(jrKmLivre);
        jrKmLivre.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jrKmLivre.setForeground(new java.awt.Color(255, 255, 255));
        jrKmLivre.setText("KM Livre");

        jPTipoCliente.setBackground(new java.awt.Color(2, 119, 189));

        jLabel3.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("CPF:");

        try {
            txtCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLClienteFisico.setLayer(jLabel3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLClienteFisico.setLayer(txtCpf, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLClienteFisicoLayout = new javax.swing.GroupLayout(jLClienteFisico);
        jLClienteFisico.setLayout(jLClienteFisicoLayout);
        jLClienteFisicoLayout.setHorizontalGroup(
            jLClienteFisicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLClienteFisicoLayout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jLClienteFisicoLayout.setVerticalGroup(
            jLClienteFisicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLClienteFisicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLClienteFisicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel9.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("CNPJ:");

        try {
            txtCnpj.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###/####-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jLClienteJuridico.setLayer(jLabel9, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLClienteJuridico.setLayer(txtCnpj, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLClienteJuridicoLayout = new javax.swing.GroupLayout(jLClienteJuridico);
        jLClienteJuridico.setLayout(jLClienteJuridicoLayout);
        jLClienteJuridicoLayout.setHorizontalGroup(
            jLClienteJuridicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLClienteJuridicoLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );
        jLClienteJuridicoLayout.setVerticalGroup(
            jLClienteJuridicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLClienteJuridicoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLClienteJuridicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btVerificarCliente.setBackground(new java.awt.Color(255, 255, 255));
        btVerificarCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btVerificarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/check.png"))); // NOI18N
        btVerificarCliente.setText("Verificar");
        btVerificarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVerificarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPTipoClienteLayout = new javax.swing.GroupLayout(jPTipoCliente);
        jPTipoCliente.setLayout(jPTipoClienteLayout);
        jPTipoClienteLayout.setHorizontalGroup(
            jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPTipoClienteLayout.createSequentialGroup()
                .addComponent(jLClienteFisico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btVerificarCliente))
            .addGroup(jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTipoClienteLayout.createSequentialGroup()
                    .addComponent(jLClienteJuridico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 93, Short.MAX_VALUE)))
        );
        jPTipoClienteLayout.setVerticalGroup(
            jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPTipoClienteLayout.createSequentialGroup()
                .addGroup(jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLClienteFisico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPTipoClienteLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btVerificarCliente)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPTipoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPTipoClienteLayout.createSequentialGroup()
                    .addComponent(jLClienteJuridico)
                    .addContainerGap()))
        );

        bgTipoCliente.add(jrClienteFisico);
        jrClienteFisico.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jrClienteFisico.setForeground(new java.awt.Color(255, 255, 255));
        jrClienteFisico.setSelected(true);
        jrClienteFisico.setText("Cliente Físico");
        jrClienteFisico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrClienteFisicoActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Tipo Cliente:");

        bgTipoCliente.add(jrClienteJuridico);
        jrClienteJuridico.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jrClienteJuridico.setForeground(new java.awt.Color(255, 255, 255));
        jrClienteJuridico.setText("Cliente Jurídico");
        jrClienteJuridico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrClienteJuridicoActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("KM Atual:");

        txtValor.setEditable(false);
        txtValor.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        txtValor.setForeground(new java.awt.Color(255, 0, 51));

        btCalcularReserva.setBackground(new java.awt.Color(255, 255, 255));
        btCalcularReserva.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        btCalcularReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/continuar.png"))); // NOI18N
        btCalcularReserva.setText("Continuar");
        btCalcularReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCalcularReservaActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Carro:");

        cbCarros.setBackground(new java.awt.Color(255, 255, 255));
        cbCarros.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbCarros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbCarrosActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Motorista:");

        bgTipoLocacao.add(jrKmControle);
        jrKmControle.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jrKmControle.setForeground(new java.awt.Color(255, 255, 255));
        jrKmControle.setSelected(true);
        jrKmControle.setText("KM Controle");

        btSalvar.setBackground(new java.awt.Color(255, 255, 255));
        btSalvar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        btSalvar.setText("Salvar");
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        btAtualizar.setBackground(new java.awt.Color(255, 255, 255));
        btAtualizar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/atualizar.png"))); // NOI18N
        btAtualizar.setText("Atualizar");
        btAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAtualizarActionPerformed(evt);
            }
        });

        btCancelar1.setBackground(new java.awt.Color(255, 255, 255));
        btCancelar1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btCancelar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cancelar.png"))); // NOI18N
        btCancelar1.setText("Cancelar");
        btCancelar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelar1ActionPerformed(evt);
            }
        });

        try {
            txtCpfMotorista.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        tbListarReservas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbListarReservas.setComponentPopupMenu(jPopupMenu);
        jScrollPane1.setViewportView(tbListarReservas);

        jPanel1.setBackground(new java.awt.Color(2, 119, 189));

        btPesquisarLocacao.setBackground(new java.awt.Color(255, 255, 255));
        btPesquisarLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btPesquisarLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/pesquisar.png"))); // NOI18N
        btPesquisarLocacao.setText("Pesquisar");
        btPesquisarLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btPesquisarLocacaoActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Nº Locação:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(149, Short.MAX_VALUE)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPesquisarLocacao, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btPesquisarLocacao)
                .addGap(155, 155, 155))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btPesquisarLocacao)
                    .addComponent(txtPesquisarLocacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addContainerGap())
        );

        jLabel17.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Total R$:");

        txtKmAtual.setEditable(false);
        txtKmAtual.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        txtKmAtual.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPInfoReservasLayout = new javax.swing.GroupLayout(jPInfoReservas);
        jPInfoReservas.setLayout(jPInfoReservasLayout);
        jPInfoReservasLayout.setHorizontalGroup(
            jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(jPInfoReservasLayout.createSequentialGroup()
                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPInfoReservasLayout.createSequentialGroup()
                        .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPInfoReservasLayout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel10)))
                            .addGroup(jPInfoReservasLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel15)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNome)
                            .addGroup(jPInfoReservasLayout.createSequentialGroup()
                                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cbStatus, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbCategoria, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtDataRetirada, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
                                .addGap(21, 21, 21)
                                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel11)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbLocadora, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtDataEntrega, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel17))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbCarros, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtValor)))
                            .addGroup(jPInfoReservasLayout.createSequentialGroup()
                                .addComponent(txtCpfMotorista, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(103, 103, 103)
                                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPInfoReservasLayout.createSequentialGroup()
                                        .addGap(21, 21, 21)
                                        .addComponent(btCalcularReserva)
                                        .addGap(65, 65, 65)
                                        .addComponent(jLabel13)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtKmAtual))
                                    .addGroup(jPInfoReservasLayout.createSequentialGroup()
                                        .addComponent(jrKmLivre)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jrKmControle)
                                        .addGap(0, 0, Short.MAX_VALUE))))))
                    .addGroup(jPInfoReservasLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jSeparator1)
            .addComponent(jSeparator3)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPInfoReservasLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPInfoReservasLayout.createSequentialGroup()
                        .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPInfoReservasLayout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jrClienteFisico)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jrClienteJuridico))
                            .addComponent(jPTipoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(145, 145, 145))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPInfoReservasLayout.createSequentialGroup()
                        .addComponent(btSalvar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btAtualizar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btCancelar1)
                        .addGap(126, 126, 126))))
        );
        jPInfoReservasLayout.setVerticalGroup(
            jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPInfoReservasLayout.createSequentialGroup()
                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jrClienteFisico)
                    .addComponent(jrClienteJuridico))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPTipoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5)
                    .addComponent(txtDataRetirada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbCarros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel14))
                    .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtDataEntrega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cbCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(cbLocadora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(jrKmLivre)
                    .addComponent(jrKmControle))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPInfoReservasLayout.createSequentialGroup()
                        .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtCpfMotorista, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel15))
                            .addComponent(btCalcularReserva, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btSalvar)
                            .addComponent(btAtualizar)
                            .addComponent(btCancelar1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPInfoReservasLayout.createSequentialGroup()
                        .addGroup(jPInfoReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(txtKmAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        jMenu.setBackground(new java.awt.Color(255, 255, 255));
        jMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Menu.png"))); // NOI18N
        jMenu.setText("Menu");
        jMenu.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMDevolucao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMDevolucao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/devolucao.png"))); // NOI18N
        jMDevolucao.setText("Efetuar Devolução");
        jMDevolucao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMDevolucaoActionPerformed(evt);
            }
        });
        jMenu.add(jMDevolucao);

        jMUsuario.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/AdicionarFuncionario.png"))); // NOI18N
        jMUsuario.setText("Gerenciar Usuários");
        jMUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMUsuarioActionPerformed(evt);
            }
        });
        jMenu.add(jMUsuario);

        jMCategoria.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCategoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarCategoria.png"))); // NOI18N
        jMCategoria.setText("Gerenciar Categoria");
        jMCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMCategoriaActionPerformed(evt);
            }
        });
        jMenu.add(jMCategoria);

        jMClientes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/login.png"))); // NOI18N
        jMClientes.setText("Gerenciar Clientes");
        jMClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClientesActionPerformed(evt);
            }
        });
        jMenu.add(jMClientes);

        jMLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocao.png"))); // NOI18N
        jMLocacao.setText("Gerenciar Locações");
        jMLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocacaoActionPerformed(evt);
            }
        });
        jMenu.add(jMLocacao);

        jMLocadoras.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocadoras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocadoras.png"))); // NOI18N
        jMLocadoras.setText("Gerenciar Locadoras");
        jMLocadoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocadorasActionPerformed(evt);
            }
        });
        jMenu.add(jMLocadoras);

        jMReservas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarReserva.png"))); // NOI18N
        jMReservas.setText("Gerenciar Reservas");
        jMReservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservasActionPerformed(evt);
            }
        });
        jMenu.add(jMReservas);

        jMVeiculos.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMVeiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarVeiculo.png"))); // NOI18N
        jMVeiculos.setText("Gerenciar Veículos");
        jMVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMVeiculosActionPerformed(evt);
            }
        });
        jMenu.add(jMVeiculos);
        jMenu.add(jSeparator5);

        jMInicios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMInicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/home.png"))); // NOI18N
        jMInicios.setText("Tela Principal");
        jMInicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIniciosActionPerformed(evt);
            }
        });
        jMenu.add(jMInicios);

        jMenuBar.add(jMenu);

        jMenuRelatorios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/MenuRelatorios.png"))); // NOI18N
        jMenuRelatorios.setText("Relatórios");
        jMenuRelatorios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cliente.png"))); // NOI18N
        jMCliente.setText("Clientes");
        jMCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClienteActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMCliente);

        jMReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioReservas.png"))); // NOI18N
        jMReserva.setText("Reservas");
        jMReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservaActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMReserva);

        jMRelatorioLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMRelatorioLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioLocacao.png"))); // NOI18N
        jMRelatorioLocacao.setText("Locações");
        jMRelatorioLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMRelatorioLocacaoActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMRelatorioLocacao);

        jMenuBar.add(jMenuRelatorios);

        jMInfoTempo.setForeground(new java.awt.Color(0, 0, 0));
        jMInfoTempo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dataMenu.png"))); // NOI18N
        jMInfoTempo.setText("tempo");
        jMInfoTempo.setEnabled(false);
        jMInfoTempo.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jMenuBar.add(jMInfoTempo);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPInfoReservas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator2)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPtemReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(175, 175, 175))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jrTemReserva)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jrNaoTemReserva)
                        .addGap(205, 205, 205))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jrTemReserva)
                    .addComponent(jrNaoTemReserva))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
                .addComponent(jPtemReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPInfoReservas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbCategoriaActionPerformed
        // TODO add your handling code here:
        this.cbCarros.removeAllItems();
        listaVeiculosDisponiveis(this.cbCategoria.getSelectedItem().toString());
        ResultSet rs;
        rs = gerenciarReserva.carregarCategoriaVeiculo(cbCategoria.getSelectedItem().toString());
        if (rs != null) {
            try {

                while (rs.next()) {
                    this.txtValor.setText(Float.toString(rs.getFloat(3)));
                    valorCategoria = Float.toString(rs.getFloat(3));
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");
                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Cliente não tem cadastro!");

        }


    }//GEN-LAST:event_cbCategoriaActionPerformed

    private void btVerificarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVerificarClienteActionPerformed
        // TODO add your handling code here:
        if (jrClienteFisico.isSelected()) {
            carregarClienteFisico(this.txtCpf.getText());
        }
        if (jrClienteJuridico.isSelected()) {
            carregarClienteJuridico(this.txtCnpj.getText());
        }

    }//GEN-LAST:event_btVerificarClienteActionPerformed

    private void jrClienteFisicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrClienteFisicoActionPerformed
        // TODO add your handling code here:
        if (jrClienteFisico.isSelected()) {
            this.txtCnpj.setText("");
            this.txtCnpj.setEnabled(false);
            this.txtCpf.setEnabled(true);
            this.jLClienteJuridico.setVisible(false);
            this.jLClienteFisico.setVisible(true);

        }
    }//GEN-LAST:event_jrClienteFisicoActionPerformed

    private void jrClienteJuridicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrClienteJuridicoActionPerformed
        // TODO add your handling code here:
        if (jrClienteJuridico.isSelected()) {
            this.txtCpf.setText("");
            this.txtCpf.setEnabled(false);
            this.txtCnpj.setEnabled(true);
            this.jLClienteFisico.setVisible(false);
            this.jLClienteJuridico.setVisible(true);

        }
    }//GEN-LAST:event_jrClienteJuridicoActionPerformed

    private void btCalcularReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCalcularReservaActionPerformed
        // TODO add your handling code here:
        if (gerenciarLocacao.verificaMotorista(this.txtCpfMotorista.getText()) == true) {
            if (this.txtDataRetirada.getDate() != null && this.txtDataEntrega.getDate() != null) {
                String dataRetirada = df.format(this.txtDataRetirada.getDate());
                String dataEntrega = df.format(this.txtDataEntrega.getDate());

                int diaRetirada = Integer.parseInt(dataRetirada.substring(0, 2));
                int mesRetirada = Integer.parseInt(dataRetirada.substring(3, 5));
                int anoRetirada = Integer.parseInt(dataRetirada.substring(6));

                int diaEntrega = Integer.parseInt(dataEntrega.substring(0, 2));
                int mesEntrega = Integer.parseInt(dataEntrega.substring(3, 5));
                int anoEntrega = Integer.parseInt(dataEntrega.substring(6));

                if (mesEntrega != mesRetirada || anoRetirada != anoEntrega || diaEntrega < diaRetirada) {
                    JOptionPane.showMessageDialog(null, "Datas inválida");

                } else if (jrKmLivre.isSelected()) {
                    float valorPadrao = Float.parseFloat(valorCategoria);
                    int diarias = diaEntrega - diaRetirada;
                    String novoValor = Float.toString((valorPadrao * diarias) * 2);
                    this.txtValor.setText(novoValor);
                    this.btSalvar.setEnabled(true);

                } else if (jrKmControle.isSelected()) {
                    float valorPadrao = Float.parseFloat(valorCategoria);
                    int diarias = diaEntrega - diaRetirada;
                    String novoValor = Float.toString(valorPadrao * diarias);
                    this.txtValor.setText(novoValor);
                    this.btSalvar.setEnabled(true);

                } else {
                    JOptionPane.showMessageDialog(null, "Selecione um tipo de locação!");
                }

            } else {
                JOptionPane.showMessageDialog(null, "Selecione a DATA de retirada e a DATA de entrega!");

            }
        } else {
            JOptionPane.showMessageDialog(null, "Ops!Verifique os dados do motorista!");
        }
        JOptionPane.showMessageDialog(null, "Tudo Certo, agora é só salvar ;)");

    }//GEN-LAST:event_btCalcularReservaActionPerformed

    private void btValidarReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btValidarReservaActionPerformed
        // TODO add your handling code here:
        listarReservas(this.txtNReserva.getText());
    }//GEN-LAST:event_btValidarReservaActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        // TODO add your handling code here:
        String placa = cbCarros.getSelectedItem().toString();
        String motorista = this.txtCpfMotorista.getText();

        if (jrTemReserva.isSelected()) {
            String dataRetirada = df.format(this.txtDataRetirada.getDate());
            String dataRetiradaSql = utilitarios.converteData(dataRetirada);
            String dataEntrega = df.format(this.txtDataEntrega.getDate());
            String dataEntregaSql = utilitarios.converteData(dataEntrega);
            String status = cbStatus.getSelectedItem().toString();
            String nomeCategoriaVeiculo = cbCategoria.getSelectedItem().toString();
            String nomeLocadora = cbLocadora.getSelectedItem().toString();
            String tipo_locacao;
            int idReserva = Integer.parseInt(this.txtNReserva.getText());
            float valor = Float.parseFloat(this.txtValor.getText());

            if (jrKmLivre.isSelected()) {
                tipo_locacao = jrKmLivre.getText();
            } else {
                tipo_locacao = jrKmControle.getText();
            }

            boolean atualizarReserva = gerenciarReserva.alterarReserva(dataRetiradaSql, status, dataEntregaSql, nomeCategoriaVeiculo,
                    nomeLocadora, valor, idReservaAlterada, tipo_locacao);

            boolean registrarLocacao = gerenciarLocacao.efetuarLocacao(motorista, placa, idReserva);

            if (atualizarReserva == true && registrarLocacao == true) {
                JOptionPane.showMessageDialog(null, "Locado com sucesso");
                int nLoacao = gerenciarLocacao.retornaNumeroLocacao();
                JOptionPane.showMessageDialog(null, "ANOTE o número de sua locação:" + nLoacao);
                listarLocacoes("");

            } else {
                JOptionPane.showMessageDialog(null, "Erro ao finalizar locação!");
            }
        } else if (jrNaoTemReserva.isSelected()) {
            String dataRetirada = df.format(this.txtDataRetirada.getDate());
            String dataRetiradaSql = utilitarios.converteData(dataRetirada);
            String dataEntrega = df.format(this.txtDataEntrega.getDate());
            String dataEntregaSql = utilitarios.converteData(dataEntrega);
            String status = cbStatus.getSelectedItem().toString();
            int idCliente = this.idCliente;
            String nomeCategoriaVeiculo = cbCategoria.getSelectedItem().toString();
            String nomeLocadora = cbLocadora.getSelectedItem().toString();
            String tipoCliente;
            float valor = Float.parseFloat(this.txtValor.getText());
            String tipo_locacao;
            if (this.jrClienteFisico.isSelected()) {
                tipoCliente = this.jrClienteFisico.getText();
            } else {
                tipoCliente = this.jrClienteJuridico.getText();
            }
            if (this.jrKmLivre.isSelected()) {
                tipo_locacao = this.jrKmLivre.getText();
            } else {
                tipo_locacao = jrKmControle.getText();
            }
            if (gerenciarReserva.efeutarReserva(dataRetiradaSql, status, dataEntregaSql, idCliente,
                    nomeCategoriaVeiculo, nomeLocadora, tipoCliente, valor, tipo_locacao) == true) {
                int idRerserva = gerenciarReserva.retornarId();
                if (gerenciarLocacao.efetuarLocacao(motorista, placa, idRerserva) == true) {

                    JOptionPane.showMessageDialog(null, "Locado com sucesso");
                    int nLoacao = gerenciarLocacao.retornaNumeroLocacao();
                    JOptionPane.showMessageDialog(null, "ANOTE o número de sua locação:" + nLoacao);
                    listarLocacoes("");
                } else {
                    JOptionPane.showMessageDialog(null, "Erro ao finalizar locaçã!");
                }

            } else {
                JOptionPane.showMessageDialog(null, "Erro ao finalizar locaçã!");
            }

        }

    }//GEN-LAST:event_btSalvarActionPerformed

    private void btAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAtualizarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btAtualizarActionPerformed

    private void btCancelar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelar1ActionPerformed
        // TODO add your handling code here:
        estadoDoCampos(false);
        limparCampos();
    }//GEN-LAST:event_btCancelar1ActionPerformed

    private void jrNaoTemReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrNaoTemReservaActionPerformed
        // TODO add your handling code here:
        if (jrNaoTemReserva.isSelected()) {
            ativarCamposCliente();
        }
    }//GEN-LAST:event_jrNaoTemReservaActionPerformed
    private void jrTemReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrTemReservaActionPerformed
        // TODO add your handling code here:
        desativarCamposCliente();
    }//GEN-LAST:event_jrTemReservaActionPerformed

    private void cbCarrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbCarrosActionPerformed
        // TODO add your handling code here:
        if (this.cbCarros.getSelectedItem() != null) {
            kmAtual(this.cbCarros.getSelectedItem().toString());

        } else {
            this.txtKmAtual.setText("0");

        }

    }//GEN-LAST:event_cbCarrosActionPerformed

    private void btPesquisarLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btPesquisarLocacaoActionPerformed
        // TODO add your handling code here:
        listarLocacoes(this.txtPesquisarLocacao.getText());
    }//GEN-LAST:event_btPesquisarLocacaoActionPerformed

    private void jMExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMExcluirActionPerformed
        // TODO add your handling code here:
        int itemSelecionado = tbListarReservas.getSelectedRow();
        String placa = tbListarReservas.getValueAt(itemSelecionado, 1).toString();
        boolean confirmacao, alteraStatusDoVeiculo;
        if (itemSelecionado >= 0) {
            String deletar = tbListarReservas.getValueAt(itemSelecionado, 0).toString();
            String deletarReserva = tbListarReservas.getValueAt(itemSelecionado, 10).toString();
            System.err.println("");
            Object[] botoes = {"Sim", "Não"};
            int resposta = JOptionPane.showOptionDialog(null,
                    "Deseja Excluir esse item?",
                    "Confirmação", // o título da janela
                    JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    botoes, botoes[0]);
            if (resposta == 0) {
                confirmacao = gerenciarLocacao.deletarReservaLocacao(deletar, deletarReserva, placa, resposta);

                if (confirmacao == true) {
                    JOptionPane.showMessageDialog(null, "Excluído!");
                    listarLocacoes("");
                } else {
                    JOptionPane.showMessageDialog(null, "Erro ao  tentar excluir!");
                }
            }

        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }
    }//GEN-LAST:event_jMExcluirActionPerformed

    private void cbStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbStatusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbStatusActionPerformed

    private void cbLocadoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbLocadoraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbLocadoraActionPerformed

    private void jMDevolucaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMDevolucaoActionPerformed
        // TODO add your handling code here:
        Devolucao devolucao = new Devolucao();
        devolucao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMDevolucaoActionPerformed

    private void jMUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMUsuarioActionPerformed
        // TODO add your handling code here:
        GerencialUsuario usuario = new GerencialUsuario();
        usuario.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMUsuarioActionPerformed

    private void jMCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMCategoriaActionPerformed
        // TODO add your handling code here:
        GerenciarCategoria categoria = new GerenciarCategoria();
        categoria.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMCategoriaActionPerformed

    private void jMClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClientesActionPerformed
        // TODO add your handling code here:
        GerenciarCliente clientes = new GerenciarCliente();
        clientes.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClientesActionPerformed

    private void jMLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocacaoActionPerformed
        // TODO add your handling code here:
        GerenciarLocacao locacao = new GerenciarLocacao();
        locacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocacaoActionPerformed

    private void jMLocadorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocadorasActionPerformed
        // TODO add your handling code here:
        GerenciarLocadora locadora = new GerenciarLocadora();
        locadora.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocadorasActionPerformed

    private void jMReservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservasActionPerformed
        // TODO add your handling code here:
        GerenciarReserva reservas = new GerenciarReserva();
        reservas.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservasActionPerformed

    private void jMVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMVeiculosActionPerformed
        // TODO add your handling code here:
        GerenciarVeiculos gerenciarVeiculo = new GerenciarVeiculos();
        gerenciarVeiculo.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMVeiculosActionPerformed

    private void jMIniciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIniciosActionPerformed
        // TODO add your handling code here:
        Menu menu = new Menu();
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMIniciosActionPerformed

    private void jMClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClienteActionPerformed
        // TODO add your handling code here:
        RelatorioPorCliente relatorioCliente = new RelatorioPorCliente();
        relatorioCliente.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClienteActionPerformed

    private void jMReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservaActionPerformed
        // TODO add your handling code here:
        RelatorioReserva relatorioReserva = new RelatorioReserva();
        relatorioReserva.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservaActionPerformed

    private void jMRelatorioLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMRelatorioLocacaoActionPerformed
        // TODO add your handling code here:
        RelatorioLocacao relatorioLocacao = new RelatorioLocacao();
        relatorioLocacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMRelatorioLocacaoActionPerformed

//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(GerenciarLocacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(GerenciarLocacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(GerenciarLocacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(GerenciarLocacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new GerenciarLocacao().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgTemReserva;
    private javax.swing.ButtonGroup bgTipoCliente;
    private javax.swing.ButtonGroup bgTipoLocacao;
    private javax.swing.JButton btAtualizar;
    private javax.swing.JButton btCalcularReserva;
    private javax.swing.JButton btCancelar1;
    private javax.swing.JButton btPesquisarLocacao;
    private javax.swing.JButton btSalvar;
    private javax.swing.JButton btValidarReserva;
    private javax.swing.JButton btVerificarCliente;
    private javax.swing.JComboBox<String> cbCarros;
    private javax.swing.JComboBox<String> cbCategoria;
    private javax.swing.JComboBox<String> cbLocadora;
    private javax.swing.JComboBox<String> cbStatus;
    private javax.swing.JLayeredPane jLClienteFisico;
    private javax.swing.JLayeredPane jLClienteJuridico;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMCategoria;
    private javax.swing.JMenuItem jMCliente;
    private javax.swing.JMenuItem jMClientes;
    private javax.swing.JMenuItem jMDevolucao;
    private javax.swing.JMenuItem jMEditar;
    private javax.swing.JMenuItem jMExcluir;
    private javax.swing.JMenu jMInfoTempo;
    private javax.swing.JMenuItem jMInicios;
    private javax.swing.JMenuItem jMLocacao;
    private javax.swing.JMenuItem jMLocadoras;
    private javax.swing.JMenuItem jMRelatorioLocacao;
    private javax.swing.JMenuItem jMReserva;
    private javax.swing.JMenuItem jMReservas;
    private javax.swing.JMenuItem jMUsuario;
    private javax.swing.JMenuItem jMVeiculos;
    private javax.swing.JMenu jMenu;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuRelatorios;
    private javax.swing.JPanel jPInfoReservas;
    private javax.swing.JPanel jPTipoCliente;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu;
    private javax.swing.JPanel jPtemReserva;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JRadioButton jrClienteFisico;
    private javax.swing.JRadioButton jrClienteJuridico;
    private javax.swing.JRadioButton jrKmControle;
    private javax.swing.JRadioButton jrKmLivre;
    private javax.swing.JRadioButton jrNaoTemReserva;
    private javax.swing.JRadioButton jrTemReserva;
    private javax.swing.JTable tbListarReservas;
    private javax.swing.JFormattedTextField txtCnpj;
    private javax.swing.JFormattedTextField txtCpf;
    private javax.swing.JFormattedTextField txtCpfMotorista;
    private com.toedter.calendar.JDateChooser txtDataEntrega;
    private com.toedter.calendar.JDateChooser txtDataRetirada;
    private javax.swing.JTextField txtKmAtual;
    private javax.swing.JTextField txtNReserva;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtPesquisarLocacao;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
}
