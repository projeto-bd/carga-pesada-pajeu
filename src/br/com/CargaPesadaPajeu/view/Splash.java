/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.view;

import java.awt.Color;
import java.awt.Dimension;
import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.UIManager;
import org.netbeans.lib.awtextra.AbsoluteConstraints;
import org.netbeans.lib.awtextra.AbsoluteLayout;

/**
 *
 * @author root
 */
public class Splash extends JWindow {

    AbsoluteLayout absoluto;
    AbsoluteConstraints absImg, absBarra;
    ImageIcon imagem;
    JLabel labelImagem;
    JProgressBar barra;
    
    public Splash() {
        absoluto = new AbsoluteLayout();
        absImg = new AbsoluteConstraints(0, 0);
        absBarra = new AbsoluteConstraints(0, 214);
        labelImagem = new JLabel();
        barra = new JProgressBar();
        UIManager.put("ProgressBar.background", Color.gray);
        UIManager.put("ProgressBar.foreground", Color.blue);
        barra.setPreferredSize(new Dimension(348,12));
        this.getContentPane().setLayout(absoluto);
        imagem = new ImageIcon(this.getClass().getResource("/imagens/CarregandoSistema.png"));
        labelImagem.setIcon(imagem);
        this.getContentPane().add(labelImagem, absImg);
        this.getContentPane().add(barra, absBarra);
       
        
        this.pack();
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        new Thread() {
            public void run() {
                int i = 0;
                Menu telaPrincipal = new Menu();
                while (i < 101) {
                    barra.setValue(i);
                    i++;
                    try {
                        sleep(70);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Splash.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
                telaPrincipal.setVisible(true);
                Splash.this.dispose();
            }
            
            
        }.start();
       
        
    }

}
