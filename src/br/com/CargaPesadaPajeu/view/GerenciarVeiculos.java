/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.view;

import br.com.CargaPesadaPajeu.control.Controle_Veiculo;
import br.com.CargaPesadaPajeu.control.Utilitarios;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author root
 */
public class GerenciarVeiculos extends javax.swing.JFrame {

    Controle_Veiculo gerenciarVeiculo = new Controle_Veiculo();
    Utilitarios utiliatiro = new Utilitarios();
    String idAutomovel;
    Date infoMenu = new Date(System.currentTimeMillis());
    SimpleDateFormat fInfoMenu = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Creates new form GerenciarVeiculos
     */
    public GerenciarVeiculos() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.getContentPane().setBackground(Color.decode("#0277bd"));
        this.setResizable(false);
        tbListarAutomovelPequeno.setAutoResizeMode(tbListarAutomovelPequeno.AUTO_RESIZE_OFF);
        tbListarCamiCarga.setAutoResizeMode(tbListarCamiCarga.AUTO_RESIZE_OFF);
        tbListaCamiPassageiro.setAutoResizeMode(tbListaCamiPassageiro.AUTO_RESIZE_OFF);
        desativarCamposAutomovelPequeno();
        desativarCamposCaminhoneteCarga();
        desativarComponenetesCaminhonetePassageiro();
        carregarCategoria();
        carregarLocadora();
        listarAutomovelPequeno("");
        this.jMInfoTempo.setText(fInfoMenu.format(infoMenu));

    }

    public void desativarCamposAutomovelPequeno() {

        txtTamanhoAutomovelPequeno.setEnabled(false);
        cbTipoCambio.setEnabled(false);
        jcRadio.setEnabled(false);
        jcMP3.setEnabled(false);
        jcDVD.setEnabled(false);
        jcDirecaoHidraulica.setEnabled(false);
        jcArcondicionado.setEnabled(false);
        jcCameraRe.setEnabled(false);

        txtTamanhoAutomovelPequeno.setText("");
        jcRadio.setSelected(false);
        jcMP3.setSelected(false);
        jcDVD.setSelected(false);
        jcDirecaoHidraulica.setSelected(false);
        jcArcondicionado.setSelected(false);
        jcCameraRe.setSelected(false);

    }

    public void ativarCamposAutomovelPequeno() {
        txtTamanhoAutomovelPequeno.setEnabled(true);
        cbTipoCambio.setEnabled(true);
        jcRadio.setEnabled(true);
        jcMP3.setEnabled(true);
        jcDVD.setEnabled(true);
        jcDirecaoHidraulica.setEnabled(true);
        jcArcondicionado.setEnabled(true);
        jcCameraRe.setEnabled(true);

    }

    public void ativarCamposCaminhoneteCarga() {
        txtCapacidadeCarga.setEnabled(true);
        txtDesempenho.setEnabled(true);
        txtDistanciaEixo.setEnabled(true);
        cbEmbreagem.setEnabled(true);
        txtPotencia.setEnabled(true);
        txtVolCombustivel.setEnabled(true);
    }

    public void desativarCamposCaminhoneteCarga() {
        txtCapacidadeCarga.setEnabled(false);
        txtDesempenho.setEnabled(false);
        txtDistanciaEixo.setEnabled(false);
        cbEmbreagem.setEnabled(false);
        txtPotencia.setEnabled(false);
        txtVolCombustivel.setEnabled(false);

        txtCapacidadeCarga.setText("");
        txtDesempenho.setText("");
        txtDistanciaEixo.setText("");
        txtPotencia.setText("");
        txtVolCombustivel.setText("");

    }

    public void ativarComponenetesCaminhonetePassageiro() {
        jcAirbag.setEnabled(true);
        jcCintoRetrateis.setEnabled(true);
        jcRodaLigaLeve.setEnabled(true);
        jcDirecaoAssistida.setEnabled(true);
        jcControlePoulicao.setEnabled(true);

    }

    public void desativarComponenetesCaminhonetePassageiro() {
        jcAirbag.setEnabled(false);
        jcCintoRetrateis.setEnabled(false);
        jcRodaLigaLeve.setEnabled(false);
        jcDirecaoAssistida.setEnabled(false);
        jcControlePoulicao.setEnabled(false);

        jcAirbag.setSelected(false);
        jcCintoRetrateis.setSelected(false);
        jcRodaLigaLeve.setSelected(false);
        jcDirecaoAssistida.setSelected(false);
        jcControlePoulicao.setSelected(false);

    }

    public void limparCampos() {
        this.txtPlaca.setText("");
        this.txtNChassi.setText("");
        this.txtNMotor.setText("");
        this.txtTorque.setText("");
        this.txtKmAtual.setText("");
        this.txtAnoFab.setText("");
        this.txtAnoModelo.setText("");
        this.txtNPortas.setText("");
        this.txtNOcupantes.setText("");

        jcAirbag.setSelected(false);
        jcCintoRetrateis.setSelected(false);
        jcRodaLigaLeve.setSelected(false);
        jcDirecaoAssistida.setSelected(false);
        jcControlePoulicao.setSelected(false);

        txtCapacidadeCarga.setText("");
        txtDesempenho.setText("");
        txtDistanciaEixo.setText("");
        txtPotencia.setText("");
        txtVolCombustivel.setText("");

        txtTamanhoAutomovelPequeno.setText("");
        jcRadio.setSelected(false);
        jcMP3.setSelected(false);
        jcDVD.setSelected(false);
        jcDirecaoHidraulica.setSelected(false);
        jcArcondicionado.setSelected(false);
        jcCameraRe.setSelected(false);

    }

    public void listarAutomovelPequeno(String consulta) {
        ResultSet rs;
        rs = gerenciarVeiculo.listarAutomovelPequeno(consulta);
        DefaultTableModel modelo = new DefaultTableModel();

        String[] retorno = new String[25];

        modelo.addColumn("Id");
        modelo.addColumn("Locadora");
        modelo.addColumn("Modelo");
        modelo.addColumn("Nº portas");
        modelo.addColumn("Nº Chassi");
        modelo.addColumn("Torque");
        modelo.addColumn("Nº Motor");
        modelo.addColumn("Placa");
        modelo.addColumn("Categoria");
        modelo.addColumn("Fabrição");
        modelo.addColumn("Km Atual");
        modelo.addColumn("Cor");
        modelo.addColumn("Combustível");
        modelo.addColumn("Nº Passageiros");
        modelo.addColumn("Revisões");
        modelo.addColumn("Status");
        modelo.addColumn("Limpeza");
        modelo.addColumn("Radio");
        modelo.addColumn("Mp3");
        modelo.addColumn("DVD");
        modelo.addColumn("Tamanho");
        modelo.addColumn("Câmera Ré");
        modelo.addColumn("Direção Hidr.");
        modelo.addColumn("Tipo Câmbio");
        modelo.addColumn("Ar-Condicionado");

        tbListarAutomovelPequeno.setModel(modelo);

        tbListarAutomovelPequeno.getColumnModel().getColumn(0).setPreferredWidth(35);
        tbListarAutomovelPequeno.getColumnModel().getColumn(1).setPreferredWidth(100);
        tbListarAutomovelPequeno.getColumnModel().getColumn(2).setPreferredWidth(105);
        tbListarAutomovelPequeno.getColumnModel().getColumn(3).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(4).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(5).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(6).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(8).setPreferredWidth(100);
        tbListarAutomovelPequeno.getColumnModel().getColumn(9).setPreferredWidth(105);
        tbListarAutomovelPequeno.getColumnModel().getColumn(10).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(11).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(12).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(13).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(14).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(15).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(16).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(17).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(18).setPreferredWidth(100);
        tbListarAutomovelPequeno.getColumnModel().getColumn(19).setPreferredWidth(105);
        tbListarAutomovelPequeno.getColumnModel().getColumn(20).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(21).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(22).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(23).setPreferredWidth(110);
        tbListarAutomovelPequeno.getColumnModel().getColumn(24).setPreferredWidth(110);

        try {
            while (rs.next()) {
                retorno[0] = Integer.toString(rs.getInt(1));
                retorno[1] = rs.getString(2);
                retorno[2] = Integer.toString(rs.getInt(3));
                retorno[3] = Integer.toString(rs.getInt(4));
                retorno[4] = Integer.toString(rs.getInt(5));
                retorno[5] = Integer.toString(rs.getInt(6));
                retorno[6] = Integer.toString(rs.getInt(7));
                retorno[7] = rs.getString(8);
                retorno[8] = rs.getString(9);
                retorno[9] = Integer.toString(rs.getInt(10));
                retorno[10] = Integer.toString(rs.getInt(11));
                retorno[11] = rs.getString(12);
                retorno[12] = rs.getString(13);
                retorno[13] = Integer.toString(rs.getInt(14));
                retorno[14] = rs.getString(15);
                retorno[15] = rs.getString(16);
                retorno[16] = rs.getString(17);
                retorno[17] = Boolean.toString(rs.getBoolean(18));
                retorno[18] = Boolean.toString(rs.getBoolean(19));
                retorno[19] = Boolean.toString(rs.getBoolean(20));
                retorno[20] = Float.toString(rs.getFloat(21));
                retorno[21] = Boolean.toString(rs.getBoolean(22));
                retorno[22] = Boolean.toString(rs.getBoolean(23));
                retorno[23] = rs.getString(24);
                retorno[24] = Boolean.toString(rs.getBoolean(25));

                modelo.addRow(retorno);
            }
            tbListarAutomovelPequeno.setModel(modelo);
            try {
                if (rs != null) {
                    rs.close();

                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao encerrar as conexões:");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de conexão!:");
        }
    }

    public void listarCamPassageiro(String consulta) {
        ResultSet rs;
        rs = gerenciarVeiculo.listarCaminhonetePassageiro(consulta);
        DefaultTableModel modelo = new DefaultTableModel();

        String[] retorno = new String[22];

        modelo.addColumn("Id");
        modelo.addColumn("Locadora");
        modelo.addColumn("Modelo");
        modelo.addColumn("Nº portas");
        modelo.addColumn("Nº Chassi");
        modelo.addColumn("Torque");
        modelo.addColumn("Nº Motor");
        modelo.addColumn("Placa");
        modelo.addColumn("Categoria");
        modelo.addColumn("Fabrição");
        modelo.addColumn("Km Atual");
        modelo.addColumn("Cor");
        modelo.addColumn("Combustível");
        modelo.addColumn("Nº Passageiros");
        modelo.addColumn("Revisões");
        modelo.addColumn("Status");
        modelo.addColumn("Limpeza");
        modelo.addColumn("AirBag");
        modelo.addColumn("Ctr.Puluição");
        modelo.addColumn("Cintos Retrateis");
        modelo.addColumn("Direção Assist.");
        modelo.addColumn("Rodas Liga Lev.");

        tbListaCamiPassageiro.setModel(modelo);

        tbListaCamiPassageiro.getColumnModel().getColumn(0).setPreferredWidth(35);
        tbListaCamiPassageiro.getColumnModel().getColumn(1).setPreferredWidth(100);
        tbListaCamiPassageiro.getColumnModel().getColumn(2).setPreferredWidth(105);
        tbListaCamiPassageiro.getColumnModel().getColumn(3).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(4).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(5).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(6).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(8).setPreferredWidth(100);
        tbListaCamiPassageiro.getColumnModel().getColumn(9).setPreferredWidth(105);
        tbListaCamiPassageiro.getColumnModel().getColumn(10).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(11).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(12).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(13).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(14).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(15).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(16).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(17).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(18).setPreferredWidth(100);
        tbListaCamiPassageiro.getColumnModel().getColumn(19).setPreferredWidth(105);
        tbListaCamiPassageiro.getColumnModel().getColumn(20).setPreferredWidth(110);
        tbListaCamiPassageiro.getColumnModel().getColumn(21).setPreferredWidth(110);

        try {
            while (rs.next()) {
                retorno[0] = Integer.toString(rs.getInt(1));
                retorno[1] = rs.getString(2);
                retorno[2] = Integer.toString(rs.getInt(3));
                retorno[3] = Integer.toString(rs.getInt(4));
                retorno[4] = Integer.toString(rs.getInt(5));
                retorno[5] = Integer.toString(rs.getInt(6));
                retorno[6] = Integer.toString(rs.getInt(7));
                retorno[7] = rs.getString(8);
                retorno[8] = rs.getString(9);
                retorno[9] = Integer.toString(rs.getInt(10));
                retorno[10] = Integer.toString(rs.getInt(11));
                retorno[11] = rs.getString(12);
                retorno[12] = rs.getString(13);
                retorno[13] = Integer.toString(rs.getInt(14));
                retorno[14] = rs.getString(15);
                retorno[15] = rs.getString(16);
                retorno[16] = rs.getString(17);
                retorno[17] = Boolean.toString(rs.getBoolean(18));
                retorno[18] = Boolean.toString(rs.getBoolean(19));
                retorno[19] = Boolean.toString(rs.getBoolean(20));
                retorno[20] = Boolean.toString(rs.getBoolean(21));
                retorno[21] = Boolean.toString(rs.getBoolean(22));

                modelo.addRow(retorno);
            }
            tbListaCamiPassageiro.setModel(modelo);
            try {
                if (rs != null) {
                    rs.close();

                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao encerrar as conexões:");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de conexão!:");
        }
    }

    public void listarCaminhonetCarga(String consulta) {
        ResultSet rs;
        rs = gerenciarVeiculo.listarCaminhoneteCarga(consulta);
        DefaultTableModel modelo = new DefaultTableModel();

        String[] retorno = new String[23];

        modelo.addColumn("Id");
        modelo.addColumn("Locadora");
        modelo.addColumn("Modelo");
        modelo.addColumn("Nº portas");
        modelo.addColumn("Nº Chassi");
        modelo.addColumn("Torque");
        modelo.addColumn("Nº Motor");
        modelo.addColumn("Placa");
        modelo.addColumn("Categoria");
        modelo.addColumn("Fabrição");
        modelo.addColumn("Km Atual");
        modelo.addColumn("Cor");
        modelo.addColumn("Combustível");
        modelo.addColumn("Nº Passageiros");
        modelo.addColumn("Revisões");
        modelo.addColumn("Status");
        modelo.addColumn("Limpeza");
        modelo.addColumn("Capacidade Carg.");
        modelo.addColumn("Vol.Combustível");
        modelo.addColumn("Desempenho");
        modelo.addColumn("Distancia Eixo");
        modelo.addColumn("Potencia");
        modelo.addColumn("Embreagem");

        tbListarCamiCarga.setModel(modelo);

        tbListarCamiCarga.getColumnModel().getColumn(0).setPreferredWidth(35);
        tbListarCamiCarga.getColumnModel().getColumn(1).setPreferredWidth(100);
        tbListarCamiCarga.getColumnModel().getColumn(2).setPreferredWidth(105);
        tbListarCamiCarga.getColumnModel().getColumn(3).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(4).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(5).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(6).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(7).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(8).setPreferredWidth(100);
        tbListarCamiCarga.getColumnModel().getColumn(9).setPreferredWidth(105);
        tbListarCamiCarga.getColumnModel().getColumn(10).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(11).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(12).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(13).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(14).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(15).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(16).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(17).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(18).setPreferredWidth(100);
        tbListarCamiCarga.getColumnModel().getColumn(19).setPreferredWidth(105);
        tbListarCamiCarga.getColumnModel().getColumn(20).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(21).setPreferredWidth(110);
        tbListarCamiCarga.getColumnModel().getColumn(22).setPreferredWidth(110);

        try {
            while (rs.next()) {
                retorno[0] = Integer.toString(rs.getInt(1));
                retorno[1] = rs.getString(2);
                retorno[2] = Integer.toString(rs.getInt(3));
                retorno[3] = Integer.toString(rs.getInt(4));
                retorno[4] = Integer.toString(rs.getInt(5));
                retorno[5] = Integer.toString(rs.getInt(6));
                retorno[6] = Integer.toString(rs.getInt(7));
                retorno[7] = rs.getString(8);
                retorno[8] = rs.getString(9);
                retorno[9] = Integer.toString(rs.getInt(10));
                retorno[10] = Integer.toString(rs.getInt(11));
                retorno[11] = rs.getString(12);
                retorno[12] = rs.getString(13);
                retorno[13] = Integer.toString(rs.getInt(14));
                retorno[14] = rs.getString(15);
                retorno[15] = rs.getString(16);
                retorno[16] = rs.getString(17);
                retorno[17] = Float.toString(rs.getFloat(18));
                retorno[18] = Float.toString(rs.getFloat(19));
                retorno[19] = Float.toString(rs.getFloat(20));
                retorno[20] = Float.toString(rs.getFloat(21));
                retorno[21] = Float.toString(rs.getFloat(22));
                retorno[22] = rs.getString(23);

                modelo.addRow(retorno);
            }
            tbListarCamiCarga.setModel(modelo);
            try {
                if (rs != null) {
                    rs.close();

                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro ao encerrar as conexões:");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro de conexão!:");
        }

    }

    public void carregarCategoria() {
        ResultSet rs;
        rs = gerenciarVeiculo.carregarCategoria();
        if (rs != null) {
            try {

                while (rs.next()) {
                    cbCategoria.addItem(rs.getString(1));
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");

                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Não foi possível carregar as categorias");

        }

    }

    public void carregarLocadora() {
        ResultSet rs;
        rs = gerenciarVeiculo.carregarLocadora();
        if (rs != null) {
            try {

                while (rs.next()) {
                    cbLocadoras.addItem(rs.getString(1));
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                        System.out.println("Conexão Encerrada");

                    }
                } catch (Exception e) {
                    System.out.println("Erro ao fechar a conexão");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Não foi possível carregar as locadoras");

        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgTipCarro = new javax.swing.ButtonGroup();
        bgTipoPesquisa = new javax.swing.ButtonGroup();
        jPMAutoPequeno = new javax.swing.JPopupMenu();
        jMEditarAutoPequeno = new javax.swing.JMenuItem();
        jMExcluirAutoPequeno = new javax.swing.JMenuItem();
        jPMCamCarga = new javax.swing.JPopupMenu();
        jMEditarCamCarga = new javax.swing.JMenuItem();
        jMExcluirCamCarga = new javax.swing.JMenuItem();
        jPMCamPassageiro = new javax.swing.JPopupMenu();
        jMEditarCamPassageiro = new javax.swing.JMenuItem();
        jMExcluirCamPassageiro = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtNChassi = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtNMotor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtTorque = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cbTipoGasolina = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        cbCor = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        txtKmAtual = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtAnoFab = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtAnoModelo = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtNPortas = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        cbLocadoras = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        cbCategoria = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jrCaminhoneteCarga = new javax.swing.JRadioButton();
        jrAutomovelPequeno = new javax.swing.JRadioButton();
        jrCaminhonetPassageiro = new javax.swing.JRadioButton();
        pAutomovelPequeno = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        txtTamanhoAutomovelPequeno = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        cbTipoCambio = new javax.swing.JComboBox<>();
        jcDVD = new javax.swing.JCheckBox();
        jcRadio = new javax.swing.JCheckBox();
        jcMP3 = new javax.swing.JCheckBox();
        jcDirecaoHidraulica = new javax.swing.JCheckBox();
        jcArcondicionado = new javax.swing.JCheckBox();
        jcCameraRe = new javax.swing.JCheckBox();
        btSalvar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btPesquisarVeiculo = new javax.swing.JButton();
        jTextField16 = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jrPesCarroPequeno = new javax.swing.JRadioButton();
        jrPesqCaminhoneteCarga = new javax.swing.JRadioButton();
        jrPesCaminhonetePassageiro = new javax.swing.JRadioButton();
        jLabel28 = new javax.swing.JLabel();
        jPListarVeiculos = new javax.swing.JPanel();
        jLAutomovelPequeno = new javax.swing.JLayeredPane();
        jSAutomovelPequeno = new javax.swing.JScrollPane();
        tbListarAutomovelPequeno = new javax.swing.JTable();
        jLCamiCarga = new javax.swing.JLayeredPane();
        jSCamiCarga = new javax.swing.JScrollPane();
        tbListarCamiCarga = new javax.swing.JTable();
        jLCamiPassageiro = new javax.swing.JLayeredPane();
        jSCamiPassageiro = new javax.swing.JScrollPane();
        tbListaCamiPassageiro = new javax.swing.JTable();
        pCaminhonetCarga = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txtCapacidadeCarga = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtVolCombustivel = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtDesempenho = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtDistanciaEixo = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtPotencia = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        cbEmbreagem = new javax.swing.JComboBox<>();
        pCaminhonetePassageiro = new javax.swing.JPanel();
        jcAirbag = new javax.swing.JCheckBox();
        jcCintoRetrateis = new javax.swing.JCheckBox();
        jcRodaLigaLeve = new javax.swing.JCheckBox();
        jcDirecaoAssistida = new javax.swing.JCheckBox();
        jcControlePoulicao = new javax.swing.JCheckBox();
        btAtualizar = new javax.swing.JButton();
        btCancelar = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        cbRevisoes = new javax.swing.JComboBox<>();
        jLabel24 = new javax.swing.JLabel();
        cbStatus = new javax.swing.JComboBox<>();
        jLabel25 = new javax.swing.JLabel();
        cbLimpeza = new javax.swing.JComboBox<>();
        jLabel26 = new javax.swing.JLabel();
        txtNOcupantes = new javax.swing.JTextField();
        txtPlaca = new javax.swing.JFormattedTextField();
        jMenuBar = new javax.swing.JMenuBar();
        jMenu = new javax.swing.JMenu();
        jMDevolucao = new javax.swing.JMenuItem();
        jMUsuario = new javax.swing.JMenuItem();
        jMCategoria = new javax.swing.JMenuItem();
        jMClientes = new javax.swing.JMenuItem();
        jMLocacao = new javax.swing.JMenuItem();
        jMLocadoras = new javax.swing.JMenuItem();
        jMReservas = new javax.swing.JMenuItem();
        jMVeiculos = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMInicios = new javax.swing.JMenuItem();
        jMenuRelatorios = new javax.swing.JMenu();
        jMCliente = new javax.swing.JMenuItem();
        jMReserva = new javax.swing.JMenuItem();
        jMRelatorioLocacao = new javax.swing.JMenuItem();
        jMInfoTempo = new javax.swing.JMenu();

        jMEditarAutoPequeno.setText("Editar");
        jMEditarAutoPequeno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMEditarAutoPequenoActionPerformed(evt);
            }
        });
        jPMAutoPequeno.add(jMEditarAutoPequeno);

        jMExcluirAutoPequeno.setText("Excluir");
        jMExcluirAutoPequeno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMExcluirAutoPequenoActionPerformed(evt);
            }
        });
        jPMAutoPequeno.add(jMExcluirAutoPequeno);

        jMEditarCamCarga.setText("Editar");
        jMEditarCamCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMEditarCamCargaActionPerformed(evt);
            }
        });
        jPMCamCarga.add(jMEditarCamCarga);

        jMExcluirCamCarga.setText("Excluir");
        jMExcluirCamCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMExcluirCamCargaActionPerformed(evt);
            }
        });
        jPMCamCarga.add(jMExcluirCamCarga);

        jMEditarCamPassageiro.setText("Editar");
        jMEditarCamPassageiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMEditarCamPassageiroActionPerformed(evt);
            }
        });
        jPMCamPassageiro.add(jMEditarCamPassageiro);

        jMExcluirCamPassageiro.setText("Excluir");
        jMExcluirCamPassageiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMExcluirCamPassageiroActionPerformed(evt);
            }
        });
        jPMCamPassageiro.add(jMExcluirCamPassageiro);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gerenciar Veículo");

        jLabel1.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Placa:");

        jLabel2.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nº Chassi:");

        jLabel3.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Nº Motor:");

        jLabel4.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Torque:");

        jLabel5.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Combustível:");

        cbTipoGasolina.setBackground(new java.awt.Color(255, 255, 255));
        cbTipoGasolina.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbTipoGasolina.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Gasonila", "Álcool", "Flex" }));

        jLabel6.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Cor:");

        cbCor.setBackground(new java.awt.Color(255, 255, 255));
        cbCor.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbCor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Preto", "Branco", "Prata", "Vermelho", "Azul", "Amarelo", "Laranja", "Roxo", "Vinho", "Verde" }));

        jLabel7.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("KM Atual:");

        jLabel8.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Ano Fab:");

        jLabel9.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Ano Modelo:");

        jLabel10.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Nº Portas:");

        jLabel11.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Categoria:");

        cbLocadoras.setBackground(new java.awt.Color(255, 255, 255));
        cbLocadoras.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Responsável:");

        cbCategoria.setBackground(new java.awt.Color(255, 255, 255));
        cbCategoria.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Tipo de Carro:");

        bgTipCarro.add(jrCaminhoneteCarga);
        jrCaminhoneteCarga.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrCaminhoneteCarga.setForeground(new java.awt.Color(255, 255, 255));
        jrCaminhoneteCarga.setText("Caminhonete de Carga");
        jrCaminhoneteCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrCaminhoneteCargaActionPerformed(evt);
            }
        });

        bgTipCarro.add(jrAutomovelPequeno);
        jrAutomovelPequeno.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrAutomovelPequeno.setForeground(new java.awt.Color(255, 255, 255));
        jrAutomovelPequeno.setText("Automóvel Pequeno");
        jrAutomovelPequeno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrAutomovelPequenoActionPerformed(evt);
            }
        });

        bgTipCarro.add(jrCaminhonetPassageiro);
        jrCaminhonetPassageiro.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrCaminhonetPassageiro.setForeground(new java.awt.Color(255, 255, 255));
        jrCaminhonetPassageiro.setText("Caminhonet Passageiro");
        jrCaminhonetPassageiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrCaminhonetPassageiroActionPerformed(evt);
            }
        });

        pAutomovelPequeno.setBackground(new java.awt.Color(2, 119, 189));
        pAutomovelPequeno.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        pAutomovelPequeno.setPreferredSize(new java.awt.Dimension(225, 221));

        jLabel14.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Tamanho:");

        jLabel15.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Cambio:");

        cbTipoCambio.setBackground(new java.awt.Color(255, 255, 255));
        cbTipoCambio.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbTipoCambio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Manual", "Autómático" }));

        jcDVD.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcDVD.setForeground(new java.awt.Color(255, 255, 255));
        jcDVD.setText("DVD");

        jcRadio.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcRadio.setForeground(new java.awt.Color(255, 255, 255));
        jcRadio.setText("Rádio");

        jcMP3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcMP3.setForeground(new java.awt.Color(255, 255, 255));
        jcMP3.setText("MP3");

        jcDirecaoHidraulica.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcDirecaoHidraulica.setForeground(new java.awt.Color(255, 255, 255));
        jcDirecaoHidraulica.setText("Direção Hidraulica");

        jcArcondicionado.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcArcondicionado.setForeground(new java.awt.Color(255, 255, 255));
        jcArcondicionado.setText("Ar-Condicionado");

        jcCameraRe.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcCameraRe.setForeground(new java.awt.Color(255, 255, 255));
        jcCameraRe.setText("Câmera de ré");

        javax.swing.GroupLayout pAutomovelPequenoLayout = new javax.swing.GroupLayout(pAutomovelPequeno);
        pAutomovelPequeno.setLayout(pAutomovelPequenoLayout);
        pAutomovelPequenoLayout.setHorizontalGroup(
            pAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pAutomovelPequenoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jcCameraRe)
                    .addComponent(jcArcondicionado)
                    .addGroup(pAutomovelPequenoLayout.createSequentialGroup()
                        .addComponent(jcRadio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcMP3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jcDVD))
                    .addGroup(pAutomovelPequenoLayout.createSequentialGroup()
                        .addGroup(pAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbTipoCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTamanhoAutomovelPequeno)))
                    .addComponent(jcDirecaoHidraulica))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        pAutomovelPequenoLayout.setVerticalGroup(
            pAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pAutomovelPequenoLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(pAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txtTamanhoAutomovelPequeno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(cbTipoCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcRadio)
                    .addComponent(jcMP3)
                    .addComponent(jcDVD))
                .addGap(18, 18, 18)
                .addComponent(jcDirecaoHidraulica)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jcArcondicionado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jcCameraRe)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btSalvar.setBackground(new java.awt.Color(255, 255, 255));
        btSalvar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        btSalvar.setText("Salvar");
        btSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(2, 119, 189));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        btPesquisarVeiculo.setBackground(new java.awt.Color(255, 255, 255));
        btPesquisarVeiculo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btPesquisarVeiculo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/pesquisar.png"))); // NOI18N
        btPesquisarVeiculo.setText("Pesquisar");
        btPesquisarVeiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btPesquisarVeiculoActionPerformed(evt);
            }
        });

        jLabel27.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setText("Pesquisar Por:");

        bgTipoPesquisa.add(jrPesCarroPequeno);
        jrPesCarroPequeno.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrPesCarroPequeno.setForeground(new java.awt.Color(255, 255, 255));
        jrPesCarroPequeno.setText("Carro Pequeno");
        jrPesCarroPequeno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrPesCarroPequenoActionPerformed(evt);
            }
        });

        bgTipoPesquisa.add(jrPesqCaminhoneteCarga);
        jrPesqCaminhoneteCarga.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrPesqCaminhoneteCarga.setForeground(new java.awt.Color(255, 255, 255));
        jrPesqCaminhoneteCarga.setText("Cami. de Carga");
        jrPesqCaminhoneteCarga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrPesqCaminhoneteCargaActionPerformed(evt);
            }
        });

        bgTipoPesquisa.add(jrPesCaminhonetePassageiro);
        jrPesCaminhonetePassageiro.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jrPesCaminhonetePassageiro.setForeground(new java.awt.Color(255, 255, 255));
        jrPesCaminhonetePassageiro.setText("Cami. de Passageiro");
        jrPesCaminhonetePassageiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jrPesCaminhonetePassageiroActionPerformed(evt);
            }
        });

        jLabel28.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setText("Placa:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jrPesCarroPequeno)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jrPesqCaminhoneteCarga)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jrPesCaminhonetePassageiro)
                        .addGap(79, 79, 79))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btPesquisarVeiculo)
                        .addGap(266, 266, 266))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(jrPesCarroPequeno)
                    .addComponent(jrPesqCaminhoneteCarga)
                    .addComponent(jrPesCaminhonetePassageiro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btPesquisarVeiculo)
                    .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28))
                .addContainerGap())
        );

        tbListarAutomovelPequeno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbListarAutomovelPequeno.setComponentPopupMenu(jPMAutoPequeno);
        jSAutomovelPequeno.setViewportView(tbListarAutomovelPequeno);

        jLAutomovelPequeno.setLayer(jSAutomovelPequeno, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLAutomovelPequenoLayout = new javax.swing.GroupLayout(jLAutomovelPequeno);
        jLAutomovelPequeno.setLayout(jLAutomovelPequenoLayout);
        jLAutomovelPequenoLayout.setHorizontalGroup(
            jLAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSAutomovelPequeno)
        );
        jLAutomovelPequenoLayout.setVerticalGroup(
            jLAutomovelPequenoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSAutomovelPequeno, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
        );

        tbListarCamiCarga.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbListarCamiCarga.setComponentPopupMenu(jPMCamCarga);
        jSCamiCarga.setViewportView(tbListarCamiCarga);

        jLCamiCarga.setLayer(jSCamiCarga, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLCamiCargaLayout = new javax.swing.GroupLayout(jLCamiCarga);
        jLCamiCarga.setLayout(jLCamiCargaLayout);
        jLCamiCargaLayout.setHorizontalGroup(
            jLCamiCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 831, Short.MAX_VALUE)
            .addGroup(jLCamiCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSCamiCarga, javax.swing.GroupLayout.DEFAULT_SIZE, 820, Short.MAX_VALUE))
        );
        jLCamiCargaLayout.setVerticalGroup(
            jLCamiCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 131, Short.MAX_VALUE)
            .addGroup(jLCamiCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSCamiCarga, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
        );

        tbListaCamiPassageiro.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbListaCamiPassageiro.setComponentPopupMenu(jPMCamPassageiro);
        jSCamiPassageiro.setViewportView(tbListaCamiPassageiro);

        jLCamiPassageiro.setLayer(jSCamiPassageiro, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLCamiPassageiroLayout = new javax.swing.GroupLayout(jLCamiPassageiro);
        jLCamiPassageiro.setLayout(jLCamiPassageiroLayout);
        jLCamiPassageiroLayout.setHorizontalGroup(
            jLCamiPassageiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 831, Short.MAX_VALUE)
            .addGroup(jLCamiPassageiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSCamiPassageiro, javax.swing.GroupLayout.DEFAULT_SIZE, 820, Short.MAX_VALUE))
        );
        jLCamiPassageiroLayout.setVerticalGroup(
            jLCamiPassageiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 131, Short.MAX_VALUE)
            .addGroup(jLCamiPassageiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSCamiPassageiro, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPListarVeiculosLayout = new javax.swing.GroupLayout(jPListarVeiculos);
        jPListarVeiculos.setLayout(jPListarVeiculosLayout);
        jPListarVeiculosLayout.setHorizontalGroup(
            jPListarVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLAutomovelPequeno)
            .addGroup(jPListarVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLCamiCarga))
            .addGroup(jPListarVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLCamiPassageiro))
        );
        jPListarVeiculosLayout.setVerticalGroup(
            jPListarVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLAutomovelPequeno)
            .addGroup(jPListarVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLCamiCarga, javax.swing.GroupLayout.Alignment.TRAILING))
            .addGroup(jPListarVeiculosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLCamiPassageiro))
        );

        pCaminhonetCarga.setBackground(new java.awt.Color(2, 119, 189));
        pCaminhonetCarga.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        pCaminhonetCarga.setPreferredSize(new java.awt.Dimension(259, 221));

        jLabel16.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Capacidade de Carga:");

        jLabel17.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("Vol. Combustível:");

        jLabel18.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Desempenho:");

        jLabel19.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Distância dos Eixos:");

        jLabel20.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("Potência:");

        jLabel21.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Embreagem:");

        cbEmbreagem.setBackground(new java.awt.Color(255, 255, 255));
        cbEmbreagem.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbEmbreagem.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Manual", "Hidraulica" }));

        javax.swing.GroupLayout pCaminhonetCargaLayout = new javax.swing.GroupLayout(pCaminhonetCarga);
        pCaminhonetCarga.setLayout(pCaminhonetCargaLayout);
        pCaminhonetCargaLayout.setHorizontalGroup(
            pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pCaminhonetCargaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel19)
                    .addComponent(jLabel18)
                    .addComponent(jLabel17)
                    .addComponent(jLabel16)
                    .addComponent(jLabel21)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pCaminhonetCargaLayout.createSequentialGroup()
                        .addComponent(cbEmbreagem, 0, 1, Short.MAX_VALUE)
                        .addGap(5, 5, 5))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pCaminhonetCargaLayout.createSequentialGroup()
                        .addComponent(txtCapacidadeCarga, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(pCaminhonetCargaLayout.createSequentialGroup()
                        .addComponent(txtVolCombustivel)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pCaminhonetCargaLayout.createSequentialGroup()
                        .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtPotencia, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDesempenho, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDistanciaEixo))
                        .addContainerGap())))
        );
        pCaminhonetCargaLayout.setVerticalGroup(
            pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pCaminhonetCargaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtCapacidadeCarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtVolCombustivel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txtDesempenho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtDistanciaEixo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtPotencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pCaminhonetCargaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(cbEmbreagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(51, Short.MAX_VALUE))
        );

        pCaminhonetePassageiro.setBackground(new java.awt.Color(2, 119, 189));
        pCaminhonetePassageiro.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        pCaminhonetePassageiro.setPreferredSize(new java.awt.Dimension(255, 221));

        jcAirbag.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcAirbag.setForeground(new java.awt.Color(255, 255, 255));
        jcAirbag.setText("Air-bag");

        jcCintoRetrateis.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcCintoRetrateis.setForeground(new java.awt.Color(255, 255, 255));
        jcCintoRetrateis.setText("Cinto retrateis");

        jcRodaLigaLeve.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcRodaLigaLeve.setForeground(new java.awt.Color(255, 255, 255));
        jcRodaLigaLeve.setText("Roda liga leve");

        jcDirecaoAssistida.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcDirecaoAssistida.setForeground(new java.awt.Color(255, 255, 255));
        jcDirecaoAssistida.setText("Direção Assistida");

        jcControlePoulicao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jcControlePoulicao.setForeground(new java.awt.Color(255, 255, 255));
        jcControlePoulicao.setText("Controle de Puluição de Ar");

        javax.swing.GroupLayout pCaminhonetePassageiroLayout = new javax.swing.GroupLayout(pCaminhonetePassageiro);
        pCaminhonetePassageiro.setLayout(pCaminhonetePassageiroLayout);
        pCaminhonetePassageiroLayout.setHorizontalGroup(
            pCaminhonetePassageiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pCaminhonetePassageiroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pCaminhonetePassageiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jcAirbag)
                    .addComponent(jcCintoRetrateis)
                    .addComponent(jcRodaLigaLeve)
                    .addComponent(jcDirecaoAssistida)
                    .addComponent(jcControlePoulicao))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pCaminhonetePassageiroLayout.setVerticalGroup(
            pCaminhonetePassageiroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pCaminhonetePassageiroLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jcAirbag)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jcCintoRetrateis)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jcRodaLigaLeve)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jcDirecaoAssistida)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jcControlePoulicao)
                .addContainerGap(57, Short.MAX_VALUE))
        );

        btAtualizar.setBackground(new java.awt.Color(255, 255, 255));
        btAtualizar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/atualizar.png"))); // NOI18N
        btAtualizar.setText("Atualizar");
        btAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAtualizarActionPerformed(evt);
            }
        });

        btCancelar.setBackground(new java.awt.Color(255, 255, 255));
        btCancelar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cancelar.png"))); // NOI18N
        btCancelar.setText("Cancelar");
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(2, 119, 189));

        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/CarroModelo.png"))); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel22)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel23.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Reviões:");

        cbRevisoes.setBackground(new java.awt.Color(255, 255, 255));
        cbRevisoes.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbRevisoes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ok", "Pendente" }));

        jLabel24.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("Status:");

        cbStatus.setBackground(new java.awt.Color(255, 255, 255));
        cbStatus.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Disponível", "Locado", "indisponível" }));

        jLabel25.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("Limpeza:");

        cbLimpeza.setBackground(new java.awt.Color(255, 255, 255));
        cbLimpeza.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        cbLimpeza.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ok", "Pendente" }));

        jLabel26.setFont(new java.awt.Font("Noto Serif", 1, 14)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setText("Nº Ocupantes:");

        try {
            txtPlaca.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("***-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jMenu.setBackground(new java.awt.Color(255, 255, 255));
        jMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Menu.png"))); // NOI18N
        jMenu.setText("Menu");
        jMenu.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMDevolucao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMDevolucao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/devolucao.png"))); // NOI18N
        jMDevolucao.setText("Efetuar Devolução");
        jMDevolucao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMDevolucaoActionPerformed(evt);
            }
        });
        jMenu.add(jMDevolucao);

        jMUsuario.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/AdicionarFuncionario.png"))); // NOI18N
        jMUsuario.setText("Gerenciar Usuários");
        jMUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMUsuarioActionPerformed(evt);
            }
        });
        jMenu.add(jMUsuario);

        jMCategoria.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCategoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarCategoria.png"))); // NOI18N
        jMCategoria.setText("Gerenciar Categoria");
        jMCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMCategoriaActionPerformed(evt);
            }
        });
        jMenu.add(jMCategoria);

        jMClientes.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/login.png"))); // NOI18N
        jMClientes.setText("Gerenciar Clientes");
        jMClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClientesActionPerformed(evt);
            }
        });
        jMenu.add(jMClientes);

        jMLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocao.png"))); // NOI18N
        jMLocacao.setText("Gerenciar Locações");
        jMLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocacaoActionPerformed(evt);
            }
        });
        jMenu.add(jMLocacao);

        jMLocadoras.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMLocadoras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarLocadoras.png"))); // NOI18N
        jMLocadoras.setText("Gerenciar Locadoras");
        jMLocadoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMLocadorasActionPerformed(evt);
            }
        });
        jMenu.add(jMLocadoras);

        jMReservas.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReservas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarReserva.png"))); // NOI18N
        jMReservas.setText("Gerenciar Reservas");
        jMReservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservasActionPerformed(evt);
            }
        });
        jMenu.add(jMReservas);

        jMVeiculos.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMVeiculos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/gerenciarVeiculo.png"))); // NOI18N
        jMVeiculos.setText("Gerenciar Veículos");
        jMVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMVeiculosActionPerformed(evt);
            }
        });
        jMenu.add(jMVeiculos);
        jMenu.add(jSeparator2);

        jMInicios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMInicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/home.png"))); // NOI18N
        jMInicios.setText("Tela Principal");
        jMInicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMIniciosActionPerformed(evt);
            }
        });
        jMenu.add(jMInicios);

        jMenuBar.add(jMenu);

        jMenuRelatorios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/MenuRelatorios.png"))); // NOI18N
        jMenuRelatorios.setText("Relatórios");
        jMenuRelatorios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jMCliente.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cliente.png"))); // NOI18N
        jMCliente.setText("Clientes");
        jMCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMClienteActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMCliente);

        jMReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMReserva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioReservas.png"))); // NOI18N
        jMReserva.setText("Reservas");
        jMReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMReservaActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMReserva);

        jMRelatorioLocacao.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jMRelatorioLocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/RelatorioLocacao.png"))); // NOI18N
        jMRelatorioLocacao.setText("Locações");
        jMRelatorioLocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMRelatorioLocacaoActionPerformed(evt);
            }
        });
        jMenuRelatorios.add(jMRelatorioLocacao);

        jMenuBar.add(jMenuRelatorios);

        jMInfoTempo.setForeground(new java.awt.Color(0, 0, 0));
        jMInfoTempo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dataMenu.png"))); // NOI18N
        jMInfoTempo.setText("tempo");
        jMInfoTempo.setEnabled(false);
        jMInfoTempo.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        jMenuBar.add(jMInfoTempo);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jrAutomovelPequeno)
                        .addGap(18, 18, 18)
                        .addComponent(jrCaminhoneteCarga)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jrCaminhonetPassageiro)
                        .addGap(59, 59, 59))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(pAutomovelPequeno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pCaminhonetCarga, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pCaminhonetePassageiro, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel7)
                            .addComponent(jLabel4)
                            .addComponent(jLabel1)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtTorque, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                                    .addComponent(txtPlaca))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel5))
                                .addGap(2, 2, 2)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbTipoGasolina, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtNChassi)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtNOcupantes, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(36, 36, 36)
                                        .addComponent(jLabel23))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtKmAtual, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txtNPortas, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(23, 23, 23)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel8)
                                            .addComponent(jLabel11))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtAnoFab)
                                    .addComponent(cbCategoria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbRevisoes, 0, 1, Short.MAX_VALUE))))
                        .addGap(17, 17, 17)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel9)
                                        .addComponent(jLabel12))
                                    .addComponent(jLabel24))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtAnoModelo)
                                    .addComponent(cbLocadoras, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbStatus, 0, 1, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNMotor)
                                    .addComponent(cbCor, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbLimpeza, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
            .addComponent(jPListarVeiculos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btSalvar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btAtualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btCancelar)
                .addGap(205, 205, 205))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(txtNMotor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(cbCor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(txtAnoModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(cbLocadoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(txtNOcupantes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(txtNChassi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPlaca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtTorque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(cbTipoGasolina, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtKmAtual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(txtAnoFab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(txtNPortas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(cbCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel23)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cbRevisoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel24)
                                .addComponent(cbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel25)
                                .addComponent(cbLimpeza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jrCaminhoneteCarga)
                        .addComponent(jrCaminhonetPassageiro)
                        .addComponent(jrAutomovelPequeno)))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pCaminhonetePassageiro, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pCaminhonetCarga, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pAutomovelPequeno, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btSalvar)
                    .addComponent(btAtualizar)
                    .addComponent(btCancelar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPListarVeiculos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jrAutomovelPequenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrAutomovelPequenoActionPerformed
        // TODO add your handling code here:
        if (jrAutomovelPequeno.isSelected()) {
            ativarCamposAutomovelPequeno();
            desativarCamposCaminhoneteCarga();
            desativarComponenetesCaminhonetePassageiro();
        }
    }//GEN-LAST:event_jrAutomovelPequenoActionPerformed

    private void jrCaminhoneteCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrCaminhoneteCargaActionPerformed
        // TODO add your handling code here:
        if (jrCaminhoneteCarga.isSelected()) {
            ativarCamposCaminhoneteCarga();
            desativarComponenetesCaminhonetePassageiro();
            desativarCamposAutomovelPequeno();

        }
    }//GEN-LAST:event_jrCaminhoneteCargaActionPerformed

    private void jrCaminhonetPassageiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrCaminhonetPassageiroActionPerformed
        // TODO add your handling code here:
        if (jrCaminhonetPassageiro.isSelected()) {
            ativarComponenetesCaminhonetePassageiro();
            desativarCamposAutomovelPequeno();
            desativarCamposCaminhoneteCarga();

        }
    }//GEN-LAST:event_jrCaminhonetPassageiroActionPerformed

    private void btSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarActionPerformed
        // TODO add your handling code here:
        //---------------------Inicio padrão---------------------------\\
        String placa = this.txtPlaca.getText();
        String nChassi = this.txtNChassi.getText();
        String nMotor = this.txtNMotor.getText();
        int torque = Integer.parseInt(this.txtTorque.getText());
        String combustivel = this.cbTipoGasolina.getSelectedItem().toString();
        String cor = this.cbCor.getSelectedItem().toString();
        int kmAtual = Integer.parseInt(this.txtKmAtual.getText());
        int anoFab = Integer.parseInt(this.txtAnoFab.getText());
        int anoModelo = Integer.parseInt(this.txtAnoModelo.getText());
        int nPortas = Integer.parseInt(this.txtNPortas.getText());
        String categoria = this.cbCategoria.getSelectedItem().toString();
        String locadoras = this.cbLocadoras.getSelectedItem().toString();
        int nOcupantes = Integer.parseInt(this.txtNOcupantes.getText());
        String revisoes = this.cbRevisoes.getSelectedItem().toString();
        String status = this.cbStatus.getSelectedItem().toString();
        String limpeza = this.cbLimpeza.getSelectedItem().toString();
        //---------------------fim padrão---------------------------\\

        if (jrAutomovelPequeno.isSelected() == true) {
            float tamanho = Float.parseFloat(this.txtTamanhoAutomovelPequeno.getText());
            String cambio = this.cbTipoCambio.getSelectedItem().toString();
            boolean radio = this.jcRadio.isSelected();
            boolean mp3 = this.jcMP3.isSelected();
            boolean dvd = this.jcDVD.isSelected();
            boolean direcaoHidraulica = this.jcDirecaoHidraulica.isSelected();
            boolean arCondicionado = this.jcArcondicionado.isSelected();
            boolean cameraRe = this.jcCameraRe.isSelected();
            if (gerenciarVeiculo.adicionarVeiculoPequeno(radio, mp3, tamanho, cameraRe, direcaoHidraulica,
                    cambio, arCondicionado, dvd, placa, nPortas, nMotor, combustivel, torque, cor,
                    kmAtual, anoModelo, anoFab, nPortas, categoria, locadoras, nOcupantes, revisoes, limpeza, status) == true) {

                JOptionPane.showMessageDialog(null, "Adicionado com sucesso!");
                listarAutomovelPequeno("");
                limparCampos();

            } else {
                JOptionPane.showMessageDialog(null, "Erro ao gravar dados!");
            }

        } else if (jrCaminhoneteCarga.isSelected() == true) {
            float capacidadeCarga = Float.parseFloat(this.txtCapacidadeCarga.getText());
            float volCombustivel = Float.parseFloat(this.txtVolCombustivel.getText());
            float desempenho = Float.parseFloat(this.txtDesempenho.getText());
            float distanciaEixos = Float.parseFloat(this.txtDistanciaEixo.getText());
            float potencia = Float.parseFloat(this.txtPotencia.getText());
            String embreagem = this.cbEmbreagem.getSelectedItem().toString();

            if (gerenciarVeiculo.adicionarCaminhoneteCarga(capacidadeCarga, volCombustivel, desempenho,
                    distanciaEixos, potencia, embreagem, placa, nPortas, nMotor, combustivel, torque,
                    cor, kmAtual, anoModelo, anoFab, nPortas, categoria, locadoras, nOcupantes, revisoes,
                    limpeza, status) == true) {

                JOptionPane.showMessageDialog(null, "Adicionado com sucesso!");
                listarCaminhonetCarga("");
                limparCampos();

            } else {
                JOptionPane.showMessageDialog(null, "Erro ao gravar dados!");
            }

        } else if (jrCaminhonetPassageiro.isSelected()) {

            boolean airBag = this.jcAirbag.isSelected();
            boolean cintosRetrateis = this.jcCintoRetrateis.isSelected();
            boolean rodaLigaLeve = this.jcRodaLigaLeve.isSelected();
            boolean direcaoAssistida = this.jcDirecaoAssistida.isSelected();
            boolean controlePoulicao = this.jcControlePoulicao.isSelected();

            if (gerenciarVeiculo.adicionarCaminhonetePassageiro(cintosRetrateis, airBag, rodaLigaLeve,
                    direcaoAssistida, controlePoulicao, placa, nPortas, nMotor, combustivel, torque, cor,
                    kmAtual, anoModelo, anoFab, nPortas, categoria, locadoras, nOcupantes, revisoes, limpeza,
                    status) == true) {

                JOptionPane.showMessageDialog(null, "Adicionado com sucesso!");
                listarCamPassageiro("");
                limparCampos();

            } else {
                JOptionPane.showMessageDialog(null, "Erro ao gravar dados!");
            }

        }


    }//GEN-LAST:event_btSalvarActionPerformed

    private void jrPesqCaminhoneteCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrPesqCaminhoneteCargaActionPerformed
        // TODO add your handling code here:
        if (jrPesqCaminhoneteCarga.isSelected()) {
            jLAutomovelPequeno.setVisible(false);
            jLCamiCarga.setVisible(true);
            jLCamiPassageiro.setVisible(false);
            listarCaminhonetCarga("");
        }
    }//GEN-LAST:event_jrPesqCaminhoneteCargaActionPerformed

    private void jrPesCaminhonetePassageiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrPesCaminhonetePassageiroActionPerformed
        // TODO add your handling code here:
        if (jrPesCaminhonetePassageiro.isSelected()) {
            jLAutomovelPequeno.setVisible(false);
            jLCamiCarga.setVisible(false);
            jLCamiPassageiro.setVisible(true);
            listarCamPassageiro("");

        }

    }//GEN-LAST:event_jrPesCaminhonetePassageiroActionPerformed

    private void jrPesCarroPequenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jrPesCarroPequenoActionPerformed
        // TODO add your handling code here:
        if (jrPesCarroPequeno.isSelected()) {
            jLAutomovelPequeno.setVisible(true);
            jLCamiCarga.setVisible(false);
            jLCamiPassageiro.setVisible(false);
            listarAutomovelPequeno("");

        }
    }//GEN-LAST:event_jrPesCarroPequenoActionPerformed

    private void jMExcluirAutoPequenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMExcluirAutoPequenoActionPerformed
        // TODO add your handling code here:
        int itemSelecionado = tbListarAutomovelPequeno.getSelectedRow();
        boolean confirmacao;
        if (itemSelecionado >= 0) {
            String deletar = tbListarAutomovelPequeno.getValueAt(itemSelecionado, 0).toString();
            System.err.println("");
            Object[] botoes = {"Sim", "Não"};
            int resposta = JOptionPane.showOptionDialog(null,
                    "Deseja Excluir esse item?",
                    "Confirmação", // o título da janela
                    JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    botoes, botoes[0]);
            confirmacao = gerenciarVeiculo.deletarAutoPequeno(deletar, resposta);
            if (confirmacao == true) {
                JOptionPane.showMessageDialog(null, "Excluído!");
                listarAutomovelPequeno("");
            } else {
                JOptionPane.showMessageDialog(null, "Erro ao  tentar excluir!");
            }

        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }
    }//GEN-LAST:event_jMExcluirAutoPequenoActionPerformed

    private void jMExcluirCamCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMExcluirCamCargaActionPerformed
        // TODO add your handling code here:
        int itemSelecionado = tbListarCamiCarga.getSelectedRow();
        boolean confirmacao;
        if (itemSelecionado >= 0) {
            String deletar = tbListarCamiCarga.getValueAt(itemSelecionado, 0).toString();
            System.err.println("");
            Object[] botoes = {"Sim", "Não"};
            int resposta = JOptionPane.showOptionDialog(null,
                    "Deseja Excluir esse item?",
                    "Confirmação", // o título da janela
                    JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    botoes, botoes[0]);
            confirmacao = gerenciarVeiculo.deletarCamCarga(deletar, resposta);
            if (confirmacao == true) {
                JOptionPane.showMessageDialog(null, "Excluído!");
                listarCaminhonetCarga("");
            } else {
                JOptionPane.showMessageDialog(null, "Erro ao  tentar excluir!");
            }

        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }
    }//GEN-LAST:event_jMExcluirCamCargaActionPerformed

    private void jMExcluirCamPassageiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMExcluirCamPassageiroActionPerformed
        // TODO add your handling code here:
        int itemSelecionado = tbListaCamiPassageiro.getSelectedRow();
        boolean confirmacao;
        if (itemSelecionado >= 0) {
            String deletar = tbListaCamiPassageiro.getValueAt(itemSelecionado, 0).toString();
            System.err.println("");
            Object[] botoes = {"Sim", "Não"};
            int resposta = JOptionPane.showOptionDialog(null,
                    "Deseja Excluir esse item?",
                    "Confirmação", // o título da janela
                    JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                    botoes, botoes[0]);
            if (resposta == 0) {
                confirmacao = gerenciarVeiculo.deletarCamPassageiro(deletar, resposta);
                if (confirmacao == true) {
                    JOptionPane.showMessageDialog(null, "Excluído!");
                    listarCamPassageiro("");
                } else {
                    JOptionPane.showMessageDialog(null, "Erro ao  tentar excluir!");
                }
            }

        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }
    }//GEN-LAST:event_jMExcluirCamPassageiroActionPerformed

    private void jMEditarAutoPequenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMEditarAutoPequenoActionPerformed
        // TODO add your handling code here:
        int itemSelecionado = tbListarAutomovelPequeno.getSelectedRow();
        if (itemSelecionado >= 0) {
            idAutomovel = tbListarAutomovelPequeno.getValueAt(itemSelecionado, 0).toString();
            this.cbLocadoras.setSelectedItem(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 1).toString());
            this.txtAnoModelo.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 2).toString());
            this.txtNPortas.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 3).toString());
            this.txtNChassi.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 4).toString());
            this.txtTorque.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 5).toString());
            this.txtNMotor.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 6).toString());
            this.txtPlaca.setText(utiliatiro.convertePlaca(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 7).toString()));
            this.cbCategoria.setSelectedItem(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 8).toString());
            this.txtAnoFab.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 9).toString());
            this.txtKmAtual.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 10).toString());
            this.cbCor.setSelectedItem(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 11).toString());
            this.cbTipoGasolina.setSelectedItem(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 12).toString());
            this.txtNOcupantes.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 13).toString());
            this.cbRevisoes.setSelectedItem(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 14).toString());
            this.cbStatus.setSelectedItem(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 15).toString());
            this.cbLimpeza.setSelectedItem(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 16).toString());
            if (tbListarAutomovelPequeno.getValueAt(itemSelecionado, 17).toString().equals("true")) {
                this.jcRadio.setSelected(true);
            }
            if (tbListarAutomovelPequeno.getValueAt(itemSelecionado, 18).toString().equals("true")) {
                this.jcMP3.setSelected(true);
            }
            if (tbListarAutomovelPequeno.getValueAt(itemSelecionado, 19).toString().equals("true")) {
                this.jcDVD.setSelected(true);
            }
            this.txtTamanhoAutomovelPequeno.setText(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 20).toString());

            if (tbListarAutomovelPequeno.getValueAt(itemSelecionado, 21).toString().equals("true")) {
                this.jcCameraRe.setSelected(true);
            }
            if (tbListarAutomovelPequeno.getValueAt(itemSelecionado, 22).toString().equals("true")) {
                this.jcDirecaoHidraulica.setSelected(true);
            }
            this.cbTipoCambio.setSelectedItem(tbListarAutomovelPequeno.getValueAt(itemSelecionado, 23).toString());
            if (tbListarAutomovelPequeno.getValueAt(itemSelecionado, 24).toString().equals("true")) {
                this.jcArcondicionado.setSelected(true);

            }
            jrAutomovelPequeno.setSelected(true);
        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }
    }//GEN-LAST:event_jMEditarAutoPequenoActionPerformed

    private void jMEditarCamCargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMEditarCamCargaActionPerformed
        // TODO add your handling code here:
        int itemSelecionado = tbListarCamiCarga.getSelectedRow();
        if (itemSelecionado >= 0) {
            idAutomovel = tbListarCamiCarga.getValueAt(itemSelecionado, 0).toString();
            this.cbLocadoras.setSelectedItem(tbListarCamiCarga.getValueAt(itemSelecionado, 1).toString());
            this.txtAnoModelo.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 2).toString());
            this.txtNPortas.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 3).toString());
            this.txtNChassi.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 4).toString());
            this.txtTorque.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 5).toString());
            this.txtNMotor.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 6).toString());
            this.txtPlaca.setText(utiliatiro.convertePlaca(tbListarCamiCarga.getValueAt(itemSelecionado, 7).toString()));
            this.cbCategoria.setSelectedItem(tbListarCamiCarga.getValueAt(itemSelecionado, 8).toString());
            this.txtAnoFab.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 9).toString());
            this.txtKmAtual.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 10).toString());
            this.cbCor.setSelectedItem(tbListarCamiCarga.getValueAt(itemSelecionado, 11).toString());
            this.cbTipoGasolina.setSelectedItem(tbListarCamiCarga.getValueAt(itemSelecionado, 12).toString());
            this.txtNOcupantes.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 13).toString());
            this.cbRevisoes.setSelectedItem(tbListarCamiCarga.getValueAt(itemSelecionado, 14).toString());
            this.cbStatus.setSelectedItem(tbListarCamiCarga.getValueAt(itemSelecionado, 15).toString());
            this.cbLimpeza.setSelectedItem(tbListarCamiCarga.getValueAt(itemSelecionado, 16).toString());
            this.txtCapacidadeCarga.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 17).toString());
            this.txtVolCombustivel.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 18).toString());
            this.txtDesempenho.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 19).toString());
            this.txtDistanciaEixo.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 20).toString());
            this.txtPotencia.setText(tbListarCamiCarga.getValueAt(itemSelecionado, 21).toString());
            this.cbEmbreagem.setSelectedItem(tbListarCamiCarga.getValueAt(itemSelecionado, 22).toString());

            jrCaminhoneteCarga.setSelected(true);

        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }
    }//GEN-LAST:event_jMEditarCamCargaActionPerformed

    private void btAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAtualizarActionPerformed
        // TODO add your handling code here:
        String placa = this.txtPlaca.getText();
        String nChassi = this.txtNChassi.getText();
        String nMotor = this.txtNMotor.getText();
        int torque = Integer.parseInt(this.txtTorque.getText());
        String combustivel = this.cbTipoGasolina.getSelectedItem().toString();
        String cor = this.cbCor.getSelectedItem().toString();
        int kmAtual = Integer.parseInt(this.txtKmAtual.getText());
        int anoFab = Integer.parseInt(this.txtAnoFab.getText());
        int anoModelo = Integer.parseInt(this.txtAnoModelo.getText());
        int nPortas = Integer.parseInt(this.txtNPortas.getText());
        String categoria = this.cbCategoria.getSelectedItem().toString();
        String locadoras = this.cbLocadoras.getSelectedItem().toString();
        int nOcupantes = Integer.parseInt(this.txtNOcupantes.getText());
        String revisoes = this.cbRevisoes.getSelectedItem().toString();
        String status = this.cbStatus.getSelectedItem().toString();
        String limpeza = this.cbLimpeza.getSelectedItem().toString();
        //---------------------fim padrão---------------------------\\

        if (jrAutomovelPequeno.isSelected() == true) {
            float tamanho = Float.parseFloat(this.txtTamanhoAutomovelPequeno.getText());
            String cambio = this.cbTipoCambio.getSelectedItem().toString();
            boolean radio = this.jcRadio.isSelected();
            boolean mp3 = this.jcMP3.isSelected();
            boolean dvd = this.jcDVD.isSelected();
            boolean direcaoHidraulica = this.jcDirecaoHidraulica.isSelected();
            boolean arCondicionado = this.jcArcondicionado.isSelected();
            boolean cameraRe = this.jcCameraRe.isSelected();
            if (gerenciarVeiculo.alterarAutoPequeno(radio, mp3, tamanho, cameraRe, direcaoHidraulica,
                    cambio, arCondicionado, dvd, placa, nPortas, nMotor, combustivel, torque, cor,
                    kmAtual, anoModelo, anoFab, nPortas, categoria, locadoras, nOcupantes, revisoes, limpeza, status, idAutomovel) == true) {

                JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
                listarAutomovelPequeno("");

            } else {
                JOptionPane.showMessageDialog(null, "Erro ao Alterar dados!");
            }
        } else if (jrCaminhoneteCarga.isSelected() == true) {
            float capacidadeCarga = Float.parseFloat(this.txtCapacidadeCarga.getText());
            float volCombustivel = Float.parseFloat(this.txtVolCombustivel.getText());
            float desempenho = Float.parseFloat(this.txtDesempenho.getText());
            float distanciaEixos = Float.parseFloat(this.txtDistanciaEixo.getText());
            float potencia = Float.parseFloat(this.txtPotencia.getText());
            String embreagem = this.cbEmbreagem.getSelectedItem().toString();

            if (gerenciarVeiculo.alterarCamCarga(capacidadeCarga, volCombustivel, desempenho,
                    distanciaEixos, potencia, embreagem, placa, nPortas, nMotor, combustivel, torque,
                    cor, kmAtual, anoModelo, anoFab, nPortas, categoria, locadoras, nOcupantes, revisoes,
                    limpeza, status, idAutomovel) == true) {

                JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
                listarCaminhonetCarga("");

            } else {
                JOptionPane.showMessageDialog(null, "Erro ao alterar dados!");
            }

        } else if (jrCaminhonetPassageiro.isSelected()) {

            boolean airBag = this.jcAirbag.isSelected();
            boolean cintosRetrateis = this.jcCintoRetrateis.isSelected();
            boolean rodaLigaLeve = this.jcRodaLigaLeve.isSelected();
            boolean direcaoAssistida = this.jcDirecaoAssistida.isSelected();
            boolean controlePoulicao = this.jcControlePoulicao.isSelected();

            if (gerenciarVeiculo.alterarCamPassageiro(cintosRetrateis, airBag, rodaLigaLeve,
                    direcaoAssistida, controlePoulicao, placa, nPortas, nMotor, combustivel, torque, cor,
                    kmAtual, anoModelo, anoFab, nPortas, categoria, locadoras, nOcupantes, revisoes, limpeza,
                    status, idAutomovel) == true) {

                JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
                listarCamPassageiro("");

            } else {
                JOptionPane.showMessageDialog(null, "Erro ao Atualizar dados!");
            }

        }
    }//GEN-LAST:event_btAtualizarActionPerformed

    private void jMEditarCamPassageiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMEditarCamPassageiroActionPerformed
        // TODO add your handling code here:

        int itemSelecionado = tbListaCamiPassageiro.getSelectedRow();
        if (itemSelecionado >= 0) {
            idAutomovel = tbListaCamiPassageiro.getValueAt(itemSelecionado, 0).toString();
            this.cbLocadoras.setSelectedItem(tbListaCamiPassageiro.getValueAt(itemSelecionado, 1).toString());
            this.txtAnoModelo.setText(tbListaCamiPassageiro.getValueAt(itemSelecionado, 2).toString());
            this.txtNPortas.setText(tbListaCamiPassageiro.getValueAt(itemSelecionado, 3).toString());
            this.txtNChassi.setText(tbListaCamiPassageiro.getValueAt(itemSelecionado, 4).toString());
            this.txtTorque.setText(tbListaCamiPassageiro.getValueAt(itemSelecionado, 5).toString());
            this.txtNMotor.setText(tbListaCamiPassageiro.getValueAt(itemSelecionado, 6).toString());
            this.txtPlaca.setText(utiliatiro.convertePlaca(tbListaCamiPassageiro.getValueAt(itemSelecionado, 7).toString()));
            this.cbCategoria.setSelectedItem(tbListaCamiPassageiro.getValueAt(itemSelecionado, 8).toString());
            this.txtAnoFab.setText(tbListaCamiPassageiro.getValueAt(itemSelecionado, 9).toString());
            this.txtKmAtual.setText(tbListaCamiPassageiro.getValueAt(itemSelecionado, 10).toString());
            this.cbCor.setSelectedItem(tbListaCamiPassageiro.getValueAt(itemSelecionado, 11).toString());
            this.cbTipoGasolina.setSelectedItem(tbListaCamiPassageiro.getValueAt(itemSelecionado, 12).toString());
            this.txtNOcupantes.setText(tbListaCamiPassageiro.getValueAt(itemSelecionado, 13).toString());
            this.cbRevisoes.setSelectedItem(tbListaCamiPassageiro.getValueAt(itemSelecionado, 14).toString());
            this.cbStatus.setSelectedItem(tbListaCamiPassageiro.getValueAt(itemSelecionado, 15).toString());
            this.cbLimpeza.setSelectedItem(tbListaCamiPassageiro.getValueAt(itemSelecionado, 16).toString());

            if (tbListaCamiPassageiro.getValueAt(itemSelecionado, 17).toString().equals("true")) {
                this.jcAirbag.setSelected(true);
            }
            if (tbListaCamiPassageiro.getValueAt(itemSelecionado, 18).toString().equals("true")) {
                this.jcControlePoulicao.setSelected(true);
            }
            if (tbListaCamiPassageiro.getValueAt(itemSelecionado, 19).toString().equals("true")) {
                this.jcCintoRetrateis.setSelected(true);
            }
            if (tbListaCamiPassageiro.getValueAt(itemSelecionado, 20).toString().equals("true")) {
                this.jcDirecaoAssistida.setSelected(true);
            }
            if (tbListaCamiPassageiro.getValueAt(itemSelecionado, 20).toString().equals("true")) {
                this.jcRodaLigaLeve.setSelected(true);
            }
            jrCaminhonetPassageiro.setSelected(true);
        } else {
            JOptionPane.showMessageDialog(null, "Item não selecionado!");
        }

    }//GEN-LAST:event_jMEditarCamPassageiroActionPerformed

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        // TODO add your handling code here:
        limparCampos();
    }//GEN-LAST:event_btCancelarActionPerformed

    private void btPesquisarVeiculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btPesquisarVeiculoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btPesquisarVeiculoActionPerformed

    private void jMDevolucaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMDevolucaoActionPerformed
        // TODO add your handling code here:
        Devolucao devolucao = new Devolucao();
        devolucao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMDevolucaoActionPerformed

    private void jMUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMUsuarioActionPerformed
        // TODO add your handling code here:
        GerencialUsuario usuario = new GerencialUsuario();
        usuario.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMUsuarioActionPerformed

    private void jMCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMCategoriaActionPerformed
        // TODO add your handling code here:
        GerenciarCategoria categoria = new GerenciarCategoria();
        categoria.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMCategoriaActionPerformed

    private void jMClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClientesActionPerformed
        // TODO add your handling code here:
        GerenciarCliente clientes = new GerenciarCliente();
        clientes.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClientesActionPerformed

    private void jMLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocacaoActionPerformed
        // TODO add your handling code here:
        GerenciarLocacao locacao = new GerenciarLocacao();
        locacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocacaoActionPerformed

    private void jMLocadorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMLocadorasActionPerformed
        // TODO add your handling code here:
        GerenciarLocadora locadora = new GerenciarLocadora();
        locadora.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMLocadorasActionPerformed

    private void jMReservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservasActionPerformed
        // TODO add your handling code here:
        GerenciarReserva reservas = new GerenciarReserva();
        reservas.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservasActionPerformed

    private void jMVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMVeiculosActionPerformed
        // TODO add your handling code here:
        GerenciarVeiculos gerenciarVeiculo = new GerenciarVeiculos();
        gerenciarVeiculo.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMVeiculosActionPerformed

    private void jMIniciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMIniciosActionPerformed
        // TODO add your handling code here:
        Menu menu = new Menu();
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMIniciosActionPerformed

    private void jMClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMClienteActionPerformed
        // TODO add your handling code here:
        RelatorioPorCliente relatorioCliente = new RelatorioPorCliente();
        relatorioCliente.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMClienteActionPerformed

    private void jMReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMReservaActionPerformed
        // TODO add your handling code here:
        RelatorioReserva relatorioReserva = new RelatorioReserva();
        relatorioReserva.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMReservaActionPerformed

    private void jMRelatorioLocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMRelatorioLocacaoActionPerformed
        // TODO add your handling code here:
        RelatorioLocacao relatorioLocacao = new RelatorioLocacao();
        relatorioLocacao.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMRelatorioLocacaoActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(GerenciarVeiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(GerenciarVeiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(GerenciarVeiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(GerenciarVeiculos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new GerenciarVeiculos().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgTipCarro;
    private javax.swing.ButtonGroup bgTipoPesquisa;
    private javax.swing.JButton btAtualizar;
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btPesquisarVeiculo;
    private javax.swing.JButton btSalvar;
    private javax.swing.JComboBox<String> cbCategoria;
    private javax.swing.JComboBox<String> cbCor;
    private javax.swing.JComboBox<String> cbEmbreagem;
    private javax.swing.JComboBox<String> cbLimpeza;
    private javax.swing.JComboBox<String> cbLocadoras;
    private javax.swing.JComboBox<String> cbRevisoes;
    private javax.swing.JComboBox<String> cbStatus;
    private javax.swing.JComboBox<String> cbTipoCambio;
    private javax.swing.JComboBox<String> cbTipoGasolina;
    private javax.swing.JLayeredPane jLAutomovelPequeno;
    private javax.swing.JLayeredPane jLCamiCarga;
    private javax.swing.JLayeredPane jLCamiPassageiro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMCategoria;
    private javax.swing.JMenuItem jMCliente;
    private javax.swing.JMenuItem jMClientes;
    private javax.swing.JMenuItem jMDevolucao;
    private javax.swing.JMenuItem jMEditarAutoPequeno;
    private javax.swing.JMenuItem jMEditarCamCarga;
    private javax.swing.JMenuItem jMEditarCamPassageiro;
    private javax.swing.JMenuItem jMExcluirAutoPequeno;
    private javax.swing.JMenuItem jMExcluirCamCarga;
    private javax.swing.JMenuItem jMExcluirCamPassageiro;
    private javax.swing.JMenu jMInfoTempo;
    private javax.swing.JMenuItem jMInicios;
    private javax.swing.JMenuItem jMLocacao;
    private javax.swing.JMenuItem jMLocadoras;
    private javax.swing.JMenuItem jMRelatorioLocacao;
    private javax.swing.JMenuItem jMReserva;
    private javax.swing.JMenuItem jMReservas;
    private javax.swing.JMenuItem jMUsuario;
    private javax.swing.JMenuItem jMVeiculos;
    private javax.swing.JMenu jMenu;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuRelatorios;
    private javax.swing.JPanel jPListarVeiculos;
    private javax.swing.JPopupMenu jPMAutoPequeno;
    private javax.swing.JPopupMenu jPMCamCarga;
    private javax.swing.JPopupMenu jPMCamPassageiro;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jSAutomovelPequeno;
    private javax.swing.JScrollPane jSCamiCarga;
    private javax.swing.JScrollPane jSCamiPassageiro;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JCheckBox jcAirbag;
    private javax.swing.JCheckBox jcArcondicionado;
    private javax.swing.JCheckBox jcCameraRe;
    private javax.swing.JCheckBox jcCintoRetrateis;
    private javax.swing.JCheckBox jcControlePoulicao;
    private javax.swing.JCheckBox jcDVD;
    private javax.swing.JCheckBox jcDirecaoAssistida;
    private javax.swing.JCheckBox jcDirecaoHidraulica;
    private javax.swing.JCheckBox jcMP3;
    private javax.swing.JCheckBox jcRadio;
    private javax.swing.JCheckBox jcRodaLigaLeve;
    private javax.swing.JRadioButton jrAutomovelPequeno;
    private javax.swing.JRadioButton jrCaminhonetPassageiro;
    private javax.swing.JRadioButton jrCaminhoneteCarga;
    private javax.swing.JRadioButton jrPesCaminhonetePassageiro;
    private javax.swing.JRadioButton jrPesCarroPequeno;
    private javax.swing.JRadioButton jrPesqCaminhoneteCarga;
    private javax.swing.JPanel pAutomovelPequeno;
    private javax.swing.JPanel pCaminhonetCarga;
    private javax.swing.JPanel pCaminhonetePassageiro;
    private javax.swing.JTable tbListaCamiPassageiro;
    private javax.swing.JTable tbListarAutomovelPequeno;
    private javax.swing.JTable tbListarCamiCarga;
    private javax.swing.JTextField txtAnoFab;
    private javax.swing.JTextField txtAnoModelo;
    private javax.swing.JTextField txtCapacidadeCarga;
    private javax.swing.JTextField txtDesempenho;
    private javax.swing.JTextField txtDistanciaEixo;
    private javax.swing.JTextField txtKmAtual;
    private javax.swing.JTextField txtNChassi;
    private javax.swing.JTextField txtNMotor;
    private javax.swing.JTextField txtNOcupantes;
    private javax.swing.JTextField txtNPortas;
    private javax.swing.JFormattedTextField txtPlaca;
    private javax.swing.JTextField txtPotencia;
    private javax.swing.JTextField txtTamanhoAutomovelPequeno;
    private javax.swing.JTextField txtTorque;
    private javax.swing.JTextField txtVolCombustivel;
    // End of variables declaration//GEN-END:variables
}
