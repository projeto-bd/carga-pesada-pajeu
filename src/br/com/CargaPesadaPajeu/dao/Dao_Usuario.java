/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import br.com.CargaPesadaPajeu.modeller.Usuarios;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author root
 */
public class Dao_Usuario {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;

    public boolean adicionarUsuario(Usuarios usuario) {
        boolean confirmacao = false;
        String sql = "INSERT INTO usuario (nome,email,cpf,senha,login) "
                + "VALUES (?,?,?,?,?)";

        try {
            conn = new ConexaoBD().getConexao();
            pstm = conn.prepareStatement(sql);

            pstm.setString(1, usuario.getNome());
            pstm.setString(2, usuario.getEmail());
            pstm.setString(3, usuario.getCpf());
            pstm.setString(4, usuario.getSenha());
            pstm.setString(5, usuario.getLongin());
            pstm.execute();
            confirmacao = true;

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            confirmacao = false;
        } finally {

            //Fecha as conexões
            try {
                if (pstm != null) {

                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("Erro ao fechar conexão");
            }
        }
        return confirmacao;
    }

    public ResultSet listarUsuarios(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("")) {
            sql = "SELECT nome,email,cpf,login from usuario ORDER BY nome";

        } else {
            sql = "SELECT nome,email,cpf,login from usuario WHERE cpf='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro do capiroto!");
        }
        return rs;
    }

    public boolean deletarUsuario(String deletar, int resposta) {
        boolean confirmacao = false;
        String sql = "DELETE FROM usuario WHERE cpf='" + deletar + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();
                pstm = conn.prepareStatement(sql);
                pstm.executeUpdate();
                confirmacao = true;
            } catch (Exception e) {
                confirmacao = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {
                    System.err.println("Erro Malasombrado");
                }
            }

        }
        return confirmacao;
    }

    public boolean alterarUsuario(String nome, String email, String cpf, String login, String senha, String cpfNovo) {
        boolean confirmacao = false;
        try {
            conn = new ConexaoBD().getConexao();
            String sql = null;
            if (senha.isEmpty()) {
                sql = "UPDATE usuario SET nome='" + nome
                        + "',email='" + email
                        + "',cpf='" + cpfNovo
                        + "',login='" + login
                        + "'WHERE cpf='" + cpf + "'";

            } else {
                sql = "UPDATE usuario SET nome='" + nome
                        + "',email='" + email
                        + "',cpf='" + cpfNovo
                        + "',senha='" + senha
                        + "',login='" + login
                        + "'WHERE cpf='" + cpf + "'";

            }

            pstm = conn.prepareStatement(sql);
            pstm.executeUpdate();
            confirmacao = true;

        } catch (Exception e) {
            confirmacao = false;
        } finally {
            //Fecha as conexões
            try {
                if (pstm != null) {
                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("Ops! erro ao fechar as conexões!");
            }
        }
        return confirmacao;

    }

    public boolean verificaUsuario(String login, String senha) {
        boolean confirmacao = false;
        conn = new ConexaoBD().getConexao();
        String sql = "SELECT * FROM usuario";
        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {

                if (login.equals(rs.getString("login")) && senha.equals(rs.getString("senha"))) {

                    confirmacao = true;

                    break;
                } else {

                    confirmacao = false;
                }

            }

        } catch (SQLException ex) {
            System.err.println("Algo deu errado:" + ex.getMessage());
        } finally {
            //Fecha as conexões
            try {
                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("Ops! erro ao fechar as conexões!");
            }
            return confirmacao;

        }

    }
}
