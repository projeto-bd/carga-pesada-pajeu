/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author root
 */
public class Dao_Devolucao {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;

    public int quantidadeRetornada(String consulta){
          int resultado = 0;
        conn = new ConexaoBD().getConexao();

        String achoSql = "";

        achoSql = "SELECT * FROM v_LocacaoReserva WHERE id_locacao='" + consulta + "' AND status !='" + "Finalizada" + "'";

        try {

            st = conn.createStatement();
            rs = st.executeQuery(achoSql);
            while (rs.next()) {
                resultado = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }

        return resultado;
    
    }
    public ResultSet preencherCamposReserva(String consulta) {
        conn = new ConexaoBD().getConexao();
        int qtRetorno;

        String sql = "";

        sql = "SELECT * FROM v_LocacaoReserva WHERE id_locacao='" + consulta + "' AND status !='" + "Finalizada" + "'";
        qtRetorno = quantidadeRetornada(consulta);
        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro:" + ex.getMessage());
        }
        if(qtRetorno !=0){
            return rs;
        }else{
            return null;
        }
        
    }

    public boolean finalizarLocacao(String status, int idLocacao, String placa, int kmAtual, float valorTotal) {
        boolean confirmacao = false;
        try {
            conn = new ConexaoBD().getConexao();
            String sqlLocacao = "UPDATE v_LocacaoReserva SET status='" + status
                    + "'WHERE id_locacao='" + idLocacao + "'";
            pstm = conn.prepareStatement(sqlLocacao);
            pstm.executeUpdate();
            if (alteraStatusVeiculoPDispinvel(placa) == true && atualizarKmAtual(placa, kmAtual) && atualizarValorTotalFinal(valorTotal, idLocacao)) {
                pstm.executeUpdate();
                confirmacao = true;

            } else {
                confirmacao = false;
            }

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            confirmacao = false;
        } finally {
            //Fecha as conexões
            try {

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("Erro ao fechar as conexões");
            }
        }
        return confirmacao;

    }

    public boolean atualizarKmAtual(String placa, int kmAtual) {
        boolean confirmacao = false;
        try {
            conn = new ConexaoBD().getConexao();
            String sqlAtualizaKm = "UPDATE veiculo SET quilometragem_atual='" + kmAtual
                    + "'WHERE placa='" + placa + "'";
            pstm = conn.prepareStatement(sqlAtualizaKm);
            pstm.executeUpdate();

            pstm.executeUpdate();
            confirmacao = true;

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            confirmacao = false;
        }

        return confirmacao;
    }

    public boolean atualizarValorTotalFinal(float valorTotal, int id_reserva) {
        boolean confirmacao = false;
        try {
            conn = new ConexaoBD().getConexao();
            String sqlAtualizaValor = "UPDATE v_LocacaoReserva SET valor_reserva='" + valorTotal
                    + "'WHERE id_locacao='" + id_reserva + "'";
            pstm = conn.prepareStatement(sqlAtualizaValor);
            pstm.executeUpdate();

            pstm.executeUpdate();
            confirmacao = true;

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            confirmacao = false;
        }
        return confirmacao;
    }

    public boolean alteraStatusVeiculoPDispinvel(String placa) {
        conn = new ConexaoBD().getConexao();
        boolean confirmarDeletarReserva = false;
        try {
            CallableStatement cstmt = conn.prepareCall("{call alterarVeiculoPDisponivel(?)}");
            cstmt.setString(1, placa);
            cstmt.execute();
            confirmarDeletarReserva = true;
        } catch (Exception e) {
            confirmarDeletarReserva = false;
        }
        return confirmarDeletarReserva;
    }

}
