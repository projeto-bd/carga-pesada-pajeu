/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import br.com.CargaPesadaPajeu.modeller.Reserva;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author root
 */
public class Dao_Reserva {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;

    public ResultSet verificaClienteFisico(String cpf) {

        conn = new ConexaoBD().getConexao();

        String sql = "";

        sql = "SELECT * FROM v_clienteFisico WHERE cpf='" + cpf + "'";
        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("erro de conexão!" + ex.getErrorCode());
        }

        return rs;
    }
    public int retormarMaxReserva(){
        int resultado = 0;
        conn = new ConexaoBD().getConexao();
        try {
            st = conn.createStatement();;

            String sqlMaxId = "SELECT MAX(id_reserva) FROM reserva";
            rs = st.executeQuery(sqlMaxId);
            while (rs.next()) {
                resultado = rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return resultado;
    }

    public ResultSet verificaClienteJuridico(String cnpj) {

        conn = new ConexaoBD().getConexao();

        String sql = "";

        sql = "SELECT * FROM v_clienteJuridico WHERE cnpj='" + cnpj + "'";
        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("erro de conexão!" + ex.getErrorCode());
        }

        return rs;
    }

    public ResultSet carregarValorCategoria(String nome) {

        conn = new ConexaoBD().getConexao();

        String sql = "";

        sql = "SELECT * FROM categoria_veiculo WHERE nome='" + nome + "'";
        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("erro de conexão!" + ex.getErrorCode());
        }

        return rs;
    }

    public boolean efetuarReserva(Reserva reserva) {
        boolean resultado = false;

        String sql = "INSERT INTO reserva (data_retirada,status,data_entrega,id_cliente,"
                + "nome_categoria_veiculo,nome_locadora,tipo_cliente,valor_reserva,tipo_locacao) "
                + "VALUES (?,?,?,?,?,?,?,?,?)";

        try {
            conn = new ConexaoBD().getConexao();
            pstm = conn.prepareStatement(sql);

            pstm.setString(1, reserva.getDataRetirada());
            pstm.setString(2, reserva.getStatus());
            pstm.setString(3, reserva.getDataEntrega());
            pstm.setInt(4, reserva.getIdCliente());
            pstm.setString(5, reserva.getNome_categoria_veiculo());
            pstm.setString(6, reserva.getNomeLocadora());
            pstm.setString(7, reserva.getTipo_cliente());
            pstm.setFloat(8, reserva.getValor_reserva());
            pstm.setString(9, reserva.getTipo_locacao());
            pstm.execute();
            resultado = true;

        } catch (Exception e) {
            resultado = false;
            System.err.println("erro:" + e.getMessage());

        } finally {

            //Fecha as conexões
            try {
                if (pstm != null) {

                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {

                System.err.println("Erro ao fechar conexões!");
            }
        }
        return resultado;
    }

    public ResultSet listarReservas(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("") || consulta.isEmpty()) {
            sql = "SELECT * FROM v_reserva";

        } else {
            sql = "SELECT * FROM v_reserva WHERE id_reserva='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }
        return rs;
    }

    public boolean deletarReserva(String deletar, int resposta) {
        boolean confirmacao = false;
        String sql = "DELETE FROM reserva WHERE id_reserva='" + deletar + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();

                pstm = conn.prepareStatement(sql);
                pstm.executeUpdate();
                confirmacao = true;
            } catch (SQLException e) {
                System.err.println("Erro:"+e.getErrorCode());
                confirmacao = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {
                    System.err.println("Erro ao fechar conexão");
                }
            }

        }

        return confirmacao;

    }

    public boolean alterarReserva(String dataRetirada, String status, String dataEntrega,
            String nome_categoria_veiculo, String nomeLocadora, float valor, String idReserva,String tipo_locacao) {
        boolean resultado = false;
        try {
            conn = new ConexaoBD().getConexao();
            String sqlLocadora = "UPDATE reserva SET data_retirada='" + dataRetirada
                    + "',status='" + status
                    + "',data_entrega='" + dataEntrega
                    + "',nome_categoria_veiculo='" + nome_categoria_veiculo
                    + "',nome_locadora='" + nomeLocadora
                    + "',valor_reserva='" + valor
                    + "',tipo_locacao='" + tipo_locacao
                    + "'WHERE id_reserva='" + idReserva + "'";
            pstm = conn.prepareStatement(sqlLocadora);
            pstm.executeUpdate();

            resultado = true;

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            resultado = false;
        } finally {
            //Fecha as conexões
            try {
                if (pstm != null) {
                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("Erro ao fechar as conexões");
            }
        }
        return resultado;

    }

}
