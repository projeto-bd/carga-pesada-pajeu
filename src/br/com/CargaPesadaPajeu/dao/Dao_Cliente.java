/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import br.com.CargaPesadaPajeu.modeller.ClientePessoaFisica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//melhorar!
/**
 *
 * @author root
 */
public class Dao_Cliente {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;

    public boolean verificaCpf(String cpf) {
        boolean resultado = false;

        String sql = "SELECT cpf FROM cliente_fisico";
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {

                if (cpf.equals(rs.getString("cpf"))) {

                    resultado = true;

                    break;
                } else {

                    resultado = false;
                }

            }

        } catch (SQLException ex) {
            System.err.println("Erro desconhecido:" + ex.getMessage());
        }

        return resultado;

    }

    public int retornaProximoId() {
        int resultado = 0;
        try {
            Statement st2 = conn.createStatement();;
            ResultSet rs2;

            String sqlMaxId = "SELECT MAX(id_cliente) FROM cliente";
            rs2 = st2.executeQuery(sqlMaxId);
            while (rs2.next()) {
                resultado = rs2.getInt(1);
            }
        } catch (Exception e) {
        }

        return resultado;
    }

    public boolean adicionarClienteFisico(ClientePessoaFisica cliente) {
        boolean confirmacao = false;
        conn = new ConexaoBD().getConexao();
        if (verificaCpf(cliente.getCpf()) == false) {
            try {
                String sqlCliente = "INSERT INTO cliente (nome) "
                        + "VALUES (?)";

                pstm = conn.prepareStatement(sqlCliente);

                pstm.setString(1, cliente.getNome());

                pstm.execute();
                confirmacao = true;

            } catch (Exception e) {
                System.err.println("Erro ao adicionar dados!");
                confirmacao = false;
            }

            try {

                int resultado = retornaProximoId();
                String sqlEndereco = "INSERT INTO endereco_cliente (id_cliente,rua,bairro,cidade,cep,estado) "
                        + "VALUES (?,?,?,?,?,?)";

                pstm = conn.prepareStatement(sqlEndereco);
                pstm.setInt(1, resultado);
                pstm.setString(2, cliente.getNomeRua());
                pstm.setString(3, cliente.getBairro());
                pstm.setString(4, cliente.getCidade());
                pstm.setString(5, cliente.getCep());
                pstm.setString(6, cliente.getEstado());
                pstm.execute();
                confirmacao = true;

            } catch (Exception e) {
                System.err.println("erro ao adicionar dados:" + e.getMessage());
                confirmacao = false;
            }
            try {
                String sqlClienteFisico = "INSERT INTO cliente_fisico (cpf,sexo,data_nascimento,id_cliente,num_habilitacao,vencimento_habilitacao) "
                        + "VALUES (?,?,?,?,?,?)";

                int resultado = retornaProximoId();
                pstm = conn.prepareStatement(sqlClienteFisico);
                pstm.setString(1, cliente.getCpf());
                pstm.setString(2, cliente.getSexo());
                pstm.setString(3, cliente.getDataNascimento());
                pstm.setInt(4, resultado);
                pstm.setString(5, cliente.getNumeroHabilitacao());
                pstm.setString(6, cliente.getVencimentoHabilitacao());

                pstm.execute();
                confirmacao = true;

            } catch (Exception e) {
                System.err.println("erro ao adicionar dados:" + e.getMessage());
                confirmacao = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {
                    System.err.println("erro ao fechar conexões:" + e.getMessage());
                }
            }
        } else {
            confirmacao = false;

        }
        return confirmacao;
    }

}
