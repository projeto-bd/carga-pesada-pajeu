/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import br.com.CargaPesadaPajeu.modeller.CategoriaVeiculo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Dao_CategoriaVeiculo {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;

    public int adicionarCategoria(CategoriaVeiculo categoriaVeiculo) {
        String sqlLocadora = "INSERT INTO categoria_veiculo (nome,valor) "
                + "VALUES (?,?)";
        try {
            conn = new ConexaoBD().getConexao();
            pstm = conn.prepareStatement(sqlLocadora);
            pstm.setString(1, categoriaVeiculo.getNome());
            pstm.setFloat(2, categoriaVeiculo.getValor());
            pstm.execute();
            return 1;

        } catch (Exception e) {
            return 0;
        } finally {

            //Fecha as conexões
            try {
                if (pstm != null) {

                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {

                JOptionPane.showMessageDialog(null, "Ops! algo deu errado!");
            }
        }
    }

    public ResultSet listarCategorias(String consulta) {

        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("") || consulta.isEmpty()) {
            sql = "SELECT * FROM categoria_veiculo";

        } else {
            sql = "SELECT * FROM categoria_veiculo WHERE nome='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Carregar Informações:" + ex.getMessage());
        }

        return rs;

    }

    public int exlcuirCategoria(String deletar, int resposta) {
        
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();
                String sql = "DELETE FROM categoria_veiculo WHERE id_categoria_veiculo='" + deletar + "'";
                pstm = conn.prepareStatement(sql);
                pstm.executeUpdate();

                return 1;
            } catch (Exception e) {
                return 0;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    JOptionPane.showMessageDialog(null, "Ops! algo deu errado!");
                }
            }

        }

        return 0;

    }

}
