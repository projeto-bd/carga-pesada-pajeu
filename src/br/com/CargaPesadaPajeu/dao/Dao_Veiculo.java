/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import br.com.CargaPesadaPajeu.modeller.VeiculoAutomovelPequeno;
import br.com.CargaPesadaPajeu.modeller.VeiculoCaminhoneteCarga;
import br.com.CargaPesadaPajeu.modeller.VeiculoCaminhonetePassageiro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author root
 */
public class Dao_Veiculo {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;

    //Carregar combobox
    public ResultSet carregarCategoria() {
        conn = new ConexaoBD().getConexao();
        String sqlMotorista = "SELECT nome FROM categoria_veiculo";
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sqlMotorista);
            return rs;
        } catch (Exception ex) {
            System.err.println("Erro:" + ex.getMessage());
            return null;
        }
    }

    //Carregar combobox
    public ResultSet CarregarLocadora() {
        conn = new ConexaoBD().getConexao();
        String sqlMotorista = "SELECT nome FROM locadora";
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery(sqlMotorista);
            return rs;
        } catch (Exception ex) {
            System.err.println("Erro:" + ex.getMessage());
            return null;
        }

    }

    public boolean verificaPlaca(String placa) {
        boolean resultado = false;
        conn = new ConexaoBD().getConexao();

        String sql = "SELECT placa FROM veiculo";
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {

                if (placa.equals(rs.getString("placa"))) {
                    resultado = true;

                    break;
                } else {

                    resultado = false;
                }

            }

        } catch (SQLException ex) {
            System.err.println("Erro ao buscar dados");
        }

        return resultado;
    }

    public int retornaProximoId() {
        int resultado = 0;
        try {
            st = conn.createStatement();;

            String sqlMaxId = "SELECT MAX(id_veiculo) FROM veiculo";
            rs = st.executeQuery(sqlMaxId);
            while (rs.next()) {
                resultado = rs.getInt(1);
            }
        } catch (Exception e) {
            System.err.println("erro ao buscar informações!");
        }
        return resultado;
    }

   //--------------------------AUTOMOVEL PEQUENO --------------------------------------\\
    public boolean adicionarVeiculoPequeno(VeiculoAutomovelPequeno veiuculoPequeno) {
        boolean confirmacao;
        conn = new ConexaoBD().getConexao();
        if (verificaPlaca(veiuculoPequeno.getPlaca()) == false) {

            try {
                String sqlCarroPequeno = "INSERT INTO veiculo (nome_locadora,ano_modelo,num_portas,num_chassi,"
                        + "torque,num_motor,placa,nome_categoria_veiculo,ano_fabricacao,"
                        + "quilometragem_atual,cor,tipo_combustivel,num_passageiros,revisoes,status,limpeza) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                pstm = conn.prepareStatement(sqlCarroPequeno);
                pstm.setString(1, veiuculoPequeno.getResponsavel());
                pstm.setInt(2, veiuculoPequeno.getAnoModelo());
                pstm.setInt(3, veiuculoPequeno.getNumeroPortas());
                pstm.setInt(4, veiuculoPequeno.getNumeroChassi());
                pstm.setInt(5, veiuculoPequeno.getTork());
                pstm.setString(6, veiuculoPequeno.getNumeroMotor());
                pstm.setString(7, veiuculoPequeno.getPlaca());
                pstm.setString(8, veiuculoPequeno.getCategoria());
                pstm.setInt(9, veiuculoPequeno.getAnoFabricacao());
                pstm.setInt(10, veiuculoPequeno.getKmAtual());
                pstm.setString(11, veiuculoPequeno.getCor());
                pstm.setString(12, veiuculoPequeno.getTipoCombustivel());
                pstm.setInt(13, veiuculoPequeno.getNumeroOcupantes());
                pstm.setString(14, veiuculoPequeno.getRevisoes());
                pstm.setString(15, veiuculoPequeno.getStatus());
                pstm.setString(16, veiuculoPequeno.getLimpeza());

                pstm.execute();

                //Parte 2
                int resultado = retornaProximoId();
                String sqlVeiculoPequeno = "INSERT INTO automovel_pequeno (id_veiculo,radio,mp3,dvd,tamanho,camera_re,"
                        + "direcao_hidraulica,tipo_cambio,ar_condicionado) "
                        + "VALUES (?,?,?,?,?,?,?,?,?)";
                // PreparedStatement pstm2;
                pstm = conn.prepareStatement(sqlVeiculoPequeno);
                pstm.setInt(1, resultado);
                pstm.setBoolean(2, veiuculoPequeno.isRadio());
                pstm.setBoolean(3, veiuculoPequeno.isMp3());
                pstm.setBoolean(4, veiuculoPequeno.isDvd());
                pstm.setFloat(5, veiuculoPequeno.getTamanho());
                pstm.setBoolean(6, veiuculoPequeno.isCameraRe());
                pstm.setBoolean(7, veiuculoPequeno.isDirecaoHidraulica());
                pstm.setString(8, veiuculoPequeno.getTipoCambio());
                pstm.setBoolean(9, veiuculoPequeno.isArCondicionado());

                pstm.execute();

                confirmacao = true;
            } catch (Exception e) {
                System.err.println("erro:" + e.getMessage());
                confirmacao = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    System.err.println("Erro ao fechar conexões");
                }
            }
        } else {
            confirmacao = false;
        }
        return confirmacao;
    }
    
    public ResultSet listarAutomovelPequeno(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("") || consulta.isEmpty()) {
            sql = "SELECT * FROM v_veiculoPequeno";

        } else {
            sql = "SELECT * FROM v_veiculoPequeno WHERE placa='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            return null;
        }

        return rs;
    }

    public boolean deletarAutomovelPequeno(String deletar, int resposta) {
        boolean resultado = false;
        String sqlAutPequeno = "DELETE FROM automovel_pequeno WHERE id_veiculo='" + deletar + "'";
        String sqlVeiculo = "DELETE FROM veiculo WHERE id_veiculo='" + deletar + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();

                pstm = conn.prepareStatement(sqlAutPequeno);
                pstm.executeUpdate();

                pstm = conn.prepareStatement(sqlVeiculo);
                pstm.executeUpdate();
                resultado = true;
            } catch (Exception e) {

                resultado = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    System.err.println("Erro ao fechar conexões");
                }
            }

        }

        return resultado;
    }
    
      public boolean alterarAutoPequeno(boolean radio, boolean mp3, float tamanho, boolean cameraRe,
            boolean direcaoHidraulica, String tipoCambio, boolean arCondicionado, boolean dvd,
            String placa, int numeroChassi, String numeroMotor, String tipoCombustivel, int tork,
            String cor, int kmAtual, int anoModelo, int anoFabricacao, int numeroPortas, String categoria,
            String responsavel, int numeroOcupantes, String revisoes, String limpeza, String status, String idVeiculo) {
        boolean resposta;
        int radioParaSql;
        int mp3ParaSql;
        int cameraReParaSql;
        int direcaoHidraulicaParaSql;
        int arCondicionadoParaSql;
        int dvdParaSql;
        if (radio == true) {
            radioParaSql = 1;
        } else {
            radioParaSql = 0;
        }
        if (mp3 == true) {
            mp3ParaSql = 1;
        } else {
            mp3ParaSql = 0;
        }
        if (cameraRe == true) {
            cameraReParaSql = 1;
        } else {
            cameraReParaSql = 0;
        }
        if (direcaoHidraulica == true) {
            direcaoHidraulicaParaSql = 1;
        } else {
            direcaoHidraulicaParaSql = 0;
        }
        if (arCondicionado == true) {
            arCondicionadoParaSql = 1;
        } else {
            arCondicionadoParaSql = 0;
        }
        if (dvd == true) {
            dvdParaSql = 1;
        } else {
            dvdParaSql = 0;
        }
        try {
            conn = new ConexaoBD().getConexao();
            String sqlAutoPequeno = "UPDATE automovel_pequeno SET radio='" + radioParaSql
                    + "',mp3='" + mp3ParaSql
                    + "',dvd='" + dvdParaSql
                    + "',tamanho='" + tamanho
                    + "',camera_re='" + cameraReParaSql
                    + "',direcao_hidraulica='" + direcaoHidraulicaParaSql
                    + "',tipo_cambio='" + tipoCambio
                    + "',ar_condicionado='" + arCondicionadoParaSql
                    + "'WHERE id_veiculo='" + idVeiculo + "'";

            pstm = conn.prepareStatement(sqlAutoPequeno);
            pstm.executeUpdate();

            String sqlVeiculo = "UPDATE veiculo SET nome_locadora='" + responsavel
                    + "',ano_modelo='" + anoModelo
                    + "',num_portas='" + numeroPortas
                    + "',num_chassi='" + numeroChassi
                    + "',torque='" + tork
                    + "',num_motor='" + numeroMotor
                    + "',placa='" + placa
                    + "',nome_categoria_veiculo='" + categoria
                    + "',ano_fabricacao='" + anoFabricacao
                    + "',quilometragem_atual='" + kmAtual
                    + "',cor='" + cor
                    + "',tipo_combustivel='" + tipoCombustivel
                    + "',num_passageiros='" + numeroOcupantes
                    + "',revisoes='" + revisoes
                    + "',status='" + status
                    + "',limpeza='" + revisoes
                    + "'WHERE id_veiculo='" + idVeiculo + "'";

            pstm = conn.prepareStatement(sqlVeiculo);
            pstm.executeUpdate();
            resposta = true;

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            resposta = false;
        } finally {
            //Fecha as conexões
            try {
                if (pstm != null) {
                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("erro ao fechar conexões");
            }
        }
        return resposta;
    }
    //------------------------FIM AUTOMOVEL PEQUENO --------------------------------------\\  
    
      //------------------------ CAMINHONETE CARGA --------------------------------------\\
    public boolean adicionarCaminhonheteCarga(VeiculoCaminhoneteCarga caminhoneteCarga) {
        boolean confirmacao;
        conn = new ConexaoBD().getConexao();
        if (verificaPlaca(caminhoneteCarga.getPlaca()) == false) {

            try {
                String sqlVeiculoCarga = "INSERT INTO veiculo (nome_locadora,ano_modelo,num_portas,num_chassi,"
                        + "torque,num_motor,placa,nome_categoria_veiculo,ano_fabricacao,"
                        + "quilometragem_atual,cor,tipo_combustivel,num_passageiros,revisoes,status,limpeza) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                pstm = conn.prepareStatement(sqlVeiculoCarga);
                pstm.setString(1, caminhoneteCarga.getResponsavel());
                pstm.setInt(2, caminhoneteCarga.getAnoModelo());
                pstm.setInt(3, caminhoneteCarga.getNumeroPortas());
                pstm.setInt(4, caminhoneteCarga.getNumeroChassi());
                pstm.setInt(5, caminhoneteCarga.getTork());
                pstm.setString(6, caminhoneteCarga.getNumeroMotor());
                pstm.setString(7, caminhoneteCarga.getPlaca());
                pstm.setString(8, caminhoneteCarga.getCategoria());
                pstm.setInt(9, caminhoneteCarga.getAnoFabricacao());
                pstm.setInt(10, caminhoneteCarga.getKmAtual());
                pstm.setString(11, caminhoneteCarga.getCor());
                pstm.setString(12, caminhoneteCarga.getTipoCombustivel());
                pstm.setInt(13, caminhoneteCarga.getNumeroOcupantes());
                pstm.setString(14, caminhoneteCarga.getRevisoes());
                pstm.setString(15, caminhoneteCarga.getStatus());
                pstm.setString(16, caminhoneteCarga.getLimpeza());

                pstm.execute();

                //Parte 2
                int resultado = retornaProximoId();
                String sqlCaminhoneteCarga = "INSERT INTO caminhonete_carga(id_veiculo,capacidade_carga,"
                        + "vol_vombustivel,desempenho,distancia_eixo,potencia,embreagem)"
                        + "VALUES (?,?,?,?,?,?,?)";
                // PreparedStatement pstm2;
                pstm = conn.prepareStatement(sqlCaminhoneteCarga);
                pstm.setInt(1, resultado);
                pstm.setFloat(2, caminhoneteCarga.getCapacidadeCarga());
                pstm.setFloat(3, caminhoneteCarga.getVolumeCombustivel());
                pstm.setFloat(4, caminhoneteCarga.getDesempenho());
                pstm.setFloat(5, caminhoneteCarga.getDistanciaEixos());
                pstm.setFloat(6, caminhoneteCarga.getPotencia());
                pstm.setString(7, caminhoneteCarga.getEmbreagem());

                pstm.execute();

                confirmacao = true;
            } catch (Exception e) {
                System.err.println("erro:" + e.getMessage());
                confirmacao = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    System.err.println("Erro ao fechar conexões");
                }
            }
        } else {
            confirmacao = false;
        }
        return confirmacao;
    }
    
        public ResultSet listarCaminhoneteCarga(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("") || consulta.isEmpty()) {
            sql = "SELECT * FROM v_caminhoneteCarga";

        } else {
            sql = "SELECT * FROM v_caminhoneteCarga WHERE placa='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            return null;
        }

        return rs;
    }
      
        public boolean deletarCamCarga(String deletar, int resposta) {
        boolean resultado = false;
        String sqlAutPequeno = "DELETE FROM caminhonete_carga WHERE id_veiculo='" + deletar + "'";
        String sqlVeiculo = "DELETE FROM veiculo WHERE id_veiculo='" + deletar + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();

                pstm = conn.prepareStatement(sqlAutPequeno);
                pstm.executeUpdate();

                pstm = conn.prepareStatement(sqlVeiculo);
                pstm.executeUpdate();
                resultado = true;
            } catch (Exception e) {

                resultado = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    System.err.println("Erro ao fechar conexões");
                }
            }

        }

        return resultado;
    }
    
        public boolean alterarCamCarga(float capacidadeCarga, float volumeCombustivel, float desempenho,
            float distanciaEixos, float potencia, String embreagem, String placa, int numeroChassi,
            String numeroMotor, String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo,
            int anoFabricacao, int numeroPortas, String categoria, String responsavel, int numeroOcupantes,
            String revisoes, String limpeza, String status, String idVeiculo) {
        boolean resposta;
        try {
            conn = new ConexaoBD().getConexao();
            String sqlCamCarga = "UPDATE caminhonete_carga SET capacidade_carga='" + capacidadeCarga
                    + "',vol_vombustivel='" + volumeCombustivel
                    + "',desempenho='" + desempenho
                    + "',distancia_eixo='" + distanciaEixos
                    + "',potencia='" + potencia
                    + "',embreagem='" + embreagem
                    + "'WHERE id_veiculo='" + idVeiculo + "'";

            pstm = conn.prepareStatement(sqlCamCarga);
            pstm.executeUpdate();

            String sqlVeiculo = "UPDATE veiculo SET nome_locadora='" + responsavel
                    + "',ano_modelo='" + anoModelo
                    + "',num_portas='" + numeroPortas
                    + "',num_chassi='" + numeroChassi
                    + "',torque='" + tork
                    + "',num_motor='" + numeroMotor
                    + "',placa='" + placa
                    + "',nome_categoria_veiculo='" + categoria
                    + "',ano_fabricacao='" + anoFabricacao
                    + "',quilometragem_atual='" + kmAtual
                    + "',cor='" + cor
                    + "',tipo_combustivel='" + tipoCombustivel
                    + "',num_passageiros='" + numeroOcupantes
                    + "',revisoes='" + revisoes
                    + "',status='" + status
                    + "',limpeza='" + revisoes
                    + "'WHERE id_veiculo='" + idVeiculo + "'";

            pstm = conn.prepareStatement(sqlVeiculo);
            pstm.executeUpdate();
            resposta = true;

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            resposta = false;
        } finally {
            //Fecha as conexões
            try {
                if (pstm != null) {
                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("erro ao fechar conexões");
            }
        }
        return resposta;

    }
        
    
    //------------------------ FIM CAMINHONETE CARGA --------------------------------------\\

    //------------------------CAMINHONETE PASSAGEIRO --------------------------------------\\
    public boolean adicionarCaminhonetePassageiro(VeiculoCaminhonetePassageiro caminhonetePassageiro) {
        boolean confirmacao;
        conn = new ConexaoBD().getConexao();
        if (verificaPlaca(caminhonetePassageiro.getPlaca()) == false) {

            try {
                String sqlVeiculoPassageiro = "INSERT INTO veiculo (nome_locadora,ano_modelo,num_portas,num_chassi,"
                        + "torque,num_motor,placa,nome_categoria_veiculo,ano_fabricacao,"
                        + "quilometragem_atual,cor,tipo_combustivel,num_passageiros,revisoes,status,limpeza) "
                        + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                pstm = conn.prepareStatement(sqlVeiculoPassageiro);
                pstm.setString(1, caminhonetePassageiro.getResponsavel());
                pstm.setInt(2, caminhonetePassageiro.getAnoModelo());
                pstm.setInt(3, caminhonetePassageiro.getNumeroPortas());
                pstm.setInt(4, caminhonetePassageiro.getNumeroChassi());
                pstm.setInt(5, caminhonetePassageiro.getTork());
                pstm.setString(6, caminhonetePassageiro.getNumeroMotor());
                pstm.setString(7, caminhonetePassageiro.getPlaca());
                pstm.setString(8, caminhonetePassageiro.getCategoria());
                pstm.setInt(9, caminhonetePassageiro.getAnoFabricacao());
                pstm.setInt(10, caminhonetePassageiro.getKmAtual());
                pstm.setString(11, caminhonetePassageiro.getCor());
                pstm.setString(12, caminhonetePassageiro.getTipoCombustivel());
                pstm.setInt(13, caminhonetePassageiro.getNumeroOcupantes());
                pstm.setString(14, caminhonetePassageiro.getRevisoes());
                pstm.setString(15, caminhonetePassageiro.getStatus());
                pstm.setString(16, caminhonetePassageiro.getLimpeza());

                pstm.execute();

                //Parte 2
                int resultado = retornaProximoId();
                String sqlCaminhonetePassageiro = "INSERT INTO caminhonete_passageiros(id_veiculo,air_bag,"
                        + "contr_poluicao_ar,cinto_seg_traseiro_retrateis,direcao_assistida,rodas_liga_leve)"
                        + "VALUES (?,?,?,?,?,?)";

                pstm = conn.prepareStatement(sqlCaminhonetePassageiro);
                pstm.setInt(1, resultado);
                pstm.setBoolean(2, caminhonetePassageiro.isAirbag());
                pstm.setBoolean(3, caminhonetePassageiro.isControlePoulicao());
                pstm.setBoolean(4, caminhonetePassageiro.isCintoSeguranca());
                pstm.setBoolean(5, caminhonetePassageiro.isDirecaoAssistida());
                pstm.setBoolean(6, caminhonetePassageiro.isRodasLigaLeve());

                pstm.execute();

                confirmacao = true;
            } catch (Exception e) {
                System.err.println("erro:" + e.getMessage());
                confirmacao = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    System.err.println("Erro ao fechar conexões");
                }
            }
        } else {
            confirmacao = false;
        }
        return confirmacao;

    }

    public ResultSet listarCaminhonetePassageiro(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("") || consulta.isEmpty()) {
            sql = "SELECT * FROM v_caminhonetePassageiro";

        } else {
            sql = "SELECT * FROM v_caminhonetePassageiro WHERE placa='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            return null;
        }

        return rs;
    }


    public boolean deletarCamPassageiro(String deletar, int resposta) {
        boolean resultado = false;
        String sqlAutPequeno = "DELETE FROM caminhonete_passageiros WHERE id_veiculo='" + deletar + "'";
        String sqlVeiculo = "DELETE FROM veiculo WHERE id_veiculo='" + deletar + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();

                pstm = conn.prepareStatement(sqlAutPequeno);
                pstm.executeUpdate();

                pstm = conn.prepareStatement(sqlVeiculo);
                pstm.executeUpdate();
                resultado = true;
            } catch (Exception e) {

                resultado = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    System.err.println("Erro ao fechar conexões");
                }
            }

        }

        return resultado;
    }
    
    public boolean alterarCamPassageiro(boolean cintoSeguranca, boolean airbag, boolean rodasLigaLeve,
            boolean direcaoAssistida, boolean controlePoulicao, String placa, int numeroChassi, String numeroMotor,
            String tipoCombustivel, int tork, String cor, int kmAtual, int anoModelo, int anoFabricacao,
            int numeroPortas, String categoria, String responsavel, int numeroOcupantes, String revisoes,
            String limpeza, String status,String idVeiculo){
    
        boolean resposta;
        int cintoSegurancaSql;
        int airBagSql;
        int rodasLigaLeveSql;
        int controlePoluicaoSql;
        int direcaoAssistidaSql;
       
        if (cintoSeguranca == true) {
            cintoSegurancaSql = 1;
        } else {
            cintoSegurancaSql = 0;
        }
        if (airbag == true) {
            airBagSql = 1;
        } else {
            airBagSql = 0;
        }
        if (rodasLigaLeve == true) {
            rodasLigaLeveSql = 1;
        } else {
            rodasLigaLeveSql = 0;
        }
        if (controlePoulicao == true) {
            controlePoluicaoSql = 1;
        } else {
            controlePoluicaoSql = 0;
        }
        if (direcaoAssistida == true) {
            direcaoAssistidaSql = 1;
        } else {
            direcaoAssistidaSql = 0;
        }
       
        try {
            conn = new ConexaoBD().getConexao();
            String sqlCamPassageiro = "UPDATE caminhonete_passageiros SET air_bag='" + airBagSql
                    + "',contr_poluicao_ar='" + controlePoluicaoSql
                    + "',cinto_seg_traseiro_retrateis='" + cintoSegurancaSql
                    + "',direcao_assistida='" + direcaoAssistidaSql
                    + "',rodas_liga_leve='" + rodasLigaLeveSql
                    + "'WHERE id_veiculo='" + idVeiculo + "'";

            pstm = conn.prepareStatement(sqlCamPassageiro);
            pstm.executeUpdate();

            String sqlVeiculo = "UPDATE veiculo SET nome_locadora='" + responsavel
                    + "',ano_modelo='" + anoModelo
                    + "',num_portas='" + numeroPortas
                    + "',num_chassi='" + numeroChassi
                    + "',torque='" + tork
                    + "',num_motor='" + numeroMotor
                    + "',placa='" + placa
                    + "',nome_categoria_veiculo='" + categoria
                    + "',ano_fabricacao='" + anoFabricacao
                    + "',quilometragem_atual='" + kmAtual
                    + "',cor='" + cor
                    + "',tipo_combustivel='" + tipoCombustivel
                    + "',num_passageiros='" + numeroOcupantes
                    + "',revisoes='" + revisoes
                    + "',status='" + status
                    + "',limpeza='" + revisoes
                    + "'WHERE id_veiculo='" + idVeiculo + "'";

            pstm = conn.prepareStatement(sqlVeiculo);
            pstm.executeUpdate();
            resposta = true;

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            resposta = false;
        } finally {
            //Fecha as conexões
            try {
                if (pstm != null) {
                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("erro ao fechar conexões");
            }
        }
        return resposta;
    
    
    }

}
