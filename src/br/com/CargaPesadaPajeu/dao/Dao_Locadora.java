/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import br.com.CargaPesadaPajeu.modeller.Modelo_Locadora;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class Dao_Locadora {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;

    public int retornaProximoId() {
        int resultado = 0;
        try {
            Statement st2 = conn.createStatement();;
            ResultSet rs2;

            String sqlMaxId = "SELECT MAX(id_locadora) FROM locadora";
            rs2 = st2.executeQuery(sqlMaxId);
            while (rs2.next()) {
                resultado = rs2.getInt(1);
            }
        } catch (Exception e) {
        }

        return resultado;
    }

    public boolean adicionarLocadora(Modelo_Locadora locadora) {
        boolean confirmaca = false;
        String sqlLocadora = "INSERT INTO locadora (cnpj,nome) "
                + "VALUES (?,?)";
        try {
            conn = new ConexaoBD().getConexao();
            pstm = conn.prepareStatement(sqlLocadora);
            pstm.setString(1, locadora.getCnpj());
            pstm.setString(2, locadora.getNome());
            pstm.execute();
            confirmaca = true;

        } catch (Exception e) {
            System.err.println("erro ao gravar dados:" + e.getMessage());
            confirmaca = false;
        }
        try {

            int resultado = retornaProximoId();
            String sqlEndereco = "INSERT INTO endereco_locadora (id_locadora,rua,bairro,cidade,cep,estado) "
                    + "VALUES (?,?,?,?,?,?)";

            pstm = conn.prepareStatement(sqlEndereco);
            pstm.setInt(1, resultado);
            pstm.setString(2, locadora.getRua());
            pstm.setString(3, locadora.getBairro());
            pstm.setString(4, locadora.getCidade());
            pstm.setString(5, locadora.getCep());
            pstm.setString(6, locadora.getEstado());
            pstm.execute();
            confirmaca = true;

        } catch (Exception e) {
            System.err.println("erro ao gravar dados:" + e.getMessage());
            confirmaca = false;
        } finally {

            //Fecha as conexões
            try {
                if (pstm != null) {

                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("erro ao fechar conexões:" + e.getMessage());
            }
        }
        return confirmaca;

    }

    public ResultSet listarLocadora(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("") || consulta.isEmpty()) {
            sql = "SELECT * FROM v_locadora";

        } else {
            sql = "SELECT * FROM v_locadora WHERE cnpj='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }

        return rs;

    }

    public int deletarLocadora(String deletar, int resposta) {

        String sqlLocadora = "DELETE FROM locadora WHERE id_locadora='" + deletar + "'";
        String sqlEndereco = "DELETE FROM endereco_locadora WHERE id_locadora='" + deletar + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();

                pstm = conn.prepareStatement(sqlLocadora);
                pstm.executeUpdate();

                pstm = conn.prepareStatement(sqlEndereco);
                pstm.executeUpdate();
                return 1;
            } catch (Exception e) {

                return 0;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {

                    System.err.println("Erro ao fechar conexão");
                }
            }

        }

        return 0;
    }

    public int alterarLocadora(String idLocadora, String nome, String cnpj, String rua, String bairro, String cidade, String cep, String estado) {
        try {
            conn = new ConexaoBD().getConexao();
            String sqlLocadora = "UPDATE locadora SET nome='" + nome
                    + "',cnpj='" + cnpj
                    + "'WHERE id_locadora='" + idLocadora + "'";
            pstm = conn.prepareStatement(sqlLocadora);
            pstm.executeUpdate();

            String sqlEndLocadora = "UPDATE endereco_locadora SET rua='" + rua
                    + "',bairro='" + bairro
                    + "',cidade='" + cidade
                    + "',cep='" + cep
                    + "',estado='" + estado
                    + "'WHERE id_locadora='" + idLocadora + "'";

            pstm = conn.prepareStatement(sqlEndLocadora);
            pstm.executeUpdate();
            return 1;

        } catch (Exception e) {
            System.err.println("Erro:" + e.getMessage());
            return 0;
        } finally {
            //Fecha as conexões
            try {
                if (pstm != null) {
                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {

                System.err.println("Erro ao fechar conexões");
            }
        }

    }

}
