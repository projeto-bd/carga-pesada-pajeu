/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import br.com.CargaPesadaPajeu.modeller.Locacao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author root
 */
public class Dao_Locacao {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;
    Date dataAtual = new Date(System.currentTimeMillis());
    SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");

    //Conta quantidade de reservas existentes para não dar erro no retorno
    public int contarReserva(String consulta) {
        int resultado = 0;
        conn = new ConexaoBD().getConexao();

        String achoSql = "";

        achoSql = "SELECT COUNT(id_reserva) FROM reserva WHERE id_reserva='" + consulta + "' AND status ='"+"Aguardando"+"'";

        try {

            st = conn.createStatement();
            rs = st.executeQuery(achoSql);
            while (rs.next()) {
                resultado = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }

        return resultado;

    }

    public ResultSet listarReservas(String consulta) {
        conn = new ConexaoBD().getConexao();
        int qtReseva = 0;

        String sql = "";

        sql = "SELECT * FROM v_reserva WHERE id_reserva='" + consulta + "' AND status ='"+"Aguardando"+"'";
        qtReseva = contarReserva(consulta);

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }

        if (qtReseva != 0) {
            return rs;

        } else {
            return null;
        }

    }

    public ResultSet veiculosDisponiveis(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("P3")) {
            sql = "SELECT placa FROM v_veiculoPequeno WHERE status = '" + "Disponível" + "'";

        } else if (consulta.equals("G4")) {
            sql = "SELECT placa FROM v_caminhoneteCarga WHERE status = '" + "Disponível" + "'";

        } else if (consulta.equals("M2")) {
            sql = "SELECT placa FROM v_caminhonetePassageiro WHERE status = '" + "Disponível" + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }
        return rs;
    }
    public ResultSet listaKmAtual(String placa) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

            sql = "SELECT quilometragem_atual FROM veiculo WHERE placa = '" + placa + "'";
        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }
        return rs;
    }

    public boolean efetuarLocacao(Locacao locacao, int idreserva, String placa) {
        boolean resultado = false;

        String sql = "INSERT INTO locacao (id_reserva,motorista,placa_veiculo)"
                + "VALUES (?,?,?)";

        try {
            conn = new ConexaoBD().getConexao();
            pstm = conn.prepareStatement(sql);

            pstm.setInt(1, idreserva);
            pstm.setString(2, locacao.getMotorista());
            pstm.setString(3, locacao.getPlaca_veiculo());
            if (alterarStatusCarroLocado(placa) == true) {
                pstm.execute();
                resultado = true;

            } else {
                resultado = false;
            }

        } catch (Exception e) {
            resultado = false;
            System.err.println("erro:" + e.getMessage());

        } finally {

            //Fecha as conexões
            try {
                if (pstm != null) {

                    pstm.close();
                    System.out.println("Conexão Encerrada");
                }

                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {

                System.err.println("Erro ao fechar conexões!");
            }
        }
        return resultado;

    }

//    public boolean alterarStatusCarroLocado(String placa) {
//        boolean resultado = false;
//        try {
//            conn = new ConexaoBD().getConexao();
//            String sqlLocadora = "UPDATE veiculo SET status='" + "Locado"
//                    + "'WHERE placa='" + placa + "'";
//            pstm = conn.prepareStatement(sqlLocadora);
//            pstm.executeUpdate();
//
//            resultado = true;
//
//        } catch (Exception e) {
//            System.err.println("Erro:" + e.getMessage());
//            resultado = false;
//        } finally {
//            //Fecha as conexões
//            try {
//                if (pstm != null) {
//                    pstm.close();
//                    System.out.println("Conexão Encerrada");
//                }
//
//                if (conn != null) {
//                    conn.close();
//                    System.out.println("Conexão Encerrada");
//                }
//
//            } catch (Exception e) {
//                System.err.println("Erro ao fechar as conexões");
//            }
//        }
//        return resultado;
//
//    }
    public ResultSet listarLocacoes(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("") || consulta.isEmpty()) {
            sql = "SELECT * FROM v_LocacaoReserva";

        } else {
            sql = "SELECT * FROM v_LocacaoReserva WHERE id_locacao='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro de conexão:" + ex.getMessage());
        }

        return rs;

    }

    public boolean deletarLocacaoReserva(String deletarLocacao, String IdDeletarReserva,String placa, int resposta) {
        boolean confirmacao = false;
        int deletaReservaParametro = Integer.parseInt(IdDeletarReserva);
        String sql = "DELETE FROM locacao WHERE id_locacao='" + deletarLocacao + "'";
        if (resposta == 0) {
            try {
                conn = new ConexaoBD().getConexao();
                pstm = conn.prepareStatement(sql);
                pstm.executeUpdate();

                if (deletaReserva(deletaReservaParametro) == true && alteraStatusVeiculoPDispinvel(placa)) {
                    pstm.executeUpdate();
                    confirmacao = true;

                } else {
                    confirmacao = false;
                }

            } catch (SQLException e) {
                System.err.println("Erro:" + e.getErrorCode());
                confirmacao = false;
            } finally {

                //Fecha as conexões
                try {
                    if (pstm != null) {

                        pstm.close();
                        System.out.println("Conexão Encerrada");
                    }

                    if (conn != null) {
                        conn.close();
                        System.out.println("Conexão Encerrada");
                    }

                } catch (Exception e) {
                    System.err.println("Erro ao fechar conexão");
                }
            }

        }

        return confirmacao;

    }

    public boolean verificaMotoristas(String cpf) {
        boolean confirmacao = false;
        conn = new ConexaoBD().getConexao();
        String dataAtualConverdida = formatador.format(dataAtual);
        int anoAtual = Integer.parseInt(dataAtualConverdida.substring(6));
        String sql = "SELECT * FROM v_clienteFisico WHERE num_habilitacao !='" + "Não tem" + "'";

        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {

                if (cpf.equals(rs.getString("cpf"))) {
                    String dataNascimentoMotorista = rs.getDate(5).toString();
                    int anoNascimento = Integer.parseInt(dataNascimentoMotorista.substring(0, 4));
                    int idadeAtual = anoAtual - anoNascimento;
                    if (idadeAtual >= 21) {
                        confirmacao = true;
                    } else {
                        confirmacao = false;
                    }
                    break;
                } else {

                    confirmacao = false;
                }

            }

        } catch (SQLException ex) {
            System.err.println("Algo deu errado:" + ex.getMessage());
        } finally {
            //Fecha as conexões
            try {
                if (conn != null) {
                    conn.close();
                    System.out.println("Conexão Encerrada");
                }

            } catch (Exception e) {
                System.err.println("Ops! erro ao fechar as conexões!");
            }

        }
        return confirmacao;
    }
//---------------PROCEDURE-----------

    public boolean deletaReserva(int idRserva) {
        conn = new ConexaoBD().getConexao();
        boolean confirmarDeletarReserva = false;
        try {
            CallableStatement cstmt = conn.prepareCall("{call deletarRerva(?)}");
            cstmt.setInt(1, idRserva);
            cstmt.execute();
            confirmarDeletarReserva = true;
        } catch (Exception e) {
            confirmarDeletarReserva = false;
        }
        return confirmarDeletarReserva;
    }

    public boolean alterarStatusCarroLocado(String placa) {
        conn = new ConexaoBD().getConexao();
        boolean confirmarDeletarReserva = false;
        try {
            CallableStatement cstmt = conn.prepareCall("{call alteraVeiucloLocado(?)}");
            cstmt.setString(1, placa);
            cstmt.execute();
            confirmarDeletarReserva = true;
        } catch (Exception e) {
            confirmarDeletarReserva = false;
        }
        return confirmarDeletarReserva;
    }
    
    public boolean alteraStatusVeiculoPDispinvel(String placa) {
        conn = new ConexaoBD().getConexao();
        boolean confirmarDeletarReserva = false;
        try {
            CallableStatement cstmt = conn.prepareCall("{call alterarVeiculoPDisponivel(?)}");
            cstmt.setString(1, placa);
            cstmt.execute();
            confirmarDeletarReserva = true;
        } catch (Exception e) {
            confirmarDeletarReserva = false;
        }
        return confirmarDeletarReserva;
    }

//---------------PROCEDURE FIM-----------
    public int retornaNumeroLocacao() {
        int resultado = 0;
        conn = new ConexaoBD().getConexao();

        String achoSql = "";

        achoSql = "SELECT MAX(id_locacao) FROM locacao";

        try {

            st = conn.createStatement();
            rs = st.executeQuery(achoSql);
            while (rs.next()) {
                resultado = rs.getInt(1);
            }

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }
        System.err.println("resultado:" + resultado);
        return resultado;
    }

//    public static void main(String[] args) {
//	Date x = new Date(System.currentTimeMillis());
//        SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
//        
//        System.err.println(formatador.format(x));
//        Dao_Locacao teste = new Dao_Locacao();
//        System.err.println(teste.verificaMotoristas("555.555.555-55"));;
//
//    }
}
