/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.CargaPesadaPajeu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author root
 */
public class Dao_Relatorio {

    Connection conn = null;
    PreparedStatement pstm = null;
    ResultSet rs;
    Statement st;

    public ResultSet listarReservas(String dataInicial, String dataFinal) {
        conn = new ConexaoBD().getConexao();

        String sql = "";
        if (dataInicial.equals("") && dataFinal.equals("")) {
            sql = "SELECT * FROM v_reserva";

        } else {

            sql = "SELECT * FROM v_reserva WHERE data_retirada BETWEEN '" + dataInicial + "' AND '" + dataFinal + "'";
        }
        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }
        return rs;
    }

    public ResultSet listarLocacoes(String dataInicial, String dataFinal) {
        conn = new ConexaoBD().getConexao();
        String sql = "";
        if (dataInicial.equals("") && dataFinal.equals("")) {
            sql = "SELECT * FROM v_LocacaoReserva";

        } else {

            sql = "SELECT * FROM v_LocacaoReserva WHERE data_retirada BETWEEN '" + dataInicial + "' AND '" + dataFinal + "'";
        }
        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro de conexão");
        }
        return rs;
    }

    public ResultSet listarClienteFisico(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("")) {
            sql = "SELECT * FROM v_clienteFisico";

        } else {
            sql = "SELECT * FROM v_clienteFisico WHERE cpf='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro ao Carregar Informações:" + ex.getMessage());
        }
        return rs;

    }

    public ResultSet listarClienteJuridico(String consulta) {
        conn = new ConexaoBD().getConexao();

        String sql = "";

        if (consulta.equals("")) {
            sql = "SELECT * FROM v_clienteJuridico";

        } else {
            sql = "SELECT * FROM v_clienteJuridico WHERE cnpj='" + consulta + "'";

        }

        try {

            st = conn.createStatement();
            rs = st.executeQuery(sql);

        } catch (SQLException ex) {
            System.err.println("Erro ao Carregar Informações:" + ex.getMessage());

        }
        return rs;
    }

}
